var jsonpath = '../../servlet/ajax';

$(document).ready( 
	function() {
		$('.date').datepicker();
		$('.date').mask("99/99/9999");
		//nuovoRischio();
		nuovoAmmanco();
		nuovoActionPlan();
		nuovoProdotto();
		nuovoOwnerControlloSecondoLivello();
	}
);

// funzione per 3 select DINAMICHE nella processoFormMission.jsp

function nuovoProdotto()
{
	var id = getId();
	var idselect = 'categorie' + id;
	var idselect_sotto_categorie = 'sottocategorie' + id;
	var idselect_prodotti = 'prodotti' + id;
	
	var idriga = 'riga' + id;
	var rischi = $('#listaProdotti').append("<p id='"+idriga+"'>Categoria&nbsp;<select id='" + idselect + "' name='categorie[]' class='categorie' onchange='caricaSottocategorie(" + idselect + ","+idselect_sotto_categorie+","+idselect_prodotti+")'/>&nbsp;&nbsp;Sotto-Categoria&nbsp;<select id='" + idselect_sotto_categorie + "' name='sottocategorie[]' class='sottocategorie' onchange='caricaProdotti(" + idselect_sotto_categorie + ","+idselect_prodotti+")'/>&nbsp;&nbsp;Prodotti&nbsp;<select id='" + idselect_prodotti + "' name='prodotti[]' class='prodotti' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");
	$.getJSON(jsonpath+'?op=categorie', function(data) {
		popolaSelectCategorie($('#'+idselect), data.map);
	});
	
}


// funzione usata per caricare le SottoCategorie del Prodotto

function caricaSottocategorie(categoria, sottocategoria,prodotto)
{
	var categoria = $('#'+categoria.id).val();
	$.getJSON(jsonpath+'?op=sottocategorie&categoria='+categoria, function(data) {
		popolaSelectSottoCategorie($('#'+sottocategoria.id), data.map);
	});
	
}

//funzione usata per caricare i Prodotti dopo aver scelto la SottoCategoria

function caricaProdotti(sottocategoria, prodotto)
{
	var sottocategoria = $("#"+sottocategoria.id).val();
	$.getJSON(jsonpath+'?op=prodotti&sottocategoria='+sottocategoria, function(data) {
		popolaSelectProdotti($('#'+prodotto.id), data.map);
	});
	
}


// funzione apposita per creare le categorie con l'option TUTTE :

function popolaSelectCategorie(select, data) {   
 select.html('');   
 select.append('<option value="">Tutte</option>');
 $.each(data, function(key, value) {   
     select.append($('<option></option>').val(key).html(value));   
 });          
}   

// funzione apposita per creare le sotto categorie con l'option TUTTE :

function popolaSelectSottoCategorie(select, data) {   
 select.html('');   
 select.append('<option value="">Tutte</option>');
 $.each(data, function(key, value) {   
     select.append($('<option></option>').val(key).html(value));   
 });          
}  

// funzione apposita per creare i Prodotti con l'option TUTTI :

function popolaSelectProdotti(select, data) {   
 select.html('');   
 select.append('<option value="">Tutti</option>');
 $.each(data, function(key, value) {   
     select.append($('<option></option>').val(key).html(value));   
 });          
}  


// funzione per la select DINAMICA delle strutture organizzative utilizzate in rischioPassiForm.jsp

function nuovoRischio()
{
	var id = getId();
	var idselect = 'rischi' + id;
	var idriga = 'riga' + id;
	var rischi = $('#listaRischi').append("<p id='"+idriga+"'><label for='rischi[]'>Owner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><select id='" + idselect + "' name='rischi[]' class='rischinuovi' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");

	$.getJSON(jsonpath+'?op=struttureOrganizzative', function(data) {
		popolaSelect($('#'+idselect), data.map);
	});
}


//funzione per la select DINAMICA delle strutture organizzative utilizzate in 
// jsp/controlloModulo231/controlloSecondoLivelloPassiFormModulo231.jsp

function nuovoOwnerControlloSecondoLivello()
{
	var id = getId();
	var idselect = 'rischi' + id;
	var idriga = 'riga' + id;
	var rischi = $('#listaOwner').append("<p id='"+idriga+"'><label for='rischi[]'>Owner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><select id='" + idselect + "' name='rischi[]' class='rischinuovi' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");

	$.getJSON(jsonpath+'?op=struttureOrganizzativeParticolari', function(data) {
		popolaSelect($('#'+idselect), data.map);
	});
}


function OwnerEsistentiControllo(id_attivita)
{
	
	var id = getId();
	var idselect = 'rischi' + id;
	var idriga = 'riga' + id;
	var rischi = $('#listaOwnerEsistenti').append("<p id='"+idriga+"'><label for='rischi[]'>Owner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><select id='" + idselect + "' name='rischi[]' class='rischinuovi' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");

	$.getJSON(jsonpath+'?op=struttureOrganizzativeEsistenti&idAttivita='+id_attivita, function(data) {
		popolaSelect($('#'+idselect), data.map);
	});
}




// funzione usata nelle jsp di MODIFICA Attivita, Rischi e Controlli per
// gestione campi dinamici gi� inseriti

function aggiungiRischio(selezionata)
{
	var id = getId();
	var idselect = 'rischiAggiunti' + id;
	var idriga = 'riga' + id;
	var rischi = $('#listaModifica').append("<p id='"+idriga+"'><label for='rischi[]'>Owner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><select id='" + idselect + "' name='rischiAggiunti[]' class='rischiesistenti' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");

	$.getJSON(jsonpath+'?op=struttureOrganizzative', function(data) {
		popolaSelectSelezionata($('#'+idselect), data.map, selezionata);
	});
}


// funzione per i 4 campi DINAMICI actionPlanPassiForm.jsp

function nuovoActionPlan()
{
	var controllo = controllaCampi('listaActionPlan');	
	if (!controllo) {
		alert('descrizione data sono campi obbligatori.');
		return false;
	}	
	var id = getId();
	var idriga = 'riga' + id;
	var idselect = 'rischi' + id;
	var rischi = $('#listaActionPlan').append("<p id='"+idriga+"'><label for='descrizione"+id+"'>Descrizione</label><input type='text' name='descrizione"+id+"' class='descrizione' /><br/><label for='rischi[]'>Owner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><select id='" + idselect + "' name='rischi[]' /><label for='data"+id+"'>&nbsp;Data Completamento</label><input type='text' name='data"+id+"' class='date' /><label for='completamento"+id+"'>&nbsp;% Completamento</label><input type='text' name='completamento"+id+"'class='completamento' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");
    fixDatepickerBugActionPlan();
    $.getJSON(jsonpath+'?op=struttureOrganizzative', function(data) {
		popolaSelect($('#'+idselect), data.map);
	});
}

// funzione di controllo utilizzata in nuovoActionPlan()

function controllaCampi(oggetto)
{
	var controllo = true;
	
	$("#"+oggetto).find("p > input").each(function(){
		var kid = $(this);
		controllo = controllo && (kid.val().length != 0);		
	});
	
	return controllo;
}


// funzione per il pulsante RIMUOVI di ogni campo dinamico

function rimuoviRiga(riga)
{
	$(riga).remove();
}

function getId()
{
	var d = new Date();
	var timestamp = d.getTime();
    return timestamp;
}


function popolaSelect(select, data) {   
    select.html('');   
    $.each(data, function(key, value) {   
        select.append($('<option></option>').val(key).html(value));   
    });          
}   

// funzione usata per gestire i campi SELECTED nella Form

function popolaSelectSelezionata(select, data, selezionata) {   
    select.html('');   
  
    $.each(data, function(key, value) {   
    	if (key == selezionata){
    		select.append($('<option selected></option>').val(key).html(value));   
    	} else {
        	select.append($('<option></option>').val(key).html(value));   
    	}
    });          
}   


// funzione per i 2 campi DINAMICI rischioPassiForm.jsp gestione AMMANCHI

function nuovoAmmanco()
{
	var id = getId();
	var idriga = 'riga' + id;
	var rischi = $('#listaAmmanchi').append("<p id='"+idriga+"'>Importo (&#8364;) <input type='text' name='importo"+id+"' class='importo' /><input type='text' name='data"+id+"'  class='date' /><input type='button' value='rimuovi' onclick='rimuoviRiga("+idriga+")'  /></p>");
    fixDatepickerBug();
}



function fixDatepickerBug()
{
	$('#listaAmmanchi').find(".date").removeClass('hasDatepicker').datepicker({ showOn: 'focus', showButtonPanel: true });
}

function fixDatepickerBugActionPlan()
{
	$('#listaActionPlan').find(".date").removeClass('hasDatepicker').datepicker({ showOn: 'focus', showButtonPanel: true });
}

function asfaltaValues(contenitore, nomecampo, type)
{
	createInputHidden(contenitore, nomecampo);
	var value = "";
	
	$("#"+contenitore).find("p > " + type).each(function(){
		var kid = $(this);
		value += kid.val() + ",";
	});
	
	value = value.substring(0,value.length-1);
	$("#"+nomecampo).val(value);
}

function createInputHidden(contenitore, nomecampo)
{
	if ($("#"+nomecampo).length == 0 ) {
		var hidden = $('#'+contenitore).append("<input type='hidden' id='"+nomecampo+"' name='"+nomecampo+"' />");
	}
}








