/*
	Feel free to use your custom icons for the tree. Make sure they are all of the same size.
	User icons collections are welcome, we'll publish them giving all regards.
*/

var iconsDir = '../../../img/icone/';

var TREE_TPL = {
	'target'  : '_self',	// name of the frame links will be opened in
							// other possible values are: _blank, _parent, _search, _self and _top

	'icon_e'  : iconsDir+'empty.gif', // empty image
	'icon_l'  : iconsDir+'line.gif',  // vertical line

	'icon_32' : iconsDir+'base.gif',   // root leaf icon normal
	'icon_36' : iconsDir+'base.gif',   // root leaf icon selected
	
	'icon_48' : iconsDir+'base.gif',   // root icon normal
	'icon_52' : iconsDir+'base.gif',   // root icon selected
	'icon_56' : iconsDir+'base.gif',   // root icon opened
	'icon_60' : iconsDir+'base.gif',   // root icon selected
	
	'icon_16' : iconsDir+'folder.gif', // node icon normal
	'icon_20' : iconsDir+'folderopen.gif', // node icon selected
	'icon_24' : iconsDir+'folderopen.gif', // node icon opened
	'icon_28' : iconsDir+'folderopen.gif', // node icon selected opened

	'icon_0'  : iconsDir+'folder.gif', // leaf icon normal
	'icon_4'  : iconsDir+'folderopen.gif', // leaf icon selected
	
	'icon_2'  : iconsDir+'joinbottom.gif', // junction for leaf
	'icon_3'  : iconsDir+'join.gif',       // junction for last leaf
	'icon_18' : iconsDir+'plusbottom.gif', // junction for closed node
	'icon_19' : iconsDir+'plus.gif',       // junctioin for last closed node
	'icon_26' : iconsDir+'minusbottom.gif',// junction for opened node
	'icon_27' : iconsDir+'minus.gif',       // junction for last opened node
	// Matteo RealT customization
	'href_start' : 'javascript:mioHref(\'', // String to prepone to each href
	'href_end' : '\')' // String to append to each href
};

