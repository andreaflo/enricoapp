  /****
  * Visualizza un messaggio d'errore e porta il focus sull'oggetto errato
  *
  *   sNomeElemento,  nome del campo
  *   sMsg,           testo del messaggio da visualizzare
  *
  ****/
function errore (sNomeElemento, sMsg) {
  window.alert(sMsg);
  //sNomeElemento.focus();
}
/****
  * Verifica che un campo di tipo text non sia vuoto ed eventualmente manda un messaggio d'errore e riporta il fuoco
  * sul campo.
  *
  *   sNomeCampo,       nome del campo comprensivo di document e form
  *   sEtichettaCampo,  etichetta del campo da utilizzare nel messaggio d'errore
  *   
  */
 function verifica_text (sNomeCampo, sEtichettaCampo ) {
    var sEtichetta = new String(sEtichettaCampo);
    sEtichetta = sEtichetta.toUpperCase();
    
    var iLen = sNomeCampo.value.length;
    
    if (iLen == 0) {
      errore (sNomeCampo, 'Il campo ' + sEtichetta  + ' deve essere valorizzato!');
      return false;
    }
    return true;
  }
//**********************************************************************
// Verifica esistenza di un oggetto javascript
// ritorna false se non esiste true se esiste
 //**********************************************************************  
function exists(myVar) {
  return ((""+myVar)!="undefined")
}
//**********************************************************************
//Controlla la lunghezza di una textarea e aggiorna la sua dimensione 
//in una text passata
//simula il maxlength del object text
//f   -textarea
//d   -dimensione massima
//lab -la text area che deve essere aggiornata con la lunghezza
//**********************************************************************
function lengthControlWithLabel(field,maxdim,label) {
        var lenUSig=0
        var lenLsig=0
        var lenSysSig=0
        var dimmax=maxdim
		maxLength = dimmax - lenSysSig - lenUSig
        if (field.value.length > maxLength) {
	        field.value = field.value.substring(0,maxLength)
		    charleft = 0
        } else {
			charleft = maxLength - field.value.length
		}
        if(exists(label.value)){
        label.value = charleft
        }
}
//**********************************************************************
//Controlla una text email 
//campo,       nome del campo email comprensivo di document e form
//sEtichettaCampo,  etichetta del campo da utilizzare nel messaggio d'errore
//**********************************************************************

var emailexp = /^[a-z][a-z_0-9\.\-]+@[a-z_0-9\.]+\.[a-z]{2,4}$/i;
function ValidateEmail(campo,sEtichettaCampo){
	if(emailexp.test(campo.value)){
 	
	return true;
 	
} else {
	
	var sEtichetta = new String(sEtichettaCampo);
    	sEtichetta = sEtichetta.toUpperCase();
	errore (campo, 'Il campo ' + sEtichetta  + ' ha un formato non valido!');
	return false;
	}
}
//**********************************************************************
//Controlla che due campi password contengano la stessa stringa 
//**********************************************************************
function ValidatePassword() {
	if (document.dati_utente.password.value == document.dati_utente.conf_pwd.value) {
		return true;
	} else {
		alert ("Password errata!");
		document.dati_utente.password.value = "";
		document.dati_utente.conf_pwd.value = "";
		return false;
	}
}



  function verifica_data (sNomeCampo) {
  	
  /****
  * Verifica che una data inserita in un campo di tipo text sia esistente. Si assume che la data venga fornita in
  * formato gg/mm/aaaa
  *
  *   sNomeCampo,   nome del campo comprensivo di document e nome dela form che contiene la data
  *
  *   Return: true se la data � corretta, false altrimenti
  */  
  	
    /* separo i campi della data */
    var sData = eval(sNomeCampo + '.value');
    var strData = new String(sData);
    
    var s1 = trim_javascript(strData);
    if ( s1.length == 0)
      return true;
    
      
    arCampi = strData.split('-');
    /* verifico che i campi della data siano 3 */
    
    
    /* verifico che le date siano solo numeri */
    for (var i=0; i < arCampi.length; i++) {
      if ( !isNumeroIntero(arCampi[i]) ) {
         errore (sNomeCampo, 'I campi data devono contenere solo numeri. La data deve essere nel formato: gg/mm/aaaa');
        return false;
      }
    }
    /*  verifico che il giorno esista */
    if ( !isDataReale(arCampi[0], arCampi[1], arCampi[2]) ) {
      errore (sNomeCampo, 'La data inserita non esiste. Controllare il giorno il mese e l\'anno inseriti');
      return false;      
    }
    return true;
	}
	
  function isDataReale (sGiorno, sMese, sAnno) {
  /****
  * Verifica che, date le tre stringhe che devono rappresentare il giorno il mese e l'anno, sia una data 
  * esistente.
  *
  *   sGiorno,    stringa contenente solo numeri che rappresenta il giorno
  *   sMese,      stringa contenente solo numeri che rappresenta il mese
  *   sAnno,      stringa contenente solo numeri che rappresenta l'anno
  *   
  *   Return: true se la data � esistente, false altrimenti
  */  
  	
  	var iGiorno = parseInt(sGiorno, 10);
		var iMese = parseInt(sMese, 10);
		var iAnno = parseInt(sAnno, 10);

		var bBisestile = false;
                		
		if ((iAnno%4)==0)
			if ((iAnno%400)==0)
				bBisestile = true;
			else  if ((iAnno%100)==0)
					    bBisestile = false;
            else bBisestile = true;
		else bBisestile = false;

		if ( ( (iMese==4) || (iMese==6) || (iMese==9) || (iMese==11) ) &&  ( (iGiorno>=1) && (iGiorno<=30) ) )
			return true;
		else  if ( ( (iMese==1) || (iMese==3) || (iMese==5) || (iMese==7) || (iMese==8) || (iMese==10) || (iMese==12) ) &&  ( (iGiorno>=1) && (iGiorno<=31) ) )
				    return true;
			    else  if ( (iMese==2) &&  ((iGiorno>=1) && (iGiorno<=28)) && (bBisestile==false) )
					        return true;
				        else if ( (iMese==2) &&  ((iGiorno>=1) && (iGiorno<=29)) && (bBisestile==true) )
						           return true;
					           else return false;
	}
	  function trim_javascript (s) {
  /****
  * Restituisce la stringa senza spazi all'inizio ed in fondo
  *
  *   s,           stringa da elaborare. La stringa pu� contenere spazi interni
  *   NOTA:        trim in javascript non esiste
  */  
  
      var sTemp = new String(s);
      var bPrimoChar = false;
      var bUltimoChar = false;
      
      /* trovo dove iniziano i caratteri senza spazi */
      for (var i=0; ( (i < sTemp.length) && !bPrimoChar); i++) {
        if (sTemp.charAt(i) != ' ')
          bPrimoChar = true;
      }   
      i--;    /* il for esegue l'incremento anche se la variabile booleana � false */
            
      /* trovo dove finiscono i caratteri senza spazi */
      for (var j=sTemp.length-1; ( (j >= 0) && !bUltimoChar); j--) {
        if (sTemp.charAt(j) != ' ')
          bUltimoChar = true;
      }
      j++;    /* il for esegue l'incremento anche se la variabile booleana � false */
      
      return sTemp.substring(i, j+1);     /* substring(iniz, fine) va da iniz a fine-1 */
  }
 function isNumeroIntero (s) {
  /****
  * Verifica che una stringa contenga solo un numero intero
  *
  *   s,           stringa da verificare
  *   
  */  
    
		var iLen = s.length;

    for (var i = iLen-1; i >= 0; i--) {
			if (i == iLen-1)
        var c = s.substring(i);
      else var c = s.substring(i, i+1);

			if ((c < "0") || (c > "9")) {
				return false;
		  }
		}

		return true;	/* tutto OK */
  }
  function isLetterFree (s) {
  /****
  * Verifica che una stringa non contenga lettere
  *
  *   s,           stringa da verificare
  *   
  */  
    
		var iLen = s.length;

    for (var i = iLen-1; i >= 0; i--) {
			if (i == iLen-1)
        var c = s.substring(i);
      else var c = s.substring(i, i+1);

		if ((c >= "a") && (c <= "z")) {
			return false;
		}
		if ((c >= "A") && (c <= "Z")) {
			return false;
		}
	  }

		return true;	/* tutto OK */
  }
  
  function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}