var which = null;
var cancellami;

function showDiv(ID)
{	
	clearTimeout(cancellami);
	if (which != null) {
		which.className = "menu2LevH";
	}
	which = document.getElementById(ID);
	if (document.getElementById(ID).innerHTML!="") {
		which.className = "menu2LevS";
		positionDiv(ID);
	}
}

function hideDiv()
{
	cancellami = setTimeout('sureHideDiv()', 500);
}

function sureHideDiv()
{
	clearTimeout(cancellami);
	if (which != null) {which.className = "menu2LevH";}
	which = null;
}

function positionDiv(ID)
{
	
	document.getElementById(ID).style.left = getX("pdr" + ID) - 20 + "px";
	if (navigator.appName.indexOf("Microsoft") >= 0 || navigator.appVersion.indexOf("Chrome") >= 0 || navigator.appVersion.indexOf("Safari") >= 0)
		document.getElementById(ID).style.top = "40px";
	else
		document.getElementById(ID).style.top = "40px";
}

function getX(ID)
{	
	oElement = document.getElementById(ID);
	var iReturnValue = 0;
	while(oElement != null) {
		iReturnValue += oElement.offsetLeft;
		oElement = oElement.offsetParent;
	}
	return iReturnValue;
}