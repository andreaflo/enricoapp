// Controlla che un campo non sia vuoto
function notEmpty(field) {
	if (field.value=="" || field.value==null) return false;
	return true;
}

// Controlla che le due copie della password corrispondano
// in caso contrario cancella anche i campi
function equalPasswords(field1, field2) {
	if (field1.value==field2.value) return true;
	field1.value="";
	field2.value="";
	return false;
}

// Non un reale validator: si occupa di valorizzare alcuni
// campi hidden (o anche non) nella form
function setValue(form, field, value) {
	field.value = value;
	return true;
}
//Controlla che nel campo vi siano solo
// valori NUMERICI
function notLettera(campo) {
    var cifre= "0123456789.";
    var verifica= campo.value;
    var allValid = true;
    var allNum = "";
    
    for ( i = 0; i < verifica.length; i++ )
    {
           ch = verifica.charAt( i );
           for ( j = 0; j < cifre.length; j++ )
            if ( ch == cifre.charAt( j ))
                   break;
           if ( j == cifre.length )
           {
            allValid = false;
            break;
           }
           allNum += ch;
    }
    
    if (!allValid)
    {
          // alert( "Sono presenti dei caratteri alfanumerici, sono consentite solamente cifre numeriche" );
        
           return ( false );
    }
   return ( true );
	
	
}



