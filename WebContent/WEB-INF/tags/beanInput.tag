<%@ tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%>
<%@ attribute name="beanFormHtml" required="true" type="it.realt.webapps.html.BeanFormHtml"%>

<%
List listaProperties = beanFormHtml.getProperties();
BeanProperty beanProperty[] = new BeanProperty[listaProperties.size()];
Class c = beanFormHtml.getBean().getClass();

Iterator itProp = listaProperties.iterator();
int i=0;
while (itProp.hasNext()) {
	Object prop = (itProp.next());
	String propName;
	if (prop instanceof String) propName = (String)prop;
	else if (prop instanceof HtmlProperty) {
		propName = ((HtmlProperty)prop).getName();
	}
	else propName = prop.toString();
	beanProperty[i] = new BeanProperty(propName,c);
	i++;
}
%>
<table>
	<% for (i=0;i<beanProperty.length;i++) {
		String name=beanFormHtml.getPropertyDescription(i);
		Object propertyValue = beanProperty[i].getPropertyValue(beanFormHtml.getBean());
		if (propertyValue==null) propertyValue="";
	%>
	<tr><th><%=name%></th><td><input type="text" value="<%=propertyValue%>"></td></tr>
	<%}%>
</table>