<%
/**
 * Questo tag inserisce la funzione javascript
 * che fa il controllo di un form. Gli attributi da passare sono 
 * - il FormValidator "validator"
 * - il nome della funzione javascript "function" (valore di default: "check")
 * - il nome della form (opzionale, se omesso usa quello del formValidator)
 */
%><%@ 
tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%><%@ 
attribute name="validator" type="it.realt.webapps.html.FormValidator" required="true"%><%@
attribute name="function"%><%@
attribute name="form"%><%
if (function==null) function="check";
if (form==null) form=validator.getFormName();
Iterator it = validator.getValidationRules().iterator();
%><script type="text/javascript">
function <%=function%>() {
	messages = "";
	form = document.<%=form%>;
<%
while (it.hasNext()) {
	ValidationRule rule = (ValidationRule)it.next();
%>	if (!<%=validator.getJavascriptMethod(rule,form)%>) {
		messages+="<%=rule.getMessage()%>\n";
	}
<%}%>
	if (messages == "") {
		form.submit();
	}
	else {
		alert(messages);
	}
}
</script>