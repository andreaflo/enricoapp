<%@ 
tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%><%@
attribute name="name"%><%@ 
attribute name="action"%><%@
attribute name="method"%><%@ 
attribute name="target"%><%
String attr = "";
if (name!=null) { 
	attr+=" name=\""+name+"\"";
	getJspContext().setAttribute("name", name);
}
if (action!=null) attr+=" action=\""+action+"\"";
if (method!=null) attr+=" method=\""+method+"\"";
if (target!=null) attr+=" target=\""+target+"\"";
%><form<%=attr%>>
<jsp:doBody/>
</form>