<%@ tag import="it.realt.webapps.utente.costanti.SecurityConstants"%>
<%@ tag import="it.realt.webapps.utente.internet.ops.*, it.realt.general.BundledException"%>
<%@ tag import="it.realt.webapps.utente.dbbeans.*"%>
<%@ attribute name="idFunzione" required="true"%>
<%@ attribute name="idObject"%>
<%@ attribute name="tipoAutorizzazione" type="java.lang.Integer"%>


<%
if (idObject==null) idObject = "-";
if (tipoAutorizzazione==null) tipoAutorizzazione = new Integer(SecurityConstants.AUTHORIZED);
it.realt.webapps.utente.costanti.CostantiModuloUtente costanti = it.realt.webapps.utente.costanti.CostantiModuloUtente.getInstance();

it.realt.webapps.utente.dbbeans.Utente user = null;

if (session.getAttribute(costanti.utenteLabel) != null) {
	user = (it.realt.webapps.utente.dbbeans.Utente)session.getAttribute(costanti.utenteLabel);
}
else {
	user = new Utente();
	user.setTipoUtente("AMM");
	user.setIdUtente("2008103016100982013");
}
OpSicurezza opSicurezza = OpSicurezza.getInstance();
//if (!opSicurezza.hasPermission(user,idFunzione,idObject,tipoAutorizzazione.intValue())) throw new Exception("Non autorizzato");
%>