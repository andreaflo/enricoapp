<%@ 
tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%><%@ 
attribute name="property" required="true"%><%--@ 
attribute name="validator"--%><%@
attribute name="format" type="java.text.Format"%><%@
attribute name="bean" type="java.lang.Object"%><%@
attribute name="name"%><%@
attribute name="style"%><%@
attribute name="classe"%><%@
attribute name="type"%><%@
attribute name="maxlength"%><%@
attribute name="custom"%>
<%

String value = "";
if (name==null) name = property;
if (type==null) type = "text";
if (custom!=null) custom=" "+custom;
else custom="";

String styleAttribute = "";
if (style!=null) styleAttribute=" style=\""+style+"\"";

String classAttribute = "";
if (classe!=null) classAttribute=" class=\""+classe+"\"";

if (maxlength==null) maxlength="";
else maxlength=" maxlength=\""+maxlength+"\"";

if (bean != null) {
	BeanProperty beanProperty = new BeanProperty(property,bean.getClass());
	Object propertyObject = beanProperty.getPropertyValue(bean);
	if (propertyObject==null) value="";
	else if (format!=null) value = format.format(propertyObject);
	else value = propertyObject.toString();
}
if(type.equalsIgnoreCase("RADIOBOOLEAN")){
%>
S�<input type="radio" name="<%=name%>" value="Y" <%=styleAttribute%><%=classAttribute%><%=maxlength%><%=custom%>  <%=(value.equals("Y")||value.equals(""))?"CHECKED":""%> />
No<input type="radio" name="<%=name%>" value="N" <%=styleAttribute%><%=classAttribute%><%=maxlength%><%=custom%> <%=(value.equals("N"))?"CHECKED":""%>  />

<%} else {%>
	<input type="<%=type%>" name="<%=name%>" value="<%=value%>"<%=styleAttribute%><%=classAttribute%><%=maxlength%><%=custom%> />
<%}%>
<%--
if (validator!=null) {
%>	<script type="text/javascript"></script>
<%
}
--%>