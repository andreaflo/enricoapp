<%@ tag import="it.realt.webapps.utente.internet.ops.*,java.util.*,it.realt.webapps.utente.dbbeans.*"%>
<%@ tag import="it.realt.webapps.utente.internet.Menu"%>
<%@ tag import="it.realt.webapps.utente.costanti.CostantiModuloUtente"%>
<%@ tag import="it.realt.webapps.utente.dbbeans.Utente"%>
<%@ tag import="it.realt.webapps.utente.costanti.TipiUtente"%>
<%@ tag import="it.realt.webapps.utils.beans.FiltroLista"%>
<%@ tag import="it.realt.webapps.utils.internet.ops.OpLista"%>
<%@ 
attribute name="root" required="true" %>
<% 
request.setCharacterEncoding("UTF-8");
CostantiModuloUtente costantiMenu = CostantiModuloUtente.getInstance();
Utente user = (Utente)session.getAttribute(costantiMenu.utenteLabel);
String contesto = request.getContextPath();

// ora le impostazioni HTML del menu.....
String   beginMenu = "<table cellpadding=\"0px\" cellspacing=\"0px\" align=\"center\"><tr>";
String   endMenu   = "</tr></table>";

String   levelMenu0L= "<td id=\"pdr{0}\"  class=\"menuBarLink\" onclick=\"top.location.href=''{6}{1}''\" onMouseOver=\"showDiv(''{0}'');\" onMouseOut=\"hideDiv();\"><nobr>{2}</nobr>{3}</div></td>";
String   levelMenu1L= "<div class=\"menu2LevH\" id=\"{5}\"><table><tr><td class=\"item2Lev\" onclick=\"top.location.href=''{6}{1}''\" onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{2}</nobr></td></tr>{3}</table>";
String   levelMenu2L= "<tr><td class=\"item2Lev\" onclick=\"top.location.href=''{6}{1}''\" onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{4}{2}</nobr></td></tr>{3}";
String   levelMenu1LNODIV= "<table><tr><td class=\"item2Lev\" onclick=\"top.location.href=''{6}{1}''\" onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{2}</nobr></td></tr>{3}</table>";
String   levelMenu0LNOFIGLI= "<td id=\"pdr{0}\"  class=\"menuBarLink\" onclick=\"top.location.href=''{6}{1}''\"><nobr>{2}</nobr>{3}</div></td>";
String   levelMenu0= "<td id=\"pdr{0}\"  class=\"menuBarLink\"  onMouseOver=\"showDiv(''{0}'');\" onMouseOut=\"hideDiv();\"><nobr>{2}</nobr>{3}</div></td>";
String   levelMenu1= "<div class=\"menu2LevH\" id=\"{5}\"><table><tr><td class=\"item2Lev\"  onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{2}</nobr></td></tr>{3}</table>";
String   levelMenu2= "<tr><td class=\"item2Lev\"  onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{4}{2}</nobr></td></tr>{3}";
String   levelMenu1NODIV= "<table><tr><td class=\"item2Lev\"  onMouseOver=\"this.className=''highLight''\" onMouseout=\"this.className=''item2Lev''\"><nobr>&nbsp;&nbsp;{2}</nobr></td></tr>{3}</table>";
String[] levelMenuWithLink ={levelMenu0L,levelMenu1L,levelMenu2L,levelMenu1LNODIV,levelMenu0LNOFIGLI};
String[] levelMenuWithoutLink ={levelMenu0,levelMenu1,levelMenu2,levelMenu1NODIV};
String SPAZIOHTML="&nbsp;";

Menu menu=new Menu(user,beginMenu,endMenu,levelMenuWithLink,levelMenuWithoutLink,SPAZIOHTML);
menu.setOrdinaPer("targetField");
String sMenu=menu.getMenu(root,contesto);

%>



<%@tag import="it.realt.webapps.utils.dbbeans.DbLista"%>

<link href="<%=contesto%>/css/menu.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<%=contesto%>/js/menu.js"></script>
<!--[if lt IE 7.]>
<script defer type="text/javascript" src="<%=contesto%>/jsp/commons/js/pngfix.js"></script>
<![endif]-->
<table class="top" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="80" class="sfondoindex">&nbsp;</td>
    <td height="80" class="sfondoindex"><center><a href="<%=contesto%>"><img src="<%=contesto%>/img/menu/banner.jpg" width="200" height="80" border="0" align="top"></a></center></td>
    <td height="80" class="sfondoindex">&nbsp;</td>
  </tr>
</table>
<table class="undertop" width="100%" align="center">f
	<tr><td align="center"><%=sMenu%></td></tr>
</table>
<%
FiltroLista fLista = new FiltroLista();
OpLista opLista = new OpLista(user);
fLista.setNomeLista("utenti");
fLista.setChiave(user.getTipoUtente());
DbLista valore = new DbLista();
String utente = user.getTipoUtente();
try {
	valore = (DbLista)opLista.findByFilter(fLista).get(0);
	utente = valore.getValore();
} catch (Exception e) {}
%>
<div class="user">Benvenuto <%=user.getLogin()%> sei <%=utente%></div>
<div class="spaziatore">&nbsp;</div>
