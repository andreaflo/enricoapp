<%@ tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%>
<%@ attribute name="listaHtml" required="true" type="it.realt.webapps.html.ListaHtml"%>
<%@ attribute name="paginazione" type="it.realt.webapps.html.Paginazione"%>
<%@ attribute name="tableTags"%>
<%@ attribute name="thTags"%>
<%@ attribute name="trTags"%>
<%@ attribute name="tdTags"%>
<%@ attribute name="tableClass"%>
<%@ attribute name="trClass"%>
<%@ attribute name="tdClass"%>
<%@ attribute name="thClass"%>

<%!
private String initEmpty(String str) {
	if (str==null) str="";
	else str=" "+str;
	return str;
}
private String initClass(String str) {
	if (str==null) str="";
	else str=" class=\""+str+"\"";
	return str;
}
private String appendClass(String orig, String append) {
	if (append!=null && append.length()>0) orig+=" class=\""+append+"\"";
	return orig;
}
private String appendTags(String orig, String append) {
	if (append!=null && append.length()>0) orig+=" class=\""+append+"\"";
	return orig;
}
%><%
List listaBeans = listaHtml.getBeans();
List listaProperties = listaHtml.getProperties();
// Una matrice di propriet�: per ogni colonna c'� un'array di propriet� passate
BeanProperty beanProperty[][] = new BeanProperty[listaProperties.size()][];
Format format[]=new Format[listaProperties.size()];
TableStyler styler = listaHtml.getTableStyler();
if (styler==null) styler = new SimpleTableStyler();

tableClass = initClass(tableClass);
thClass = initClass(thClass);
trClass = initClass(trClass);
tdClass = initClass(tdClass);

tableTags = initEmpty(tableTags);
thTags = initEmpty(thTags);
trTags = initEmpty(trTags);
tdTags = initEmpty(tdTags);

if (listaBeans.size()>0) {
	Object prototype = listaBeans.get(0);
	Class c = prototype.getClass();
	Iterator itProp = listaProperties.iterator();
	int indiceColonna=0;
	while (itProp.hasNext()) {
		Object prop = (itProp.next());
		String propName;
		if (prop instanceof String) propName = (String)prop;
		else if (prop instanceof HtmlProperty) {
			propName = ((HtmlProperty)prop).getName();
			format[indiceColonna]=((HtmlProperty)prop).getFormat();
		}
		else propName = prop.toString();
		String properties[] = propName.split(",");
		beanProperty[indiceColonna] = new BeanProperty[properties.length];
		for (int indiceProprieta=0; indiceProprieta<properties.length; indiceProprieta++) {
			beanProperty[indiceColonna][indiceProprieta] = new BeanProperty(properties[indiceProprieta],c);
		}
		indiceColonna++;
	}
}
int row = 0;
String tags=tableClass+tableTags;
tags = appendClass(tags,styler.getTableClass());
tags = appendTags(tags,styler.getTableTags());
String cellMarker;
%>
<table<%=tags%>>
<%
tags=trClass+trTags;
tags = appendClass(tags,styler.getTrClass(row));
tags = appendTags(tags,styler.getTrTags(row));
%>
<tr<%=tags%>>
<%
// Header della tabella
	for (int i=0; i<listaProperties.size(); i++) {
		if (styler.isTh(row,i)) {
			cellMarker = "th";
			tags = thClass+thTags;
			tags = appendClass(tags,styler.getThClass(row,i));
			tags = appendTags(tags,styler.getThTags(row,i));
		}
		else {
			cellMarker = "td";
			tags = tdClass+tdTags;
			tags = appendClass(tags,styler.getTdClass(row,i));
			tags = appendTags(tags,styler.getTdTags(row,i));
		}
		String startLink = "", endLink = "";
		if (paginazione!=null) {
			startLink = "<a"+tags+" href=\""+paginazione.getLink(listaHtml.getPropertyOrdering(i))+"\">";
			endLink = "</a>";
		}
	%>
	<<%=cellMarker%><%=tags%>><%=startLink%><%=listaHtml.getPropertyDescription(i)%><%=endLink%></<%=cellMarker%>>
	<%
	}
%>
</tr>
<%
Iterator listaBeansIterator = listaBeans.iterator();
while (listaBeansIterator.hasNext()) {
	row++;
	Object bean = listaBeansIterator.next();
	tags=trClass+trTags;
	tags = appendClass(tags,styler.getTrClass(row));
	tags = appendTags(tags,styler.getTrTags(row));
%>
<tr<%=tags%>><%
	for (int i=0; i<beanProperty.length; i++) {
		Object propertyValue;
		if (beanProperty[i].length==1) {
			propertyValue = beanProperty[i][0].getPropertyValue(bean);
		}
		else {
			Object multiValues[]=new Object[beanProperty[i].length];
			for (int indiceProprieta=0; indiceProprieta<beanProperty[i].length; indiceProprieta++) {
				multiValues[indiceProprieta]=beanProperty[i][indiceProprieta].getPropertyValue(bean);
			}
			propertyValue = multiValues;
		}
		if (format[i]!=null) propertyValue = format[i].format(propertyValue);
		// Se la stringa � vuota mettiamo uno spazio per i bug soliti di visualizzazione
		if ("".equals(propertyValue)) propertyValue="&nbsp;";
		if (styler.isTh(row,i)) {
			cellMarker = "th";
			tags = thClass+thTags;
			tags = appendClass(tags,styler.getThClass(row,i));
			tags = appendTags(tags,styler.getThTags(row,i));
		}
		else {
			cellMarker = "td";
			tags = tdClass+tdTags;
			tags = appendClass(tags,styler.getTdClass(row,i));
			tags = appendTags(tags,styler.getTdTags(row,i));
		}
%>
	<<%=cellMarker%><%=tags%>><%=propertyValue%></<%=cellMarker%>><%
	}
%>
</tr>
<%
}
%>
</table>