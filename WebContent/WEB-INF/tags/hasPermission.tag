<%@ tag import="it.realt.webapps.utente.internet.ops.*"%>
<%@ attribute name="idFunzione" required="true"%>
<%@ attribute name="idObject"%>
<%@ attribute name="tipoAutorizzazione" type="java.lang.Integer"%>


<%
if (idObject==null) idObject = "";
if (tipoAutorizzazione==null) tipoAutorizzazione = new Integer(1);
it.realt.webapps.utente.costanti.CostantiModuloUtente costanti = it.realt.webapps.utente.costanti.CostantiModuloUtente.getInstance();
it.realt.webapps.utente.dbbeans.Utente user = (it.realt.webapps.utente.dbbeans.Utente)session.getAttribute(costanti.utenteLabel);

OpSicurezza opSicurezza = OpSicurezza.getInstance();
if (opSicurezza.hasPermission(user,idFunzione,idObject,tipoAutorizzazione.intValue())) {
	%>
	<jsp:doBody />
	<%
}
else {
%>
Non autorizzato
<%}%>