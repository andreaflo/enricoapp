<%@ attribute name="paginazione" required="true" type="it.realt.webapps.html.Paginazione"%>

<%
 int minPaginaLink = Math.max(1,paginazione.getPaginaCorrente()-paginazione.getDelta());
 int maxPaginaLink = Math.min(paginazione.getPagine(),paginazione.getPaginaCorrente()+paginazione.getDelta());
%>
<BR>
<TABLE><TR>
<td align="right"  class="menu">
pagina&nbsp;${paginazione.paginaCorrente}&nbsp;di&nbsp;${paginazione.pagine}&nbsp;&nbsp;&nbsp;&nbsp;
<%if(paginazione.getPaginaCorrente()>1){%>
	<a class="link" href="<%=paginazione.getLink(paginazione.getPaginaCorrente()-1)%>"><img src="<%=request.getContextPath()%>/img/img_primo/sx.gif" name="0" width="15" height="15" alt="Indietro" title="Indietro" border="0" align="center" valign="middle"></a> 
<%}%>
<% for (int i=minPaginaLink; i<=maxPaginaLink; i++) {%>
<A class="link" HREF="<%=paginazione.getLink(i)%>"><%=i%></a>&nbsp; 
<% } %>
<%if(paginazione.getPaginaCorrente()<paginazione.getPagine()){%>
	<a class="link" href="<%=paginazione.getLink(paginazione.getPaginaCorrente()+1)%>"><img src="<%=request.getContextPath()%>/img/img_primo/dx.gif" name="0" width="15" height="15" alt="Avanti" title="Avanti" border="0" align="center" valign="middle"></a> 
<%}%>

</td>
</TR></TABLE>