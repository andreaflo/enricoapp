<%@ tag import="org.jbeans.*,java.util.*,it.realt.webapps.html.*,java.text.*,it.realt.webapps.html.tablestyler.*"%>
<%@ attribute name="select" required="true" type="it.realt.webapps.html.Select"%>
<%@ attribute name="property"%>
<%@attribute name="bean" type="java.lang.Object"%>
<%@attribute name="custom"%>
<%
if (custom==null) custom="";
	else custom=" "+custom;
%>
<select name="<%=property%>"<%=custom%>>
<% //Questo tag consente l'inserimento di un input di tipo select
	{
	String value = null;
	if (bean != null) {
		BeanProperty beanProperty = new BeanProperty(property,bean.getClass());
		Object objectValue = beanProperty.getPropertyValue(bean);
		if (objectValue!=null) value = objectValue.toString();
		else value="";
	}
	Map m = select.getMap();
	Iterator it = m.keySet().iterator();
    while (it.hasNext()) {
    	Option opt = (Option)m.get(it.next());
    	// L'opzione � selezionata se il suo valore ci � stato passato come selezionato
    	// oppure se nessun valore � stato specificato come selezionato, nel caso in
    	// cui l'opzione sia selezionata per default.
    	boolean isSelected = (value!=null && value.equals(opt.getValue())) || (value==null && opt.isDefaultSelected());
    	String selected = isSelected?" selected=\"selected\"":"";
    %>	<option value="<%=opt.getValue()%>"<%=selected%>><%=opt.getDescrizione()%></option>
<% }
    %><%
    } %>
</select>