<%@ tag import="it.realt.webapps.i18n.*"%>

<%@ attribute name="id"%>
<%@ attribute name="bundle"%>


<%
if (id==null) id="text";
if (bundle==null) bundle="text";
LanguageBundle languageBundle = LanguageBundle.getDefault(request,response,bundle);
getJspContext().setAttribute(id, languageBundle, PageContext.REQUEST_SCOPE);
%>