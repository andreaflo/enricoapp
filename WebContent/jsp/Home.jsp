<%@ include file="/jsp/commons/costanti.jspf"%>
<!DOCTYPE html>
<!-- saved from url=(0033)https://www.stampagrafica.it/ita/ -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon"
	href="https://www.www.coronasrl.it.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon"
	href="https://www.www.coronasrl.it.it/favicon.ico">
<link rel="shortcut icon"
	href="https://www.www.coronasrl.it.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.coronasrl.it/favicon.ico"
	type="image/x-icon">
<link href="./Home_files/css" rel="stylesheet" type="text/css">
<link href="./Home_files/stampagrafica.css" rel="stylesheet"
	type="text/css">
<link href="./Home_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./Home_files/glide.css" rel="stylesheet" type="text/css">
<link href="./Home_files/carousel.css" rel="stylesheet" type="text/css">
<link href="./Home_files/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="./Home_files/connected-carousels.css" rel="stylesheet"
	type="text/css">
<link href="./Home_files/style1.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="./Home_files/jquery.js"></script>
<script type="text/javascript" src="./Home_files/jquery-ui.js"></script>
<script type="text/javascript" src="./Home_files/jquery.jcarousel.js"></script>
<script type="text/javascript" src="./Home_files/glide.js"></script>
<script type="text/javascript" src="./Home_files/animenu.js"></script>
<script type="text/javascript" src="./Home_files/jquery.fancybox.js"></script>

<script async="" src="./Home_files/analytics.js" type="text/javascript"></script>


<!-- SLIDER -->
<script type="text/javascript">
	(function($) {
		// This is the connector function.
		// It connects one item from the navigation carousel to one item from the
		// stage carousel.
		// The default behaviour is, to connect items with the same index from both
		// carousels. This might _not_ work with circular carousels!
		var connector = function(itemNavigation, carouselStage) {
			return carouselStage.jcarousel('items').eq(itemNavigation.index());
		};

		$(function() {
			// Setup the carousels. Adjust the options for both carousels here.
			var carouselStage = $('.carousel-stage').jcarousel();
			var carouselNavigation = $('.carousel-navigation').jcarousel();

			// We loop through the items of the navigation carousel and set it up
			// as a control for an item from the stage carousel.
			carouselNavigation.jcarousel('items').each(function() {
				var item = $(this);

				// This is where we actually connect to items.
				var target = connector(item, carouselStage);

				item.on('jcarouselcontrol:active', function() {
					carouselNavigation.jcarousel('scrollIntoView', this);
					item.addClass('active');
				}).on('jcarouselcontrol:inactive', function() {
					item.removeClass('active');
				}).jcarouselControl({
					target : target,
					carousel : carouselStage
				});
			});

			// Setup controls for the stage carousel
			$('.prev-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			// Setup controls for the navigation carousel
			$('.prev-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			$('.carousel').jcarousel({
				wrap : 'circular',
				scroll : 1
			}).jcarouselAutoscroll({
				interval : 5000,
				target : '+=1',
				autostart : true
			}).hover(function() {
				$(this).jcarouselAutoscroll('stop');
			}, function() {
				$(this).jcarouselAutoscroll('start');

			});
		});
	})(jQuery);

	$('.jcarousel').jcarousel({
		wrap : 'circular'
	}).jcarouselAutoscroll({
		interval : 3000,
		target : '+=1'
	});
</script>

<script type="text/javascript">
	$(document).ready($(function() {
		// this initializes the dialog (and uses some common options that I do)
		$("#dialog").dialog({
			autoOpen : false,
			modal : true,
			resizable : false,
			height : 200,
			show : "blind",
			hide : "blind"
		});

		// next add the onclick handler
		$('[name="download"]').click(function() {
			$("#dialog").dialog("open");
			$("#emailForm").show();
			return false;
		});
		
		$("#email").mouseover( function() {
		let empty = true;
		empty = $("#email").val().length == 0;
		if (empty)
			$("#submitEmail").attr('disabled', 'disabled');
		else
			$("#submitEmail").attr('disabled', false);
	});
		}));
</script>

<title>Home</title>

<!-- GOOGLE ANALITYCS-->
<script type="text/javascript">
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', 'UA-1442714-23', 'auto');
	ga('send', 'pageview');
	ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

</head>

<body>
	<div class="frame">
		<div class="bit-1">
			<div id="topbar">
				<div class="quickcontact">
					<p class="vcenter">
						<strong>Contatti veloci:</strong><br> tel. 045.7830311<br>
						info@coronasrl.it
					</p>
				</div>
				<div class="lingue">
					<p class="vcenter">
						<a href="./Home.jsp">ITA</a>
					</p>
					<p>
						<a href="http://localhost:8080/EnricoApp/jsp/backoffice/login.jsp">BackOffice</a>
					</p>
				</div>


				<div class="logo">
					<a href="./Home.jsp"><img src="./Home_files/Logo-CoronaSrl.jpg"
						width="250" height="100" alt=""></a>
				</div>
			</div>
			<!-- fine topbar -->
		</div>
		<!--fine bit-1 -->
	</div>
	<!-------------------------fine frame ------------------------------->
	<div class="frame">
		<div class="bit-1 centertxt">
			<nav class="animenu" id="nav">
				<button class="animenu__toggle">
					<span class="animenu__toggle__bar"></span> <span
						class="animenu__toggle__bar"></span> <span
						class="animenu__toggle__bar"></span>
				</button>
				<ul class="animenu__nav">


					<li><a href="Azienda.jsp">azienda</a>
						<ul class="animenu__nav__child">
							<li><a href="I nostri Reparti.jsp">i nostri reparti</a></li>
							<li><a href="Possibilita.jsp">i nostri clienti</a></li>
						</ul></li>
					<li><a href="Prodotti.jsp">prodotti</a></li>
					<li><a href="ProdottiEServizi.jsp">servizi</a></li>
					<li><a href="News.jsp">news</a></li>
					<li><a href="Contatti.jsp">contatti</a></li>


				</ul>
			</nav>


		</div>
	</div>
	<div align="left" id="centratore">
		<div class="frame">
			<div class="wrapper">
				<div class="connected-carousels">
					<div class="stage">
						<div class="carousel carousel-stage">
							<ul>
								<li><img src="./Home_files/slide01.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide02.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide03.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide04.jpg" width="1200"
									height="400" alt=""></li>
							</ul>
						</div>
						<p class="photo-credits">
							Photos by <a href="http://www.linkedin">Marc Wiegelmann</a>
						</p>
						<a href="#" class="prev prev-stage"><span>&lsaquo;</span></a> <a
							href="#" class="next next-stage"><span>&rsaquo;</span></a>
					</div>

					<div class="navigation">
						<a href="#" class="prev prev-navigation">&lsaquo;</a> <a href="#"
							class="next next-navigation">&rsaquo;</a>
						<div class="carousel carousel-navigation">
							<ul>
								<li><img src="./Home_files/slide01.jpg" width="50"
									height="50" alt=""></li>
								<li><img src="./Home_files/slide02.jpg" width="50"
									height="50" alt=""></li>
								<li><img src="./Home_files/slide03.jpg" width="50"
									height="50" alt=""></li>
								<li><img src="./Home_files/slide04.jpg" width="50"
									height="50" alt=""></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- fine Slideshow 4 -->

				<hr>
			</div>
			<!--fine bit-1 -->
		</div>
	</div>
	<!-------------------------fine frame ------------------------------->


	<!-------------------------fine frame ------------------------------->



	<div class="frame">
		<div class="bit-3 ">
			<div class="spot2">
				<p class="txt03">reparto prestampa</p>
				<a href="I nostri Reparti.jsp"><img
					src="./Home_files/prestampa-bn.png" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
		<div class="bit-3 ">
			<div class="spot2">
				<p class="txt03">stampa offset</p>
				<a href="I nostri Reparti.jsp"><img
					src="./Home_files/stampa-offset-bn.png" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
		<div class="bit-3 ">
			<div class="spot2 noborder">
				<p class="txt03">stampa digitale</p>
				<a href="I nostri Reparti.jsp"><img
					src="./Home_files/stampa-digitale-bn.png" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
	</div>
	<!-------------------------fine frame ------------------------------->

	<div class="frame">
		<div class="bit-1">
			<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
			<div
				style="clear: both; height: 50px; overflow: hidden; background-color: #FFF;"></div>
			<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

		</div>
		<!--fine bit-1 -->
	</div>
	<!-------------------------fine frame ------------------------------->

	<div class="frame">
		<div class="bit-4">
			<div class="spot3">
				<p class="txt04">per il punto vendita</p>
				<a href="Prodotti.jsp"><img src="./Home_files/home4.jpg" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
		<div class="bit-4">
			<div class="spot3">
				<p class="txt04">prodotti marketing</p>
				<a href="Prodotti.jsp"><img src="./Home_files/home5.jpg" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
		<div class="bit-4">
			<div class="spot3">
				<p class="txt04">per fiere ed eventi</p>
				<a href="Prodotti.jsp"><img src="./Home_files/home6.jpg" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
		<div class="bit-4">
			<div class="spot3">
				<p class="txt04">realizzazione by Unico</p>
				<a href="Prodotti.jsp"><img src="./Home_files/home7.jpg" alt=""></a>
			</div>
			<!-- fine SPOT -->
		</div>
	</div>
	<!-------------------------fine frame ------------------------------->

	<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
	<div style="clear: both; height: 50px; overflow: hidden"></div>
	<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

	<div class="frame">
		<div class="bit-3 ">
			<div class="spot-home">
				<img src="./Home_files/stampa-offset.png" class="icon" alt="">
				<p class="txt01">
					Aggiornamento<br> tecnologico
				</p>
				<p class="txt02">
					Costante, quotidiano, per lasciare<br> pi� spazio al pensiero
				</p>
			</div>
		</div>
		<!-- fine SPOT -->

		<div class="bit-3 ">
			<div class="spot-home">
				<img src="./Home_files/patrimonio-culturale.png" class="icon" alt="">
				<p class="txt01">
					La storia la fanno le sfide<br> che abbiamo vinto
				</p>
				<p class="txt02">Dal 1988 si fa carico delle esigenze di imprese
					e agenzie, trasformando sfide tecniche in prodotti unici.</p>
			</div>
		</div>
		<!-- fine SPOT -->

		<div class="bit-3 ">
			<div class="spot-home nobg">
				<img src="./Home_files/problem-solving.png" class="icon" alt="">
				<p class="txt01">
					Problem<br> solving
				</p>
				<p class="txt02">
					Ascoltare le necessit� espresse,<br> interpretare quelle
					inespresse.
				</p>
			</div>
		</div>
		<!-- fine SPOT -->
	</div>

	<!-------------------------fine frame ------------------------------->



	<div class="frame">
		<div class="bit-1">
			<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
			<div
				style="clear: both; height: 50px; overflow: hidden; background-color: #FFF;"></div>
			<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
		</div>
		<!--fine bit-1 -->

	</div>
	<!-------------------------fine frame ------------------------------->






	<!----------------------------------- FOOTER ------------------------------------------>
	<div class="frame">
		<!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


		<div id="footer">

			<div class="bit-4">
				<div class="spot7">
					<p class="txt09-logo">
						s|g<br>stampa grafica
					</p>

					<p class="txt10">Stampa Grafica vive di sfide. Nasce nel 1988
						proprio da una sfida di Guido Santi, cresciuto tra macchine
						tipografiche e sistemi di stampa. Oggi, i progetti pi� complessi e
						le sfide alla creativit� trovano la soluzione pi� consona, nei
						tempi stabiliti.</p>
				</div>
				<!-- fine SPOT -->
			</div>


			<div class="bit-4">
				<div class="spot7">
					<p class="txt09">I FOCUS PRINCIPALI</p>
					<hr class="linea">
					<p class="txt10">
						Prestampa<br> Stampa Offset<br> Stampa Digitale<br>
						Unico - stampa di prototipi, mock up e pezzi unici<br>
						Collaborazione con agenzie creative<br> Consulenza di stampa
					</p>
				</div>
				<!-- fine SPOT -->
			</div>


			<div class="bit-4">
				<div class="spot7">
					<p class="txt09">CONTATTI</p>
					<hr class="linea">
					<p class="txt10">
						Stampa Grafica Srl<br> Via Zambona, 12<br> 37031 Illasi
						VR<br> P.I. 03053960237<br> tel. +39 045.7830311<br>
						mail. info@coronasrl.it
					</p>
				</div>
				<!-- fine SPOT -->
			</div>
			<!-- DOWNLOAD -->
			<div class="bit-4">
				<div class="spot7">
					<p class="txt09">DOWNLOAD</p>
					<hr class="linea">
					<p class="txt10"></p>
					<div align="left" id="fileDownload">
						<a href="#" id="download" name="download" target="_blank"
							class="downloadFile" data-fancybox-type="iframe">
							<p class="txt10">
								<strong>Download PDF Schema Lavori 1<strong></strong></strong>
							</p>
						</a> <a href="#" id="download" name="download" target="_blank"
							class="downloadFile" data-fancybox-type="iframe">
							<p class="txt10">
								<strong>Download PDF Schema Lavori 2<strong></strong></strong>
							</p>
						</a> <a href="#" id="download" name="download" target="_blank"
							class="downloadFile" data-fancybox-type="iframe">
							<p class="txt10">
								<strong>Download PDF Schema Lavori 3</strong>
							</p>
						</a> <a href="#" id="download" name="download" target="_blank"
							class="downloadFile" data-fancybox-type="iframe">
							<p class="txt10">
								<strong>Download PDF Schema Lavori 4<strong></strong></strong>
							</p>
						</a>

						<div id="dialog" title="Download" style="display: none">
							<form name="emailForm" id="emailForm"
								action="/EnricoApp/DownloadServlet" method="post"
								style="display: none">

								<p>Inserisci il tuo indirizzo email per scaricare il pdf ed
									essere ricontattato</p>
								<input type="text" id="email" name="email">
								<button type="submit" id="submitEmail" name="submitEmail"
									value="Scarica il file" style="height: 50px; width: 200px">Scarica
									il file</button>
							</form>
						</div>


					</div>

				</div>
				<!-- fine SPOT -->
			</div>

			<div class="bit-4">
				<div class="spot7">
					<p class="txt09">CREDITS</p>
					<hr class="linea">
					<p class="txt10">
						Sito realizzato da <a href="http://www.linkedin.it/"
							target="_blank">Andrea Giraldi</a>
					</p>
					<p class="txt10">
						<a href="InformativaCookies.html" target="_blank">Cookie
							policy</a><span></span>
					</p>
				</div>
				<!-- fine SPOT -->
			</div>


		</div>

		<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
		<div style="clear: both; height: 60px; overflow: hidden"></div>
		<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
	</div>
	<!------------------------- fine frame ------------------------------->
	<!-------------------------------- fine FOOTER ---------------------------->



	<script src="./Home_files/animenu.js" type="text/javascript"></script>
	<script src="./Home_files/divascookies.js" type="text/javascript"></script>
	<link href="./Home_files/divascookies.css" rel="stylesheet"
		type="text/css">
	<link href="./Home_files/animenu.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./Home_files/divascookies.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.DivasCookies();
		});
	</script>


	<div class="divascookies"
		style="bottom: -100%; top: auto; display: none;">
		<div class="divascookies-banner-container">
			<p class="divascookies-banner-text">
				Questo sito o gli strumenti terzi da questo utilizzati si avvalgono
				di cookie necessari al funzionamento. Chiudendo questo banner,
				scorrendo questa pagina, cliccando su un link o proseguendo la
				navigazione in altra maniera, acconsenti all'uso dei cookie. <span
					class="divascookies-policy-link"> <a href="#"
					onclick='window.open(InformativaCookies.html);return false;'
					target="_blank">Informativa</a></span>
			</p>
			<div class="divascookies-accept-button-container">
				<p class="divascookies-accept-button-text">Ok</p>
			</div>
		</div>
	</div>
</body>
</html>
