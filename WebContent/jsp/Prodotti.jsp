<!DOCTYPE html>
<!-- saved from url=(0046)https://www.stampagrafica.it/ita/prodotti.aspx -->
<html class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link href="./I nostri Prodotti_files/css" rel="stylesheet" type="text/css">
<link href="./I nostri Prodotti_files/stampagrafica.css" rel="stylesheet" type="text/css">
<link href="./I nostri Prodotti_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./I nostri Prodotti_files/glide.css" rel="stylesheet" type="text/css">
<link href="./I nostri Prodotti_files/jquery.fancybox.css" rel="stylesheet" type="text/css">
<link href="./Home_files/connected-carousels.css" rel="stylesheet"
	type="text/css">


<script async="" src="./I nostri Prodotti_files/analytics.js.download"></script><script type="text/javascript" src="./I nostri Prodotti_files/jquery.min.js.download"></script>
<script src="./I nostri Prodotti_files/jquery.fancybox.js.download"></script>
<script type="text/javascript" src="./I nostri Prodotti_files/glide.js.download"></script>
<script type="text/javascript" src="./Home_files/jquery.jcarousel.js"></script>

<!-- SLIDER -->

<script type="text/javascript">

(function($) {
	// This is the connector function.
	// It connects one item from the navigation carousel to one item from the
	// stage carousel.
	// The default behaviour is, to connect items with the same index from both
	// carousels. This might _not_ work with circular carousels!
	var connector = function(itemNavigation, carouselStage) {
		return carouselStage.jcarousel('items').eq(itemNavigation.index());
	};

	$(function() {
		// Setup the carousels. Adjust the options for both carousels here.
		var carouselStage = $('.carousel-stage').jcarousel();
		var carouselNavigation = $('.carousel-navigation').jcarousel();

		// We loop through the items of the navigation carousel and set it up
		// as a control for an item from the stage carousel.
		carouselNavigation.jcarousel('items').each(function() {
			var item = $(this);

			// This is where we actually connect to items.
			var target = connector(item, carouselStage);

			item.on('jcarouselcontrol:active', function() {
				carouselNavigation.jcarousel('scrollIntoView', this);
				item.addClass('active');
			}).on('jcarouselcontrol:inactive', function() {
				item.removeClass('active');
			}).jcarouselControl({
				target : target,
				carousel : carouselStage
			});
		});

		// Setup controls for the stage carousel
		$('.prev-stage').on('jcarouselcontrol:inactive', function() {
			$(this).addClass('inactive');
		}).on('jcarouselcontrol:active', function() {
			$(this).removeClass('inactive');
		}).jcarouselControl({
			target : '-=1'
		});

		$('.next-stage').on('jcarouselcontrol:inactive', function() {
			$(this).addClass('inactive');
		}).on('jcarouselcontrol:active', function() {
			$(this).removeClass('inactive');
		}).jcarouselControl({
			target : '+=1'
		});

		// Setup controls for the navigation carousel
		$('.prev-navigation').on('jcarouselcontrol:inactive', function() {
			$(this).addClass('inactive');
		}).on('jcarouselcontrol:active', function() {
			$(this).removeClass('inactive');
		}).jcarouselControl({
			target : '-=1'
		});

		$('.next-navigation').on('jcarouselcontrol:inactive', function() {
			$(this).addClass('inactive');
		}).on('jcarouselcontrol:active', function() {
			$(this).removeClass('inactive');
		}).jcarouselControl({
			target : '+=1'
		});

		$('.carousel').jcarousel({
			 wrap: 'circular',
			 scroll : 1
		}).jcarouselAutoscroll({
			interval : 5000,
			target : '+=1',
			autostart : true
		}).hover(function() {
		    $(this).jcarouselAutoscroll('stop');
		}, function() {
		    $(this).jcarouselAutoscroll('start');

		});
	});
})(jQuery);

$('.jcarousel').jcarousel({
	wrap : 'circular'
}).jcarouselAutoscroll({
	interval : 3000,
	 target: '+=1'
});

</script>

<title>I nostri Prodotti</title>

<!-- GOOGLE ANALITYCS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

<!-- FAncybox per aprire popup su immmagine -->
<script>
$(document).ready(function() {
    $(".iframe").fancybox({
        type: 'iframe'
    });
});
</script>

<script type="text/javascript">
		$(document).ready(function() {
			/* funzione per aprire pagine in HTML */
			$(".iframe").fancybox({
        	type: 'iframe'
   			 });
	
			/* funzione per aprire popup immagine con caption */
			$('.fancybox').fancybox();
			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,
				openEffect : 'none',
				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

		});
	</script>
    
<style type="text/css">.fancybox-margin{margin-right:17px;}</style>
<style type="text/css" media="screen">

.connected-carousels .prev-navigation {
    left: -220px;
    top: -50px;
    text-indent: 6px;
</style>
<style type="text/css" media="screen">
.connected-carousels .next-navigation {
    right: -220px;
    top: -50px;
    text-indent: 20px;
}

</style>
</head>
<body>
<div class="frame">
  <div class="bit-1">
    <div id="topbar">
      <div class="quickcontact">
        <p class="vcenter"><strong>Contatti veloci:</strong><br>
          tel. 045.7830311<br>
          info@coronasrl</p>
      </div>
      <div class="lingue"><p class="vcenter"><a href="./Home_files/ITA/index.html">ITA</a> | ENG</p></div>


      <div class="logo"><a href="Home.jsp"><img src="./Home_files/Logo-CoronaSrl.jpg" width="250" height="100"></a></div>
    </div>
    <!-- fine topbar --> 
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 centertxt">
    <nav class="animenu" id="nav"> 
  <button class="animenu__toggle">
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
  </button>
  <ul class="animenu__nav">
   
   
    <li>
      <a href="Azienda.jsp">azienda</a>
      <ul class="animenu__nav__child">
        <li><a href="I nostri Reparti.jsp">i nostri reparti</a></li>
        <li><a href="Possibilita.jsp">i nostri clienti</a></li>
       </ul>
    </li> 
    <li>
      <a href="Prodotti.jsp">prodotti</a>
    </li>    
    <li>
      <a href="ProdottiEServizi.jsp">servizi</a>
     </li>
    <li>
      <a href="News.jsp">news</a>
    </li>
    <li>
      <a href="Contatti.jsp">contatti</a>
    </li>   
    
                     
  </ul>
</nav>


  </div>
</div>
<div class="frame">
  <div class="bit-1"> 
    
   
    
  </div>
  <!--fine bit-1 --> 
</div>

<div align="left" id="centratore">
		<div class="frame">
			<div class="wrapper">
				<div class="connected-carousels">
					<div class="stage">
						<div class="carousel carousel-stage">
							<ul>
								<li><img src="./Home_files/slide01.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide02.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide03.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide04.jpg" width="1200"
									height="400" alt=""></li>
							</ul>
						</div>
						<a href="#" class="prev prev-stage" style=""><span>&lsaquo;</span></a> <a
							href="#" class="next next-stage"><span>&rsaquo;</span></a>
					</div>

					<div class="navigation">
						<a href="#" class="prev prev-navigation">&lsaquo;</a> <a href="#"
							class="next next-navigation">&rsaquo;</a>
				<!-- fine Slideshow 4 -->
				</div>
				</div>
				</div>
				</div>
				</div>


<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-4 ">
    <div class="spot">
      <p class="txt01">per il punto vendita</p>
      <p class="txt02bn">brochure / espositori / pack prodotto decorazioni adesive per pavimento rotair da soffitto / voucher / volantini pannelli visual</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-4 ">
    <div class="spot">
      <p class="txt01">prodotti marketing</p>
      <p class="txt02bn">brochure prodotto / presentoir</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-4 ">
    <div class="spot">
      <p class="txt01">fiere ed eventi</p>
      <p class="txt02bn">espositori / voucher / cartoline gratta e vinci / gadget / allestimenti e visual / arredi temporanei di cartone / press kit</p>
    </div>
  </div>
  <!-- fine SPOT --> 
  
   <div class="bit-4 ">
    <div class="spot nobg">
      <p class="txt01">realizzazioni by Unico</p>
      <p class="txt02bn">giocattoli in cartone / arredi in cartone / complementi d'arredo in cartone o legno / punto vendita / oggettistica e marketing / mobili / promo</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 payoffbox">
    <p class="payoff">Stampa Grafica raccontata dai nostri lavori</p>
  </div>
  <!--fine bit-1 -->
  
  <!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:50px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">

   <div id="box-filter">
    <ol id="filters">
      <li data-filter="all" class="first-filter">tutti i prodotti</li>
      <li data-filter="vendita">per il punto vendita</li>
      <li data-filter="marketing">prodotti marketing</li>
      <li data-filter="eventi">fiere ed eventi</li>
      <li data-filter="unico">realizzazioni by Unico</li>
    </ol>
 </div><!-- fine BOX FILTER -->


    <div id="main" role="main" style="height: 4067px;">

      <ul id="tiles">
<!-- link a pagina -->
<li data-filter-class='["unico", "all"]'><a class="fancybox fancybox.iframe"  href="./I nostri Prodotti_files/017.jpg"> <img src="./I nostri Prodotti_files/017.jpg"></a></li>
           
<li data-filter-class="[&quot;unico&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/017.jpg" title="Culla a dondolo in cartone smontabile e personalizzabile"><img src="./I nostri Prodotti_files/017.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/027.jpg" title="Desk in cartone per punto vendita con palo per esposizione cartelli decorativi a freccia"><img src="./I nostri Prodotti_files/027.jpg"></a></li>
    
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/034.jpg" title="Espositore per prodotti da terra, con ripiani inclinati e gancio per gruccia appendi-abiti"><img src="./I nostri Prodotti_files/034.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 406px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/047-2.jpg" title="Cartello vetrina con anima in polistirolo, e base rialzata con ripianto portante per l&#39;esposizione di prodotti"><img src="./I nostri Prodotti_files/047-2.jpg"></a></li>

<li data-filter-class="[&quot;unico&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 406px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/055.jpg" title="Poltrona personalizzata, realizzata in cartone alveolare ignifugo"><img src="./I nostri Prodotti_files/055.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 465px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/070.jpg" title="Cartello vetrina imbustato, con tasca in plastica trasparente porta depliant"><img src="./I nostri Prodotti_files/070.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 691px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/075.jpg" title="Cartelli vetrina in PVC semiespanso, con piedini in PVC ad incastro"><img src="./I nostri Prodotti_files/075.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 780px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/082.jpg" title="Cartello vetrina bifacciale snodato con anima in polistirolo."><img src="./I nostri Prodotti_files/082.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 860px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/085.jpg" title="Cartello vetrina bifacciale realizzato con anima in laminil sottile e piedini ad incastro in plexiglass trasparente"><img src="./I nostri Prodotti_files/085.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 915px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/088.jpg" title="Cartello vetrina bifacciale realizzato con animain polistirolo da 2 cm e piedini ad incastro in plexiglass trasparente"><img src="./I nostri Prodotti_files/088.jpg"></a></li>
     
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 928px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/098.jpg" title="Cubi in cartone con bordi risvoltati per esposizione in vetrina"><img src="./I nostri Prodotti_files/098.jpg"></a></li>
 
<li data-filter-class="[&quot;unico&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1162px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/112.jpg" title="Elementi scenografici per allestimenti, realizzati in cartone avana tripla onda"><img src="./I nostri Prodotti_files/112.jpg"></a></li> 
 
<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1186px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/083.jpg" title="Trofeo personalizzato, realizzato in plexiglass trasparente stampato e alluminio composito satinato e a specchio con incisioni"><img src="./I nostri Prodotti_files/083.jpg"></a></li> 

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1204px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/079.jpg" title="Trofeo personalizzato, realizzato in plexiglass trasparente inciso a laser con basamento in plexiglass nero lucido"><img src="./I nostri Prodotti_files/079.jpg"></a></li> 
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1204px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/087.jpg" title="Cubo per attività di Guerrilla Marketing. Realizzato in PVC trasparente stampato, con chiusura a incastro e cordino in nylon trasparente per fissaggio agli specchietti retrovisori delle auto"><img src="./I nostri Prodotti_files/087.jpg"></a></li>
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1436px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/088.jpg" title="Piramide per attività di Guerrilla Marketing. Con nastro in nylon trasparente, chiusura a incastro e messaggio promozionale in pergamena inserito all&#39;interno"><img src="./I nostri Prodotti_files/088.jpg"></a></li> 
 
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1442px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/093.jpg" title="Cartello da banco in 3 sezioni snodate, realizzato con anima in cartone da 3 mm"><img src="./I nostri Prodotti_files/093.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1508px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/120.jpg" title="Kit di 3 pannelli da appendere, realizzati in materiale composito leggero, con bordi risvoltati"><img src="./I nostri Prodotti_files/120.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1649px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/123.jpg" title="Porta prodotti da indossare a spalle, per la distribuzione durante un evento di campioni di detersivo"><img src="./I nostri Prodotti_files/123.jpg"></a></li>
 
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1707px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/125.jpg" title="Espositore vetrina con frontale sagomato in laminil e ripiani portanti per l&#39;esposizione di prodotti"><img src="./I nostri Prodotti_files/125.jpg"></a></li> 
 
<li data-filter-class="[&quot;unico&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1713px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/129.jpg" title="Sgabelli personalizzabili in cartone avana tripla onda"><img src="./I nostri Prodotti_files/129.jpg"></a></li>  
 
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1717px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/134.jpg" title="Alzatine espositive per prodotti e cartelli da banco sagomati, realizzati in cartone alveolare ignifugo"><img src="./I nostri Prodotti_files/134.jpg"></a></li>  
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1856px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/136.jpg" title="Cofanetto promozionale cilindrico, con coperchi decorati con stampa. Realizzazione di un effetto " soft="" touch"="" per="" simulare="" la="" gomma="" tatto,="" verniciatura="" lucida="" a="" rilievo="" su="" particolari.="" alveare="" interno="" l'esposizione="" di="" prodotti="" promozionali"=""><img src="./I nostri Prodotti_files/136.jpg"></a></li> 
 
<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1948px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/140.jpg" title="Espositore da banco con tasca porta depliant e inserimento di un elemento del calcio balilla"><img src="./I nostri Prodotti_files/140.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 1950px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/145.jpg" title="Espositore da terra per la GDO. Progettato per essere esposto inserendolo all&#39;interno di expo-pallet per uova di Pasqua"><img src="./I nostri Prodotti_files/145.jpg"></a></li>

<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2137px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/149.jpg" title="Mailing di lancio nuova MINI. Il prodotto consisteva nella simulazione realistica di una &#39;bomba&#39;, con timer riportante il countdown per il lancio mondiale del nuovo modello"><img src="./I nostri Prodotti_files/149.jpg"></a></li>

<li data-filter-class="[&quot;unico&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2137px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/173.jpg" title="Kit di sedia e tavolini in cartone da costruire, utilizzati per attività ricreative con bambini. Tutta la superficie degli elementi è completamente personalizzabile e colorabile"><img src="./I nostri Prodotti_files/173.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2166px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/179.jpg" title="Espositore da banco in cartone micronda, con tasca sagomata tridimensionale"><img src="./I nostri Prodotti_files/179.jpg"></a></li>

<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2357px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/001.jpg" title="Confezione regalo in cartone rivestito, con apertura a croce e chiusura con calamite sepolte"><img src="./I nostri Prodotti_files/001.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2369px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/006.jpg" title="Lunch box personalizzato, composto da sacchetto in carta kraft personalizzato con stampa digitale e fascetta di chiusura stampata"><img src="./I nostri Prodotti_files/006.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2490px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/015.jpg" title="Cartella press-kit, con blocco per appunti e schede informative inserite all&#39;interno. Realizzata in cartone rivestito con tessuto stampato a caldo"><img src="./I nostri Prodotti_files/015.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2500px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/025.jpg" title="Cartella press-kit realizzata in cartone rivestito, con all&#39;interno applicata un&#39;anima porta gadget in cartone alveolare"><img src="./I nostri Prodotti_files/025.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2583px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/013.jpg" title="Cartella press-kit realizzata in cartone rivestito con carta stampata in colori fluorescenti, con all&#39;interno applicata un&#39;anima porta gadget in neoprene bianco"><img src="./I nostri Prodotti_files/013.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2641px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/036.jpg" title="Cartella press-kit realizzata in cartoncino plastificato. All&#39;interno sono inseriti dei vassoi porta gadget"><img src="./I nostri Prodotti_files/036.jpg"></a></li>

<li data-filter-class="[&quot;eventi&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2748px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/040.jpg" title="Lunch kit personalizzato composto da bicchiere in plastica personalizzato con adesivo e confezioni per patatine e hamburger in cartoncino plastificato per alimenti"><img src="./I nostri Prodotti_files/040.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 2838px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/161.jpg" title="Cartello da banco in 3 sezioni snodate, realizzato con anima in cartone, rivestito in tessuto e carta stampata"><img src="./I nostri Prodotti_files/161.jpg"></a></li>

<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3001px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/171.jpg" title="Valigetta porta prodotti con maniglia in plastica e alveare interno"><img src="./I nostri Prodotti_files/171.jpg"></a></li>

<li data-filter-class="[&quot;vendita&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3013px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/004.jpg" title="Espositore da banco realizzato incartone alveolare avana stampato"><img src="./I nostri Prodotti_files/004.jpg"></a></li>

<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3026px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/041.jpg" title="Confezione promozionale con alveare interno porta prodotto, tasca per l&#39;inserimento di un depliant illustrativo e finestra sagomata sul fronte"><img src="./I nostri Prodotti_files/041.jpg"></a></li>

<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3163px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/047.jpg" title="Confezione promozionale realizzata in plastica trasparente stampata con fondo bianco e vernice protettiva lucida"><img src="./I nostri Prodotti_files/047.jpg"></a></li>
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3242px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/052.jpg" title="Confezione promozionale realizzata in plastica stampata con fondo bianco e vernice protettiva lucida. Realizzazione di nicchie per la presentazione dei prodotti"><img src="./I nostri Prodotti_files/052.jpg"></a></li>
  
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3446px; left: 339px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/054.jpg" title="Confezione regalo per Natale, realizzata in cartoncino plastificato lucido con stampa a caldo argento lucido"><img src="./I nostri Prodotti_files/054.jpg"></a></li> 
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3525px; left: 37px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/066.jpg" title="Depliant prodotto sagomato, rilegato con cucitura a punto singer"><img src="./I nostri Prodotti_files/066.jpg"></a></li> 
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3557px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/063.jpg" title="Depliant prodotto sagomato, rilegato con vite cromata"><img src="./I nostri Prodotti_files/063.jpg"></a></li>  
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3678px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/072.jpg" title="Blister porta calze realizzati in cartoncino con plastificazione metallizzata argento e sovrastampa di bianco, quadricromia e vernice protettiva lucida"><img src="./I nostri Prodotti_files/072.jpg"></a></li>  
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3754px; left: 943px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/076.jpg" title="Espositre da banco con crowner sagomato e tasca porta depliant"><img src="./I nostri Prodotti_files/076.jpg"></a></li>  
 
<li data-filter-class="[&quot;marketing&quot;, &quot;all&quot;]" style="display: list-item; position: absolute; top: 3877px; left: 641px;"><a class="fancybox-effects-c" href="./I nostri Prodotti_files/048.jpg" title="Packaging prodotto composto da alveare e fascia di chiusura. Realizzato con carta con goffratura a effetto legno"><img src="./I nostri Prodotti_files/048.jpg"></a></li>   
 


        
        <!-- End of grid blocks -->
</ul>

<div class="wookmark-placeholder" style="position: absolute; display: block; left: 37px; top: 4051px; width: 300px; height: 14px;"></div><div class="wookmark-placeholder" style="position: absolute; display: block; left: 339px; top: 3882px; width: 300px; height: 183px;"></div><div class="wookmark-placeholder" style="position: absolute; display: none; left: 641px; top: 4067px; width: 300px; height: 0px;"></div><div class="wookmark-placeholder" style="position: absolute; display: block; left: 943px; top: 3908px; width: 300px; height: 157px;"></div></div>

</div><!-- fine FRAME -->



<!----------------------------------- FOOTER ------------------------------------------>
<div class="frame">
  <!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


<div id="footer">

            <div class="bit-4">
                <div class="spot7">
                <p class="txt09-logo">s|g<br>stampa grafica</p>
                
                <p class="txt10">Stampa Grafica vive di sfide.
Nasce nel 1988 proprio da una sfida di Guido
Santi, cresciuto tra macchine tipografiche e
sistemi di stampa. Oggi, i progetti più complessi e
le sfide alla creatività trovano la soluzione più
consona, nei tempi stabiliti.</p>
              </div><!-- fine SPOT -->
             </div>
             
             
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">I FOCUS PRINCIPALI</p>
                <hr class="linea">
                <p class="txt10">Prestampa<br>
Stampa Offset<br>
Stampa Digitale<br>
Unico - stampa di prototipi, mock up e pezzi unici<br>
Collaborazione con agenzie creative<br>
Consulenza di stampa</p>
              </div><!-- fine SPOT -->
            </div>
            
            
              
            
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">CONTATTI</p>
                <hr class="linea">
                <p class="txt10">Stampa Grafica Srl<br>
Via Zambona, 12<br>
37031 Illasi VR<br>
P.I. 03053960237<br>
tel. +39 045.7830311<br>
mail. info@stampagrafica.it</p>
              </div><!-- fine SPOT -->
            </div>
            
             <div class="bit-4">
              <div class="spot7">
                <p class="txt09">CREDITS</p>
                <hr class="linea">
                <p class="txt10">Sito realizzato da <a href="http://www.linkedin.it/" target="_blank">Andrea Giraldi</a></p>
                <p class="txt10"><a href="InformativaCookies.html" target="_blank">Cookie policy</a></p>
              </div><!-- fine SPOT -->
            </div>
            
            
</div>

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:60px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame -------------------------------> 
<!-------------------------------- fine FOOTER ----------------------------> 

<!-- Include the imagesLoaded plug-in -->
  <script src="./I nostri Prodotti_files/jquery.imagesloaded.js.download"></script>

  <!-- Include the plug-in -->
  <script src="./I nostri Prodotti_files/jquery.wookmark.js.download"></script>

  <!-- Once the page is loaded, initalize the plug-in. -->
  <script type="text/javascript">
    (function ($){
      $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
        var options = {
          autoResize: true, // This will auto-update the layout when the browser window is resized.
          container: $('#main'), // Optional, used for some extra CSS styling
         offset: 2, // Optional, the distance between grid items
          outerOffset: 2, // Optional, the distance to the containers border
         
          fillEmptySpace: true // Optional, fill the bottom of each column with widths of flexible height
        };

        // Get a reference to your grid items.
        var handler = $('#tiles li'),
            filters = $('#filters li');

        // Call the layout function.
        handler.wookmark(options);

        /**
         * When a filter is clicked, toggle it's active state and refresh.
         */
        var onClickFilter = function(event) {
          var item = $(event.currentTarget),
              activeFilters = [];

          if (!item.hasClass('active')) {
            filters.removeClass('active');
          }
          item.toggleClass('active');

          // Filter by the currently selected filter
          if (item.hasClass('active')) {
            activeFilters.push(item.data('filter'));
          }

          handler.wookmarkInstance.filter(activeFilters);
        }

        // Capture filter click events.
        filters.click(onClickFilter);
      });
    })(jQuery);
  </script>
  

<script src="./I nostri Prodotti_files/animenu.js.download"></script>

</body></html>