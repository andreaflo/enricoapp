<%@ page import="java.util.*"%>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*,it.realt.webapps.utente.dbbeans.*,it.realt.webapps.utente.mvc.control.*,java.util.*,it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*,it.realt.webapps.beans.*,java.text.*"%>
<%@ page import="actions.contatto.*"%>
<%@ page import="dbobjects.*"%>
<%@ page import="it.realt.webapps.anagrafica.beans.FiltroAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.ModuloAnagrafica.Tipo"%>
<%@ page import="it.realt.webapps.anagrafica.dbbeans.DbAnagrafica"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ include file="/jsp/commons/tipoAnagrafica.jspf"%>
<tags:sicurezza idFunzione="listaAnagrafiche" />
<jsp:useBean id="listaHtml" class="it.realt.webapps.html.ListaHtml"	scope="page" />
<jsp:useBean id="pag" class="it.realt.webapps.html.Paginazione"	scope="page" />

<%

String idcontatto = request.getParameter("idcontatto");
Contatto item = null;


if (idcontatto == null) {
	item = (Contatto) session.getValue("dettaglioContatto");
	
}


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script language="javascript">
	function modifica() {
		document.forms[0].submit();
	}

	function cancella(idAnag) {
		document.cancellazione.idAnagrafica.value = idAnag;
		document.cancellazione.submit();
	}

	function inserisci() {
	}
</script>
<script language="JavaScript" type="text/javascript">

function apri(a){
	window.open(a, "", "width=560, height=377, status=1, resizable=1");
}

</script>
<head>
<title>Dettaglio Anagrafica</title>
</head>
<link rel="stylesheet" href="<%=context%>/css/style.css" type="text/css"></link>
<link rel="stylesheet" href="<%=context%>/css/stylepartedinamica.css" type="text/css"></link>
<body>
<!--   <tags:menuFunzioni root="root" /> -->
<br><div align="center" class="titolo">Dettaglio contatto</div><br>

<div align="center">
<form action="<%=PORTIERE%>" method="POST">

<table class="lista">
	<TR>
		<TD>
		<table>
			<tr ALIGN="LEFT">
				<th class="primo1">Id Contatto</th>
				<td align="left"><span><%=item.getIdcontatto()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Ragione Sociale</th>
				<td align="left"><span><%=item.getRagionesociale()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Nominativo</th>
				<td align="left"><span><%=item.getNominativo()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Email</th>
				<td align="left"><span><%=item.getEmail()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Telefono</th>
				<td align="left"><span><%=item.getTelefono()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Testo</th>
				<td align="left"><span><%=item.getTesto()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Stato Record</th>
				<td align="left"><span><%=item.getStato()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
		</table>
		</td>
	</tr>
</table>
<table align=center>
	<tr>
		<td>
		   <a href="contattoList.jsp?tipo=<%=tipo.getCodice()%>"><img src="<%=context%>/img/pulsanti/indietro.png" border="0" alt="indietro" title="indietro"></a>
		</td>
	</tr>
</table>

<input type="hidden" name="idContatto" value="<%=item.getIdcontatto()%>">
<input type="hidden" name="targetpage" value="backoffice/ContattoForm.jsp?tipo=<%=tipo.getCodice()%>">
<input type="hidden" name="callpage" value="errore.jsp">
<input type="hidden" name="codoper" value="dettaglioContatto"></form>
</div>
</body>
</html>