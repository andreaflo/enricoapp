
<%@page import="it.realt.util.GeneralComparator"%>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*, it.realt.webapps.utente.dbbeans.*, it.realt.webapps.utente.mvc.control.*, java.util.*, it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*, it.realt.webapps.beans.*, java.text.*"%>
<%@ page import="it.realt.webapps.anagrafica.beans.Anagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.beans.FiltroAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.internet.ops.OpAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.ModuloAnagrafica.Tipo"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ include file="/jsp/commons/navigazione.jspf"%>

<tags:sicurezza idFunzione="nuovoContatto"/>

<%pageContext.setAttribute("languages", Languages.getInstance(), PageContext.PAGE_SCOPE);%>
<jsp:useBean id="listaHtml" class="it.realt.webapps.html.ListaHtml" scope="page"/>
<jsp:useBean id="pag" class="it.realt.webapps.html.Paginazione" scope="page"/>
<jsp:useBean id="formValidator" class="it.realt.webapps.html.FormValidator" scope="page"/>

%>
<%out.clear();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="it.realt.webapps.utente.costanti.TipiUtente"%><html>
<head>
<title>Inserimento Contatto</title>

<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<tags:formValidation validator="${formValidator}" function="inviaDati" form="dati"/>
<tags:formValidation validator="${formValidator}" function="inviaCancellazione" form="cancellazione"/>

</head>
<body>

<!-- qui inizia  la parte della RICERCA -->
<br><div align="center" class="titolo">Ricerca</div><br>
<div align="center">
 

<table>
<tr>
<td>
<h3> L'inserimento � andato a buon fine. Verrete ricontattati al piu' presto.</h3>
</td>
</tr>
<tr><td valign="bottom" align="center" height="30">
			<a href="<%=request.getContextPath()%>" target="_top"> <img alt="Indietro" title="Indietro" src="<%=request.getContextPath()%>/jsp/img/pulsanti/indietro.png" width="22" height="22" border="0" /></a> 
		</td></tr>
</table>
</div>
</body>
</html>
