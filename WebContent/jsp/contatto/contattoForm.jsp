<%@page import="dbobjects.Contatto"%>
<%@page import="it.realt.util.GeneralComparator"%>
<%@page import="it.realt.webapps.utils.dbbeans.DbLista"%>
<%@page import="it.realt.webapps.utils.dbbeans.dblista.FiltroDbLista"%>
<%@page import="it.realt.webapps.utils.internet.ops.OpLista"%>
<%@page import="it.realt.webapps.utente.internet.ops.OpUtente"%>
<%@page import="it.realt.webapps.utente.dbbeans.utente.FiltroUtente"%>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*, it.realt.webapps.utente.dbbeans.*, it.realt.webapps.utente.mvc.control.*, java.util.*, it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*, it.realt.webapps.beans.*, java.text.*"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>

<%
boolean nuova = "true".equals(request.getParameter("nuovo"));
String idFunzione="modificaContatto";
%>
<tags:sicurezza idFunzione="<%=idFunzione%>"/>


<%pageContext.setAttribute("languages", Languages.getInstance(), pageContext.PAGE_SCOPE);%>
<jsp:useBean id="formValidator" class="it.realt.webapps.html.FormValidator" scope="page"/>
<jsp:useBean id="dettaglioContatto" class="dbobjects.Contatto" scope="session"/>

<%@ include file="/jsp/commons/tipoAnagrafica.jspf"%>

<%
String codoper = nuova?"insertContatto":"modificaContatto";
if (nuova) {
	dettaglioContatto = new Contatto();
	pageContext.setAttribute("dettaglioContatto", dettaglioContatto ,pageContext.PAGE_SCOPE);
}

formValidator.setFormName("dati");

formValidator.addValidationRule(new SimpleValidationRule("testo","Descrizione necessaria!","notEmpty"));	
//formValidator.addValidationRule(new SimpleValidationRule("idcontatto","idContatto necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("ragionesociale","ragionesociale necessario!","notEmpty"));
formValidator.addValidationRule(new SimpleValidationRule("nominativo","nominativo necessario!","notEmpty"));
formValidator.addValidationRule(new SimpleValidationRule("email","Email necessario!","notEmpty"));
formValidator.addValidationRule(new SimpleValidationRule("telefono","Telefono necessario!","notEmpty"));
formValidator.addValidationRule(new SimpleValidationRule("stato","stato necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("dataInserimento","dataInserimento necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("partitaIva","partitaIva necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("codiceFiscale","codiceFiscale necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("altro","Altro necessario!","notEmpty"));

%>
<%out.clear();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=nuova?"Inserimento":"Modifica"%> Immagini</title>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<%=context%>/css/ui-lightness/jquery-ui-1.8.12.custom.css" type="text/css" media="all" />
<script type="text/javascript" src="<%=context%>/js/validator.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery.ui.datepicker-it.js"></script>
<script type="text/javascript" src="<%=context%>/js/application.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<tags:menuFunzioni root="root" />
<br><div align="center" class="titolo"><%=nuova?"Inserimento":"Modifica"%> Contatto</div><br>

<div align="center" <% if (!nuova) { %>style="float:left; margin-left: 50px;"<% } %> >
<form action="<%=PORTIERE%>" method="post" name="dati" id="dati" enctype="multipart/form-data">
<input type="hidden" name="codoper" value="<%=codoper%>" />
<input type="hidden" name="targetpage" value="contatto/contattoList.jsp?tipo=<%= tipo.getCodice() %>" />
<input type="hidden" name="callpage" value="<%=request.getRequestURL()%>" />
<input type="hidden" name="tipoAnagrafica" value="<%= tipo.getCodice() %>" />
<input type="hidden" name="nazione" value="IT">
<input type="hidden" name="lingua" value="ITA">
<input type="hidden" name="stato_record" value="A">
<table class="lista">

<%if (!nuova) {
			if (session.getAttribute("dettaglioContatto")!= null) {
			 String idAnagrafica = request.getParameter("idcontatto");
			// session.getAttribute("riferimenti_x_anagrafica");
			//pageContext.setAttribute("riferimenti_x_anagrafica", riferimenti_x_anagrafica);
			}
     %>
	<input type="hidden" name="idcontatto" value="<%=dettaglioContatto.getIdcontatto()%>" />
	<tr align="left">
        <td class="primo1"><div align="left">identificativo su Database</div></td>
        <td><div align="left" readonly=true ><tags:input bean="${dettaglioContatto}"  property="idcontatto" style="width :350px;"/></div></td>
      </tr>     
      <tr align="left">
        <td class="primo1"><div align="left">ragione Sociale</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="ragionesociale" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Nome e Cognome</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="nominativo" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">email</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="email"/></div></td>
      </tr>
            <tr align="left">
        <td class="primo1"><div align="left">telefono</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="telefono"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">testo</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="testo" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">stato</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="stato"/></div></td>
      </tr>
        <tr align="left">
        <td class="primo1"><div align="left">data</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="dataInserimento"/></div></td>
      </tr>
        <tr align="left">
        <td class="primo1"><div align="left">nota</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="notaAmministratore" style="width :350px;"/></div></td>
      </tr>
      
<%}else{%>

      <tr align="left">
        <td class="primo1"><div align="left">identificativo su Database</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="idcontatto" style="width :350px;"/></div></td>
      </tr>     
      <tr align="left">
        <td class="primo1"><div align="left">Ragione Sociale</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="ragionesociale" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Nome e Cognome</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="nominativo" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Email</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="email"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Telefono</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="telefono"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Testo</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="testo"/></div></td>
      </tr>
         <tr align="left">
        <td class="primo1"><div align="left">Stato</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="stato"/></div></td>
      </tr>
        <tr align="left">
        <td class="primo1"><div align="left">data</div></td>
           <td><div align="left"><input type="text" name="dataInserimento" class='date' /></div></td>
      </tr>
        <tr align="left">
        <td class="primo1"><div align="left">nota</div></td>
        <td><div align="left"><tags:input bean="${dettaglioContatto}"  property="notaAmministratore"/></div></td>
      </tr>
      <tr>
        <td class="primo1"><div align="left">Allegato Comodato d'uso</div></td>
		<td align="left"><input id="file_comodato" name="file_comodato" type="file" style="width:300px" editable="true"/></td>
	  </tr>
<%}%>	  
</table>
<br>
<table align="center"> 
  <tr align="left">
    <td colspan="2" align="center">
      <a href="javascript:go()"><img src="<%=context%>/img/pulsanti/conferma.png" value="<%=nuova?"Inserisci":"Modifica"%>" border="0" title="<%=nuova?"Inserisci":"Modifica"%>" alt="<%=nuova?"Inserisci":"Modifica"%>" /></a>
      <a href="contattoList.jsp?tipo=<%= tipo.getCodice() %>"> <img border="0" src="<%=context%>/img/pulsanti/indietro.png" alt="Indietro" title="Indietro" /> </a> 
     </td>
  </tr>
</table>
</form>
<tags:formValidation validator="${formValidator}" function="go"/>
</div>

<!--   MODIFICA -->


<form action="<%=PORTIERE%>" method="post" id="note" name="note">
<input type="hidden" name="codoper" value="insertNota" />
<input type="hidden" name="targetpage" value="contatto/contattoForm.jsp?tipo=<%= tipo.getCodice() %>" />
<input type="hidden" name="callpage" value="<%=request.getRequestURL()%>" />
<input type="hidden" name="idimmagine" value="<%=dettaglioContatto.getIdcontatto()%>" />
<table class="lista">
	
      <tr align="left">
        <td class="primo1"><div align="left">Nuova </div></td>
		
	  </tr>
	  <tr align="left">
	  <td>
        <div><textarea name="noteTextarea" id="noteTextarea"></textarea></div>
	  </td>
	  </tr>	  
</table>
<br>
<table align="center"> 
  <tr align="left">
    <td colspan="2" align="center">
      <a href="javascript:aggiungiNota()"><img src="<%=context%>/img/pulsanti/aggiungi.gif" width="20" height="20"  value="Inserisci Nota" border="0" title="Inserisci Nota" alt="Inserisci Nota" /></a>
      <a href="javascript:resettaText()"> <img border="0" src="<%=context%>/img/pulsanti/gomma.png" width="20" height="20" alt="Pulisci" title="Pulisci" /> </a> 
     </td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript" src="<%=context%>/js/validator.js">
</script>
<script type="text/javascript">

function aggiungiNota(){
	if (validate())
		document.note.submit();
}

function eliminaNota(ref){

	var sicuro=window.confirm("Sei sicuro di voler cancellare?");
	if(sicuro){
		ref.codoper.value = "eliminaNotaClienteXImmagine";
		ref.submit();
	}
}

function modificaNota(ref){
	ref.codoper.value = "modificaNotaClienteXImmagine";
	ref.submit();
}

function validate(){
	var text = document.note.noteTextarea;
	if (notEmpty(text))
		return true;
	else {
		alert("Annotazione vuota");
		return false;
	}
}

function resettaText(){
	var f=document.note;
	f.noteTextarea.value="";
	//f.dataVisita.value="";
}

</script>
</body>
</html>
