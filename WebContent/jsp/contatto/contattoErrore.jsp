<%@ page language="java" contentType="text/html" %>
<%@ page import="it.realt.general.BundledException" %>
<%@ page import="it.realt.webapps.Util" %>
<%@ page import="java.io.*" %>
<%@ page isErrorPage="true" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<% Exception e=(Exception)session.getValue("Exception");
   //"Assorbo" l'eccezione in modo che non venga ripresentata
   session.removeValue("Exception");
   //out.write(session.getValue("Exception"));
	//Throwable e = exception;
%>
<%out.clear();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Attenzione</title>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
</head>
<body>
<!-- Errore lanciato e non catturato:
<%
//Logger logger = Logger.getLogger(getClass());
StringWriter sw = new StringWriter();
PrintWriter pw = new PrintWriter(sw);
if (exception==null) exception = e;
if (exception!=null) exception.printStackTrace(pw);
while (exception instanceof BundledException && exception != null) {
	BundledException bex = (BundledException)exception;
	exception = bex.getNextException();
	if (exception!=null) exception.printStackTrace(pw);
}
%>
<%=sw.toString()%>
-->
<center>
	<table width="640" style="height: 90%">
	<tr><td align="center" valign="middle">
		<table border="0" cellpadding="4" cellspacing="1" width="640">
		<tr><td align="center" class="primo1"><b> Attenzione </b></td></tr>
		<tr><td>
			<%if (exception!=null) {%>
				<table border="0" width="100%"  >
				<tr><td valign="top" width="75%" align="center" height="20"> 
                    <table>
                    <tr><td align="center"> Rilevato il seguente problema: <br />
						<br />
						<%=exception.getMessage()%> 
						<%if (e instanceof BundledException) Util.debug((BundledException)e); %>
						<br />
						<br />
						<br />
					</td></tr>
                    </table>
				</td></tr>
				</table>
			<%}%>
		</td></tr>
		<tr><td valign="bottom" align="center" height="30">
			<a href="<%=request.getContextPath()%>" target="_top"> <img alt="Indietro" title="Indietro" src="<%=request.getContextPath()%>/jsp/img/pulsanti/indietro.png" width="22" height="22" border="0" /></a> 
		</td></tr>
		</table>
	</td></tr>
	</table>
</center>
</body>
</html>