<%-- <%@include  file="Contatti.html" %> --%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>

<!DOCTYPE html>
<!-- saved from url=(0046)https://www.stampagrafica.it/ita/contatti.aspx -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon" href="https://www.coronasrl.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.coronasrl.it/favicon.ico">
<link rel="icon" href="https://www.coronasrl.it/favicon.ico" type="image/x-icon">
<link href="./Contatti_files/css" rel="stylesheet" type="text/css">
<link href="./Contatti_files/stampagrafica.css" rel="stylesheet" type="text/css">
<link href="./Contatti_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./Contatti_files/glide.css" rel="stylesheet" type="text/css">
<link href="./Contatti_files/jquery-ui.css" rel="stylesheet">

<script async="" src="./Contatti_files/analytics.js"></script>
<script type="text/javascript" src="./Contatti_files/jquery.js"></script>
<script src="./Contatti_files/jquery-ui.js"></script>


<!--  FORM VALIDATOR -->
<script>
$(document).ready(function() {
	

/* resetto tutti gli errori quando digito in un campo */
$(':input').focus(function() {
{	$(':input').removeClass('error');
	$('#privacy').removeClass('error');}
});
	
/* dichiaro funzione se premo SUBMIT */					   
$(':submit').click(function(e) {
	/* dichiaro variabili */
	var errore=false;
	var campo01 = $('#campo01').val();
	var campo02 = $('#campo02').val();
	var campo03 = $('#campo03').val();
	var campo04 = $('#campo04').val();
	var campo05 = $('#campo05').val();
	
	
	
		
	/* controllo i campi e reastituisco errore se non compilati */
	
	if (campo01 ==0 ) {	$('#campo01').addClass('error'); errore = true; }
	if (campo02 ==0) {	$('#campo02').addClass('error'); errore = true; }
	if (campo03 ==0) {	$('#campo03').addClass('error'); errore = true; }
	if (campo04 ==0) {	$('#campo04').addClass('error'); errore = true; }
	if (campo05 ==0) {	$('#campo05').addClass('error'); errore = true; }
	
	if ($ ('#privacy').prop('checked')==false) { alert("Per poter inviare una richiesta, E'� necessario acconsentire al trattamento dei propri dati ") ;errore=true }
	
	
	
	
	
	/* se trova errore non spedisce */
	if (errore) {return false};
	/* se non trova errore spedisce */
	return true;
	
	
e.preventDefault();
});

});
</script>

<!-- BTN INVIA DEL MODULO -->
<script>
  $(function() {
	$("#Button1").hide() ; 
    $( "#slider" ).slider({
      range: "max",
      min: 0,
      max: 2,
      value: 0,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
		var valore= $( "#slider" ).slider( "value" );
		if (valore>0) {$("#Button1").show()}
      }
    });
    $( "#amount" ).val( $( "#slider" ).slider( "value" ) );
	
  });
  </script>
<title>Contatti Corona SRL</title>

<!-- GOOGLE ANALITYCS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

</head>

<body>
<div class="frame">
  <div class="bit-1">
    <div id="topbar">
      <div class="quickcontact">
        <p class="vcenter"><strong>Contatti veloci:</strong><br>
          tel. 045.7830311<br>
          info@coronaSrl.it</p>
      </div>
      <div class="lingue"><p class="vcenter"><a href=".\Home.jsp">ITA</a> | ENG</p></div>


      <div class="logo"><a href=".\Home.jsp"><img src="./Contatti_files/Logo-CoronaSrl.jpg" width="250" height="100"></a></div>
    </div>
    <!-- fine topbar --> 
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 centertxt">
    <nav class="animenu" id="nav"> 
  <button class="animenu__toggle">
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
  </button>
  <ul class="animenu__nav">
   
   
    <li>
      <a href=".\Azienda.jsp">azienda</a>
      <ul class="animenu__nav__child">
        <li><a href=".\I nostri Reparti.jsp">i nostri reparti</a></li>
        <li><a href=".\Possibilita.jsp">i nostri clienti</a></li>
       </ul>
    </li> 
    <li>
      <a href=".\Prodotti.jsp">prodotti</a>
    </li>    
    <li>
      <a href=".\ProdottiEServizi.jsp">servizi</a>
     </li>
    <li>
      <a href=".\News.jsp">news</a>
    </li>
    <li>
      <a href="Contatti.jsp">contatti</a>
    </li>   
    
                     
  </ul>
</nav>


  </div>
</div>
<div class="frame">
  <div class="bit-1">
    <div id="map-canvas">
    
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11203.421103850469!2d10.9673738!3d45.4122579!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4dc11042dcc27ba0!2sCorona%20srl!5e0!3m2!1sit!2sit!4v1600082087938!5m2!1sit!2sit" width="100%" width="800" height="485" frameborder="0" style="border:0" allowfullscreen="">
	</iframe></div>
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-3 ">
    <div class="spot"> <img src="./Contatti_files/contatti.png" class="icon">
      <p class="txt01">Contattaci</p>
      <p class="txt02bn">Aperto a scuole, enti e associazioni. Il mondo della<br>
        stampa è un patrimonio che è giusto condividere. <br>
        Richiedi un contatto</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot"> <img src="./Contatti_files/home.png" class="icon">
      <p class="txt01">Per visitare l’azienda</p>
      <p class="txt02bn">Per avere di nuovo la visione globale di Stampa Grafica</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot nobg"> <img src="./Contatti_files/mai-cosi-social.png" class="icon">
      <p class="txt01">Mai così social</p>
      <p class="txt02bn">Stampa Grafica entra nel mondo dei social media.<br>
        I nostri contenuti sono su <a href="https://www.facebook.com/CoronaSrl/" target="_blank">Facebook</a> e LinkedIn</p>
    </div>
  </div>
  <!-- fine SPOT --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1">
    <p class="txt16 centertxt"> Stampa Grafica Srl<br>
      Via Zambona, 12<br>
      37031 Illasi VR<br>
      P.I. 03053960237<br>
      <br>
      Tel. +39 045.7830311<br>
      Mail. <a href="mailto:%69nfo@s%74a%6D%70a%67r%61%66ica.%69t">info@coronaSRL.it</a> </p>
  </div>
  <!-- fine bit-1 -->
  
 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 payoffbox">
    <p class="payoff">per informazioni e preventivi<br>
      per essere contattato da stampa grafica</p>
  </div>
  <!--fine bit-1 -->
  <div class="separatore"></div>
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div id="box-modulo">
    <form method="post" action="<%=PORTIERE%>" id="ctl00">

      <div>Ragione Sociale<br>
        <input name="ragione" type="text" id="ragione" class="input">
      </div>
      <div>Nome e Cognome<br>
        <input name="nominativo" type="text" id="nominativo" class="input">
      </div>
      <br>
      <div>Email<br>
        <input name="email" type="text" id="email" class="input">
      </div>
      <div>Telefono<br>
        <input name="telefono" type="text" id="telefono" class="input">
      </div>
      <br>
      <div>Testo<br>
        <textarea name="testo" rows="2" cols="20" id="testo" class="input" style="width:100%;"></textarea>
      </div>
      <div class="privacy"> Informativa sulla Privacy<br>
        <p class="txt-privacy"><strong>Titolare e Responsabile del trattamento</strong><br>
          Titolare del trattamento dei dati forniti direttamente dagli utenti a seguito di consultazione del presente Sito è Stampa Grafica Srl - Via Zambona, 12 - 37031 Illasi VR.
          <br><strong>Trattamento dei dati</strong><br>
          I trattamenti connessi ai servizi web del Sito di Stampa Grafica Srl hanno luogo presso la predetta sede del Titolare e sono curati solo da personale tecnico incaricato del trattamento, interno alla Società titolare, oppure da eventuali incaricati di occasionali operazioni di manutenzione.
          I dati derivanti dal servizio web non saranno comunicati a terzi o diffusi, eccetto quelli forniti dagli utenti che inoltrano richieste di invio di materiale informativo e che saranno utilizzati al solo fine di eseguire il servizio o la prestazione richiesta. <br>
          <strong>Modalità del trattamento</strong><br>
          I dati personali sono trattati con strumenti automatizzati per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti.
          Specifiche misure di sicurezza sono osservate per prevenire la perdita dei dati, usi illeciti o non corretti ed accessi non autorizzati.<br>
          <strong>Diritti degli interessati</strong><br>
          I soggetti cui si riferiscono i dati personali hanno il diritto di ottenere, in qualunque momento, la conferma dell'esistenza o meno dei medesimi dati e di conoscerne il contenuto e l'origine, verificarne l'esattezza o chiederne l'aggiornamento, la rettifica o l'integrazione, così come stabilito dall'art. 7 D.Lgs 196/2003. Ai sensi del medesimo articolo, si ha il diritto di chiedere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, nonché di opporsi in ogni caso, per motivi legittimi, al loro trattamento. La presente informativa, potrà essere periodicamente aggiornata e/o modificata in base a quanto richiesto dalla normativa di settore.<br>
        </p>
        <input name="privacy" type="checkbox" id="privacy" value="">
        acconsento al trattamento dei dati personali </div>
      <!-- fine Privacy --> 
      
      <!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
      <div style="clear:both; height:50px; overflow:hidden"></div>
      <!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
      
      <div class="frame">
        <div id="slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><p>trascina il cursore per inviare</p><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-max" style="width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span></div>
        <input type="submit" name="Button1" value="Invia" id="Button1" class="btn-invia" style="display: none;">
        
      </div>
      <input type="hidden" name="codoper" value="postContatto">
	  <input type="hidden" name="targetpage" value="contatto/success.jsp">
    </form>
  </div>
  <!-- fine Box-modulo --> 
</div>
<!-- fine FRAME --> 

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
<div style="clear:both; height:50px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE --> 

<!----------------------------------- FOOTER ------------------------------------------>
<div class="frame">
  <!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


<div id="footer">

            <div class="bit-4">
                <div class="spot7">
                <p class="txt09-logo">s|g<br>stampa grafica</p>
                
                <p class="txt10">Stampa Grafica vive di sfide.
Nasce nel 1988 proprio da una sfida di Guido
Santi, cresciuto tra macchine tipografiche e
sistemi di stampa. Oggi, i progetti più complessi e
le sfide alla creatività trovano la soluzione più
consona, nei tempi stabiliti.</p>
              </div><!-- fine SPOT -->
             </div>
             
             
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">I FOCUS PRINCIPALI</p>
                <hr class="linea">
                <p class="txt10">Prestampa<br>
Stampa Offset<br>
Stampa Digitale<br>
Unico - stampa di prototipi, mock up e pezzi unici<br>
Collaborazione con agenzie creative<br>
Consulenza di stampa</p>
              </div><!-- fine SPOT -->
            </div>
            
            
              
            
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">CONTATTI</p>
                <hr class="linea">
                <p class="txt10">Stampa Grafica Srl<br>
Via Zambona, 12<br>
37031 Illasi VR<br>
P.I. 03053960237<br>
tel. +39 045.7830311<br>
mail. info@coronasrl.it</p>
              </div><!-- fine SPOT -->
            </div>
            
             <div class="bit-4">
              <div class="spot7">
                <p class="txt09">CREDITS</p>
                <hr class="linea">
                <p class="txt10">Sito realizzato da <a href="http://www.linkedin.it/" target="_blank">Andrea Giraldi</a></p>
                <p class="txt10"><a href="InformativaCookies.html" target="_blank">Cookie policy</a></p>
              </div><!-- fine SPOT -->
            </div>
            
            
</div>

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:60px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame -------------------------------> 
<!-------------------------------- fine FOOTER ---------------------------->


<script src="./Contatti_files/animenu.js.download"></script>

</body></html>