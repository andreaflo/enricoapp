<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0045)https://www.stampagrafica.it/ita/azienda.aspx -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link href="./Azienda_files/css" rel="stylesheet" type="text/css">
<link href="./Azienda_files/stampagrafica.css" rel="stylesheet" type="text/css">
<link href="./Azienda_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./Azienda_files/glide.css" rel="stylesheet" type="text/css">
<script async="" src="./Azienda_files/analytics.js.download"></script><script type="text/javascript" src="./Azienda_files/jquery.min.js.download"></script>
<title>Azienda</title>

<!-- GOOGLE ANALITYCS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

</head>

<body>
<div class="frame">
  <div class="bit-1">
    <div id="topbar">
      <div class="quickcontact">
        <p class="vcenter"><strong>Contatti veloci:</strong><br>
          tel. 045.7830311<br>
          info@coronaSrl.it</p>
      </div>
      ﻿ <div class="lingue"><p class="vcenter"><a href="./Azienda.jsp">ITA</a> | ENG</p></div>


      <div class="logo"><a href="./Home.jsp"><img src="./Azienda_files/Logo-StampaGrafica.jpg" width="250" height="100"></a></div>
    </div>
    <!-- fine topbar --> 
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 centertxt">
    ﻿<nav class="animenu" id="nav"> 
  <button class="animenu__toggle">
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
  </button>
  <ul class="animenu__nav">
   
   
    <li>
      <a href=".\Azienda.jsp">azienda</a>
      <ul class="animenu__nav__child">
        <li><a href=".\I nostri Reparti.jsp">i nostri reparti</a></li>
        <li><a href=".\Azienda.jsp">i nostri clienti</a></li>
       </ul>
    </li> 
    <li>
      <a href=".\Prodotti.jsp">prodotti</a>
    </li>    
    <li>
      <a href=".\ProdottiEServizi.jsp">servizi</a>
     </li>
    <li>
      <a href=".\News.jsp">news</a>
    </li>
    <li>
      <a href=".\Contatti.jsp">contatti</a>
    </li>   
    
                     
  </ul>
</nav>


  </div>
</div>
<div class="frame">
  <div class="bit-1"> 
    
    <!-- Slideshow 4 -->
    
    <div class="slider">
      <ul class="slider__wrapper">
        <li class="slider__item"><img src="./Azienda_files/slide01.jpg"></li>
      </ul>
    </div>
    
    <!-- fine Slideshow 4 --> 
    
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-3 ">
    <div class="spot"> <img src="./Azienda_files/la-sede.png" class="icon">
      <p class="txt01">La sede</p>
      <p class="txt02bn">Ampia, candida, luminosa, tecnologica:<br>
        la forma dell'orgoglio.</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot"> <img src="./Azienda_files/patrimonio-culturale.png" class="icon">
      <p class="txt01">Patrimonio culturale</p>
      <p class="txt02bn">Coltivare la cultura di settore<br>
        a sostegno della passione</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot nobg"> <img src="./Azienda_files/mai-per-caso.png" class="icon">
      <p class="txt01">Mai per caso</p>
      <p class="txt02bn">La qualità, un processo<br>
        complesso e misurabile</p>
    </div>
  </div>
  <!-- fine SPOT --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 payoffbox">
    <p class="payoff">La tecnologia è poca cosa senza la dedizione</p>
      
    <p class="txt11">Stampa Grafica vive di sfide. Nasce nel 1988 proprio da una sfida di Guido Santi, cresciuto tra macchine tipografiche e sistemi di stampa. Una piccola tipografia artigianale non gli basta, vuole crescere e diventare un punto di riferimento nel mondo della stampa per quelle aziende che sanno riconoscere l’importanza di questo settore per il proprio business. E ci riesce.<br><br>

Perché parlare di qualità di stampa e innovazione tecnologica sarebbe riduttivo. Stampa Grafica emerge per la passione che infonde in ogni progetto, per la cultura che ha maturato in trent’anni di esperienza, per le scelte che hanno portato il gruppo a imporsi nel mercato.
I progetti più complicati, le sfide alla creatività e all’innovazione: di questo vive l’azienda.<br><br>

Ma per fare questo, la cura del cliente dev’essere sempre in primo piano. Chiedere ai vari Wella, BMW e Mini per credere. Realizzando prodotti per imprese di questo calibro, Stampa Grafica può oggi vantare una predisposizione alla consulenza difficile da trovare altrove. </p>
  </div>
  <!--fine bit-1 -->

</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 payoffbox">
    <p class="payoff">I plus di stampa grafica</p>
  </div>
<!--fine bit-1 --></div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-25" style="text-align:center">
    <div class="spot9"><img src="./Azienda_files/ico_qualita.jpg"> </div>
  </div>
  <!-- fine bit25 -->
  <div class="bit-75 txt12">La nostra sensibilità, unita all'esperienza maturata servendo grandi brand dell'industria e operando con partner d'eccellenza, ci ha educato alla perfezione del risultato. Pur avendo sempre operato nell'ottica della qualità totale, nel 2008 abbiamo deciso di intraprendere il percorso della certificazione e nel febbraio 2009 abbiamo ottenuto la certificazione del sistema di Qualità secondo la norma UNI EN ISO 9001:2008.</div>
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-75 txt12">L’accelerazione impressa dalla tecnologia, ci obbliga ad un costante monitoraggio del settore per testare sempre nuove macchine, processi, prodotti e materie prime. Strumenti che consentono di limitare l’impatto ambientale, tempi di produzione e di creare soluzioni innovative. Al contempo richiedono una sempre maggiore professionalità, cura e attenzione. Gli anni di esperienza nel settore ci hanno fatto incontrare le richieste più improbabili e fuori dal comune, che hanno sempre rappresentato un importante momento di crescita. Per questo i reparti di Stampa Grafica sono attrezzati per stampare e lavorare ogni tipo di materiale: dal legno ai polimeri, da materiali plastici a cartone. Nessun lavoro è proibitivo.</div>
  <div class="bit-25" style="text-align:center">
    <div class="spot9"><img src="./Azienda_files/ico_ricerca.jpg"> </div>
  </div>
  <!-- fine bit25 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-25" style="text-align:center">
    <div class="spot9"><img src="./Azienda_files/ico_progettazione.jpg"> </div>
  </div>
  <!-- fine bit25 -->
  <div class="bit-75 txt12">Dall’ascolto attento delle esigenze dei nostri clienti prende avvio la progettazione degli strumenti, siano essi prodotti editoriali di grande tiratura o creazioni tridimensionali in numeri unici. Affiancare aziende o agenzie creative ci dà modo di esprimere tutto il nostro potenziale in termini di problem solving e consulenza tecnica.</div>
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-75 txt12">Amiamo appagare il gusto estetico, affascinare con la fedeltà dei colori e la riproduzione dei dettagli, sollecitare il tatto con carte e materiali innovativi. Abbiamo fatto della flessibilità un asset fondamentale, disponendo di reparti di stampa offset e digitale, per rispondere alle diverse necessità di ciascuno dei nostri clienti.</div>
  <div class="bit-25" style="text-align:center">
    <div class="spot9"><img src="./Azienda_files/ico_stampa.jpg"> </div>
  </div>
  <!-- fine bit25 --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-25" style="text-align:center">
    <div class="spot9"><img src="./Azienda_files/ico_logistica.jpg"> </div>
  </div>
  <!-- fine bit25 -->
  <div class="bit-75 txt12">Trasporto, consegna, stoccaggio presso il cliente: il servizio post stampa è un momento fondamentale che richiede puntualità, verifica del prodotto, identificazione facile e immediata dei diversi colli, per assicurare al cliente la migliore disponibilità e accessibilità a tutti i suoi prodotti di stampa.</div>
</div>
<!-------------------------fine frame ------------------------------->



 

<!----------------------------------- FOOTER ------------------------------------------>
<div class="frame">
  ﻿<!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


<div id="footer">

            <div class="bit-4">
                <div class="spot7">
                <p class="txt09-logo">s|g<br>stampa grafica</p>
                
                <p class="txt10">Stampa Grafica vive di sfide.
Nasce nel 1988 proprio da una sfida di Guido
Santi, cresciuto tra macchine tipografiche e
sistemi di stampa. Oggi, i progetti più complessi e
le sfide alla creatività trovano la soluzione più
consona, nei tempi stabiliti.</p>
              </div><!-- fine SPOT -->
             </div>
             
             
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">I FOCUS PRINCIPALI</p>
                <hr class="linea">
                <p class="txt10">Prestampa<br>
Stampa Offset<br>
Stampa Digitale<br>
Unico - stampa di prototipi, mock up e pezzi unici<br>
Collaborazione con agenzie creative<br>
Consulenza di stampa</p>
              </div><!-- fine SPOT -->
            </div>
            
            
              
            
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">CONTATTI</p>
                <hr class="linea">
                <p class="txt10">Stampa Grafica Srl<br>
Via Zambona, 12<br>
37031 Illasi VR<br>
P.I. 03053960237<br>
tel. +39 045.7830311<br>
mail. info@stampagrafica.it</p>
              </div><!-- fine SPOT -->
            </div>
            
             <div class="bit-4">
              <div class="spot7">
              
                <p class="txt09">CREDITS</p>
                <hr class="linea">
                <p class="txt10">Sito realizzato da <a href="http://www.linkedin.it/" target="_blank">Andrea Giraldi</a></p>
                <p class="txt10"><a href="InformativaCookies.html" target="_blank">Cookie policy</a></p>
              </div><!-- fine SPOT -->
            </div>
            
            
</div>

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:60px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame -------------------------------> 
<!-------------------------------- fine FOOTER ---------------------------->


<script src="./Azienda_files/animenu.js.download"></script>

</body></html>