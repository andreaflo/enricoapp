<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0054)https://www.stampagrafica.it/ita/i-nostri-reparti.aspx -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link href="./I nostri Reparti_files/css" rel="stylesheet" type="text/css">
<link href="./I nostri Reparti_files/stampagrafica.css" rel="stylesheet" type="text/css">
<link href="./I nostri Reparti_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./I nostri Reparti_files/glide.css" rel="stylesheet" type="text/css">
<link href="./I nostri Reparti_files/carousel.css" rel="stylesheet" type="text/css">
<link href="./I nostri Reparti_files/connected-carousels.css" rel="stylesheet"
	type="text/css">

<script type="text/javascript" src="./I nostri Reparti_files/jquery.js"></script>
<script type="text/javascript" src="./I nostri Reparti_files/jquery-ui.js"></script>
<script type="text/javascript" src="./I nostri Reparti_files/jquery.jcarousel.js"></script>
<script type="text/javascript" src="./I nostri Reparti_files/glide.js"></script>
<script async="" src="./I nostri Reparti_files/analytics.js"></script>

<!-- SLIDER -->
<script type="text/javascript">
	(function($) {
		// This is the connector function.
		// It connects one item from the navigation carousel to one item from the
		// stage carousel.
		// The default behaviour is, to connect items with the same index from both
		// carousels. This might _not_ work with circular carousels!
		var connector = function(itemNavigation, carouselStage) {
			return carouselStage.jcarousel('items').eq(itemNavigation.index());
		};

		$(function() {
			// Setup the carousels. Adjust the options for both carousels here.
			var carouselStage = $('.carousel-stage').jcarousel();
			var carouselNavigation = $('.carousel-navigation').jcarousel();

			// We loop through the items of the navigation carousel and set it up
			// as a control for an item from the stage carousel.
			carouselNavigation.jcarousel('items').each(function() {
				var item = $(this);

				// This is where we actually connect to items.
				var target = connector(item, carouselStage);

				item.on('jcarouselcontrol:active', function() {
					carouselNavigation.jcarousel('scrollIntoView', this);
					item.addClass('active');
				}).on('jcarouselcontrol:inactive', function() {
					item.removeClass('active');
				}).jcarouselControl({
					target : target,
					carousel : carouselStage
				});
			});

			// Setup controls for the stage carousel
			$('.prev-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			// Setup controls for the navigation carousel
			$('.prev-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			$('.carousel').jcarousel({
				 wrap: 'circular',
				 scroll : 1
			}).jcarouselAutoscroll({
				interval : 5000,
				target : '+=1',
				autostart : true
			}).hover(function() {
			    $(this).jcarouselAutoscroll('stop');
			}, function() {
			    $(this).jcarouselAutoscroll('start');

			});
		});
	})(jQuery);

	$('.jcarousel').jcarousel({
		wrap : 'circular'
	}).jcarouselAutoscroll({
		interval : 3000,
		 target: '+=1'
	});
</script>

<title>Possibilità di stampa</title>

<!-- GOOGLE ANALITYCS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

</head>

<body>
<div class="frame">
  <div class="bit-1">
    <div id="topbar">
      <div class="quickcontact">
        <p class="vcenter"><strong>Contatti veloci:</strong><br>
          tel. 045.7830311<br>
          info@coronasrl.it</p>
      </div>
      ﻿ <div class="lingue"><p class="vcenter"><a href="./Home_files/ITA/index.html">ITA</a> | ENG</p></div>


      <div class="logo"><a href="Home.jsp"><img src="./I nostri Reparti_files/Logo-StampaGrafica.jpg" width="250" height="100"></a></div>
    </div>
    <!-- fine topbar --> 
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->
<div class="frame">
  <div class="bit-1 centertxt">
    ﻿<nav class="animenu" id="nav"> 
  <button class="animenu__toggle">
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
  </button>
  <ul class="animenu__nav">
   
   
    <li>
      <a href="Azienda.jsp">azienda</a>
      <ul class="animenu__nav__child">
        <li><a href="I nostri Reparti.jsp">i nostri reparti</a></li>
        <li><a href="Possibilita.jsp">i nostri clienti</a></li>
       </ul>
    </li> 
    <li>
      <a href="Prodotti.jsp">prodotti</a>
    </li>    
    <li>
      <a href="ProdottiEServizi.jsp">servizi</a>
     </li>
    <li>
      <a href="News.jsp">news</a>
    </li>
    <li>
      <a href="Contatti.jsp">contatti</a>
    </li>   
    
                     
  </ul>
</nav>
</div>

<div align="left" id="centratore">
		<div class="frame">
			<div class="wrapper">
				<div class="connected-carousels">
					<div class="stage">
						<div class="carousel carousel-stage">
							<ul>
								<li><img src="./Home_files/slide01.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide02.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide03.jpg" width="1200"
									height="400" alt=""></li>
								<li><img src="./Home_files/slide04.jpg" width="1200"
									height="400" alt=""></li>
							</ul>
						</div>
						</div>
						</div>
						</div>
						</div>
						</div>
				<!-- fine Slideshow 4 -->



	<hr>
			
<!--fine bit-1 -->

<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-3 ">
    <div class="spot"> <img src="./I nostri Reparti_files/prestampa.png" class="icon">
      <p class="txt01">Prestampa</p>
      <p class="txt02bn">Gestione colore, processo unificato e operatori qualificati.<br>
        Le caratteristiche principali del reparto.</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot"> <img src="./I nostri Reparti_files/stampa-offset.png" class="icon">
      <p class="txt01">Stampa Offset</p>
      <p class="txt02bn">Fedeltà dei colori, definizione dei dettagli.<br>
        Fino a 120 linee di retino.</p>
    </div>
  </div>
  <!-- fine SPOT -->
  
  <div class="bit-3 ">
    <div class="spot nobg"> <img src="./I nostri Reparti_files/stampa-digitale.png" class="icon">
      <p class="txt01">Stampa Digitale</p>
      <p class="txt02bn">Alta qualità di stampa in tempi ridottissimi.<br>
        Ideale per un basso numero di copie</p>
    </div>
  </div>
  <!-- fine SPOT --> 
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-1 payoffbox">
    <p class="payoff noborder">Sono le macchine o le persone a fare la differenza?</p>
    <br>
    <p class="payoff-sub">In un'azienda come la nostra probabilmente è l'alchimia tra le due componenti<br>
      a rendere Stampa Grafica un'eccellenza nel suo settore.</p>
  </div>
<!--fine bit-1 --></div>
<!-------------------------fine frame -------------------------------> 

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
<div style="clear:both; height:50px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

<div class="frame">
  <div class="bit-2"><img src="./I nostri Reparti_files/stampa2.jpg" class="full-width"></div>
  <div class="bit-2">
    <p class="txt13-titolo-sx">reparto prestampa</p>
    <p class="txt13">Un reparto prestampa all’interno dell’azienda è alla base dell’intero processo di stampa, se garantisce un supporto esperto e specializzato nella gestione del file.
      <br>
    Contatto continuo con il cliente, performance di tutti i macchinari da prestampa e operatori qualificati car atterizzano il reparto di prestampa. Qui si trovano sistemi di gestione colore all’avanguardia e il processo unificato nella preparazione di lastre e forme per un risultato unico. </p>
  </div>

  <!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:80px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-2">
    <p class="txt13-titolo-dx">reparto stampa offset </p>
    <p class="txt13">Estrema definizione e alta risoluzione: il processo si potrebbe riassumere così.
      <br>
    Il reparto ha una flessibilità gestionale inimitabile e una qualità di stampa elevatissima per le alte tirature. La definizione di stampa è strettamente collegata alla misura del retino adottato: più il retino è fitto e maggiore sarà la definizione. Dati alla mano, rispetto ad un retino standard 70, Stampa Grafica adotta retini da 80 fino a 120 come quelli che ha utilizzato per la stampa dei cataloghi d’arte del MOMA di New York, dove la stampa stessa diventa opera d’arte.</p>
  </div>
  <div class="bit-2"><img src="./I nostri Reparti_files/stampa3.jpg" class="full-width"></div>
  
  <!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:80px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame ------------------------------->

<div class="frame">
  <div class="bit-2"><img src="./I nostri Reparti_files/stampa4.jpg" class="full-width"></div>
  <div class="bit-2">
    <p class="txt13-titolo-sx"> stampa digitale</p>
    <p class="txt13">Adatto a basse tirature, garantisce un sistema di definizione e fedeltà dei colori invidiabile, oltre ad un alto grado di flessibilità delle lavorazioni. 
I tempi sono ridotti, grazie ad un processo creato ad hoc e a macchine da stampa all’avanguardia. 
      <br>
      La concentrazione dell’intera filiera di stampa all’interno dell’azienda (dallo studio, alla prototipazione, alla produzione) e le innovazioni tecniche hanno tolto qualsiasi limite ai supporti - sia rigidi sia flessibili in bobina -, formati - fino a 320cm - e lavorazioni.<br>
320cm x 320cm. La misura della macchina da taglio che proponiamo basta per descrivere quest’area. Il plotter da taglio multiutensile ha le dimensioni più importanti del mercato, garantendo un servizio di taglio sui materiali più diversi.
</p>
</div>

  <!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:80px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

</div>
<!-------------------------fine frame ------------------------------->

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
<div style="clear:both; height:50px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE --> 

<!----------------------------------- FOOTER ------------------------------------------>
<div class="frame">
  ﻿<!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


<div id="footer">

            <div class="bit-4">
                <div class="spot7">
                <p class="txt09-logo">s|g<br>stampa grafica</p>
                
                <p class="txt10">Stampa Grafica vive di sfide.
Nasce nel 1988 proprio da una sfida di Guido
Santi, cresciuto tra macchine tipografiche e
sistemi di stampa. Oggi, i progetti più complessi e
le sfide alla creatività trovano la soluzione più
consona, nei tempi stabiliti.</p>
              </div><!-- fine SPOT -->
             </div>
             
             
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">I FOCUS PRINCIPALI</p>
                <hr class="linea">
                <p class="txt10">Prestampa<br>
Stampa Offset<br>
Stampa Digitale<br>
Unico - stampa di prototipi, mock up e pezzi unici<br>
Collaborazione con agenzie creative<br>
Consulenza di stampa</p>
              </div><!-- fine SPOT -->
            </div>
            
            
              
            
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">CONTATTI</p>
                <hr class="linea">
                <p class="txt10">Stampa Grafica Srl<br>
Via Zambona, 12<br>
37031 Illasi VR<br>
P.I. 03053960237<br>
tel. +39 045.7830311<br>
mail. info@stampagrafica.it</p>
              </div><!-- fine SPOT -->
            </div>
            
             <div class="bit-4">
              <div class="spot7">
                <p class="txt09">CREDITS</p>
                <hr class="linea">
                <p class="txt10">Sito realizzato da <a href="http://www.linkedin.it/" target="_blank">Andrea Giraldi</a></p>
                <p class="txt10"><<a href="InformativaCookies.html" target="_blank">Cookie policy</a></p>
              </div><!-- fine SPOT -->
            </div>
            
            
</div>

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:60px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame -------------------------------> 
<!-------------------------------- fine FOOTER ---------------------------->


<script src="./I nostri Reparti_files/animenu.js"></script>

</body></html>&nbsp