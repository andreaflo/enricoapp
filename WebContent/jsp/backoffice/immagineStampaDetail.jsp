
<%@ page import="java.util.*"%>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*,it.realt.webapps.utente.dbbeans.*,it.realt.webapps.utente.mvc.control.*,java.util.*,it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*,it.realt.webapps.beans.*,java.text.*"%>
<%@ page import="dbobjects.*"%>
<%@ page import="it.realt.webapps.anagrafica.internet.ops.OpAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.beans.FiltroAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.ModuloAnagrafica.Tipo"%>
<%@ page import="it.realt.webapps.anagrafica.dbbeans.DbAnagrafica"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ include file="/jsp/commons/tipoAnagrafica.jspf"%>
<tags:sicurezza idFunzione="listaAnagrafiche" />
<jsp:useBean id="listaHtml" class="it.realt.webapps.html.ListaHtml"	scope="page" />
<jsp:useBean id="pag" class="it.realt.webapps.html.Paginazione"	scope="page" />

<%
OpAnagrafica opAnagrafica = new OpAnagrafica(user);


String idAnagragica = request.getParameter("idImmagine");
Immagine item = null;


if (idAnagragica == null) {
	item = (Immagine) session.getValue("dettaglioImmagine");
	
} 

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script language="javascript">
	function modifica() {
		document.forms[0].submit();
	}

	function cancella(idAnag) {
		document.cancellazione.idAnagrafica.value = idAnag;
		document.cancellazione.submit();
	}

	function inserisci() {
	}
</script>
<script language="JavaScript" type="text/javascript">

function apri(a){
	window.open(a, "", "width=560, height=377, status=1, resizable=1");
}

</script>
<head>
<title>Dettaglio Anagrafica</title>
</head>
<link rel="stylesheet" href="<%=context%>/css/style.css" type="text/css"></link>
<link rel="stylesheet" href="<%=context%>/css/stylepartedinamica.css" type="text/css"></link>
<body>

<br><div align="center" class="titolo">Dettaglio Anagrafica</div><br>

<div align="center">
<form action="<%=PORTIERE%>" method="POST">

<table class="lista">
	<TR>
		<TD>
		<table>
			<tr ALIGN="LEFT">
				<th class="primo1">Id Immagine</th>
				<td align="left"><span><%=item.getIdimmagine()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Riferimento</th>
				<td align="left"><span><%=item.getDescrizione()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Nota</th>
				<td align="left"><span><%=item.getNota()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Path</th>
				<td align="left"><span><%=item.getPath()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Tipo</th>
				<td align="left"><span><%=item.getTipo()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Url</th>
				<td align="left"><span><%=item.getUrl()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
			<tr ALIGN="LEFT">
				<th class="primo1">Altri dati</th>
				<td align="left"><span><%=item.getAltro()%></span></td>
				<TD WIDTH=15>&nbsp;</TD>
			</TR>
		</table>
		</td>
	</tr>	
		</table>

<br><br>

<div align="center" class="titolo">
<input type="button" value="Stampa" onClick="window.print()" />
<input type="button" value="Indietro" onclick="location.href ='immagineList.jsp?tipo=<%=tipo.getCodice()%>';">
</div>

<input type="hidden" name="idImmagine" value="<%=item.getIdimmagine()%>">
<input type="hidden" name="targetpage" value="backoffice/immagineForm.jsp?tipo=<%=tipo.getCodice()%>">
<input type="hidden" name="callpage" value="errore.jsp">
<input type="hidden" name="codoper" value="dettaglioImmagine">
</form>
</div>
</body>
</html>