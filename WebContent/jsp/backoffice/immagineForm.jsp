<%@page import="it.realt.webapps.anagrafica.dbbeans.DbAnagrafica"%>
<%@page import="it.realt.util.GeneralComparator"%>
<%@page import="it.realt.webapps.utils.dbbeans.DbLista"%>
<%@page import="it.realt.webapps.utils.dbbeans.dblista.FiltroDbLista"%>
<%@page import="it.realt.webapps.utils.internet.ops.OpLista"%>
<%@page import="it.realt.webapps.utente.internet.ops.OpUtente"%>
<%@page import="it.realt.webapps.utente.dbbeans.utente.FiltroUtente"%>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*, it.realt.webapps.utente.dbbeans.*, it.realt.webapps.utente.mvc.control.*, java.util.*, it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*, it.realt.webapps.beans.*, java.text.*"%>
<%@ page import="dbobjects.Immagine"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>

<%
boolean nuova = "true".equals(request.getParameter("nuovo"));
String idFunzione="modificaAnagrafica";
%>
<tags:sicurezza idFunzione="<%=idFunzione%>"/>


<%pageContext.setAttribute("languages", Languages.getInstance(), pageContext.PAGE_SCOPE);%>
<jsp:useBean id="formValidator" class="it.realt.webapps.html.FormValidator" scope="page"/>
<jsp:useBean id="dettaglioImmagine" class="dbobjects.Immagine" scope="session"/>

<%@ include file="/jsp/commons/tipoAnagrafica.jspf"%>

<%
String codoper = nuova?"insertImmagine":"modificaImmagine";
if (nuova) {
	dettaglioImmagine = new Immagine();
	pageContext.setAttribute("dettaglioImmagine", dettaglioImmagine ,pageContext.PAGE_SCOPE);
	 String idAnagrafica = request.getParameter("idimmagine");
	
}

formValidator.setFormName("dati");

//formValidator.addValidationRule(new SimpleValidationRule("descrizione","Descrizione necessaria!","notEmpty"));	
//formValidator.addValidationRule(new SimpleValidationRule("idAnagrafica","idAnagrafica necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("tipoAnagrafica","tipoAnagrafica necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("nome","nome necessario!","notEmpty"));

//formValidator.addValidationRule(new SimpleValidationRule("idimmagine","Nome necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("nome","Nome necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("path","path necessario!","notEmpty"));

//formValidator.addValidationRule(new SimpleValidationRule("citta","citta necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("provincia","provincia necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("partitaIva","partitaIva necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("codiceFiscale","codiceFiscale necessario!","notEmpty"));
//formValidator.addValidationRule(new SimpleValidationRule("altro","Altro necessario!","notEmpty"));

%>
<%out.clear();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=nuova?"Inserimento":"Modifica"%> Immagini</title>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<%=context%>/css/ui-lightness/jquery-ui-1.8.12.custom.css" type="text/css" media="all" />
<script type="text/javascript" src="<%=context%>/js/validator.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="<%=context%>/js/jquery.ui.datepicker-it.js"></script>
<script type="text/javascript" src="<%=context%>/js/application.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<tags:menuFunzioni root="root" />
<br><div align="center" class="titolo"><%=nuova?"Inserimento":"Modifica"%> Immagine</div><br>

<div align="center" <% if (!nuova) { %>style="float:left; margin-left: 50px;"<% } %> >
<form action="<%=PORTIERE%>" method="post" name="dati" id="dati" enctype="multipart/form-data">
<input type="hidden" name="codoper" value="<%=codoper%>" />
<input type="hidden" name="targetpage" value="backoffice/immagineList.jsp?tipo=<%= tipo.getCodice() %>" />
<input type="hidden" name="callpage" value="<%=request.getRequestURL()%>" />
<input type="hidden" name="tipoAnagrafica" value="<%= tipo.getCodice() %>" />
<input type="hidden" name="nazione" value="IT">
<input type="hidden" name="lingua" value="ITA">
<input type="hidden" name="stato_record" value="A">
<table class="lista">

<%if (!nuova) {
			if (session.getAttribute("dettaglioImmagine")!= null) {
			 String idAnagrafica = request.getParameter("idimmagine");
			// session.getAttribute("riferimenti_x_anagrafica");
			//pageContext.setAttribute("riferimenti_x_anagrafica", riferimenti_x_anagrafica);
			}
     %>
	<input type="hidden" name="idimmagine" value="<%=dettaglioImmagine.getIdimmagine()%>" />
	<tr align="left">
        <td class="primo1"><div align="left">identificativo su Database</div></td>
        <td><div align="left" readonly=true ><tags:input bean="${dettaglioImmagine}"  property="idimmagine" style="width :350px;"/></div></td>
      </tr>     
      <tr align="left">
        <td class="primo1"><div align="left">Riferimento</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="nome" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Descrizione</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="descrizione" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Altro</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="altro"/></div></td>
      </tr>
      <tr align="left" >
        <td class="primo1"><div align="left">Path</div></td>
		<td align="left">
		<input name="file" type="file"  style="width:400px" /></td>
	  </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Nota</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="nota"/></div></td>
      </tr>
      
<%}else{%>

      <tr align="left">
        <td class="primo1"><div align="left">identificativo su Database</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="idimmagine" style="width :350px;"/></div></td>
      </tr>     
      <tr align="left">
        <td class="primo1"><div align="left">Riferimento</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="nome" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Descrizione</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="descrizione" style="width :350px;"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Altro</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="altro"/></div></td>
      </tr>
      <tr>
        <td class="primo1"><div align="left">Path</div></td>
		<td align="left"><input id="file" name="file" type="file" style="width:300px" editable="true"/></td>
	  </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Nota</div></td>
        <td><div align="left"><tags:input bean="${dettaglioImmagine}"  property="nota"/></div></td>
      </tr>
      <tr align="left">
        <td class="primo1"><div align="left">Data</div></td>
        <td><div align="left"><input type="text" name="dataInstallazione" class='date' /></div></td>
      </tr>
      
      
<%}%>	  
</table>
<br>
<table align="center"> 
  <tr align="left">
    <td colspan="2" align="center">
      <a href="javascript:go()"><img src="<%=context%>/img/pulsanti/conferma.png" value="<%=nuova?"Inserisci":"Modifica"%>" border="0" title="<%=nuova?"Inserisci":"Modifica"%>" alt="<%=nuova?"Inserisci":"Modifica"%>" /></a>
      <a href="immagineList.jsp?tipo=<%= tipo.getCodice() %>"> <img border="0" src="<%=context%>/img/pulsanti/indietro.png" alt="Indietro" title="Indietro" /> </a> 
     </td>
  </tr>
</table>
</form>
<tags:formValidation validator="${formValidator}" function="go"/>
</div>

<!--   MODIFICA -->

<script type="text/javascript" src="<%=context%>/js/validator.js">
</script>

</body>
</html>
