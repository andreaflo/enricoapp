<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<% out.clear();
   //stato del login : OK oppure KO
   String stato=(String)session.getAttribute("answer_status");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=webApp.getApplication()%></title>
<script language="javascript">
var trovatoerrore = false;

function errore(elemento,testo){
    if (trovatoerrore) return;
    window.alert(testo);
    elemento.select();
    elemento.focus();
    trovatoerrore = true; }

function verifica_form () {
      trovatoerrore = false;
      if (document.dati.login.value == "")
              errore(document.dati.login,"immettere la login...");
      if (document.dati.passwd.value == "")
              errore(document.dati.passwd,"immettere la password...");
      return !trovatoerrore;
}

function control_and_submit(){
      if(verifica_form()){
      document.dati.submit();}
}

function submitenter(myfield, e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	if (keycode == 13) {
	   myfield.form.submit();
	   return false;
	}
	else
	   return true;
}
</script>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/menu.css" type="text/css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<table class="top" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="80" class="sfondoindex">&nbsp;</td>
    <td height="80" class="sfondoindex"><center><a href="<%=context%>"><img src="<%=context%>/img/menu/banner.jpg" width="200" height="80" border="0" align="top"></a></center></td>
    <td height="80" class="sfondoindex">&nbsp;</td>
  </tr>
</table>
<div class="spaziatore">&nbsp;</div><br>
<div align="center" class="titolo">Login</div><br>
<form name="dati" action="<%=request.getContextPath()%><%=costanti.portiere%>" method="post">
<table border="0" align="center">
	<tr>
		<td class="primo1">Login</td>
		<td><input type="text" name="login" class="stilelogin" /></td>
	</tr>
	<tr>
		<td class="primo1">Password</td>
		<td><input type="password" name="passwd" class="stilelogin" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br><div align="center"><input type="button" onclick="control_and_submit();" value="Entra" /></div>

		</td>
	</tr>
	<tr>
		<td colspan="2" align="center" height="30">
		<%
			//sessione valida                            
			if (stato != null) {
				//login non corretto
				if (stato.equals("KO")) {
		%> <span class=testo> <font color="red"> <%=(String) session.getAttribute("answer_message")%>
		</font> </span> 
<%
}
	//invalido la sessione
	session.invalidate();
}
%>
		</td>
	</tr>
</table>
<input type="hidden" name="codoper" value="login" /></form>
</body>
</html>