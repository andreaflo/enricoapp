
<%@page import="dao.ImmagineDAOImpl"%>
<%@page import="util.ImageURLFormat"%>
<%@page import="it.realt.util.GeneralComparator"%>
<%@ page
	import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*, it.realt.webapps.utente.dbbeans.*, it.realt.webapps.utente.mvc.control.*, java.util.*, it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*, it.realt.webapps.beans.*, java.text.*"%>
<%@ page import="it.realt.webapps.anagrafica.beans.Anagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.beans.FiltroAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.internet.ops.OpAnagrafica"%>
<%@ page import="it.realt.webapps.anagrafica.ModuloAnagrafica.Tipo"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ include file="/jsp/commons/navigazione.jspf"%>

<tags:sicurezza idFunzione="modificaAnagrafica" />

<%pageContext.setAttribute("languages", Languages.getInstance(), PageContext.PAGE_SCOPE);%>
<jsp:useBean id="listaHtml" class="it.realt.webapps.html.ListaHtml"
	scope="page" />
<jsp:useBean id="pag" class="it.realt.webapps.html.Paginazione"
	scope="page" />
<jsp:useBean id="formValidator"
	class="it.realt.webapps.html.FormValidator" scope="page" />

<%
String idFunzione = "modificaAnagrafica";
boolean filtroAvanzato = request.getParameter("filtroAvanzato") != null;

String tempDir = "/home/andrea/WorkspaceTest/EnricoApp/jsp/files/upload/";

out.clearBuffer(); %>


<%@ include file="/jsp/commons/tipoAnagrafica.jspf"%>
<%
// Recupero lista dalla sessione, se non c'� cerco sul DB
ImmagineDAOImpl dao = new ImmagineDAOImpl();

List l = (List) session.getValue("listaImmagine");

Utente utente = (Utente) session.getAttribute("loginUtente");
String id_anagrafica = utente.getIdAnagrafica();
//System.out.println("id_anagrafica associata all'utente : "+id_anagrafica);

FiltroAnagrafica filtro=(FiltroAnagrafica)session.getValue("filtroImmagine");
if(filtro==null)
{
	filtro=new FiltroAnagrafica();
}

if (tipo.equals(Tipo.CLIENTE_FORNITORE)) {
	filtro.setTipoAnagraficaValues(new String[] {"C", "F"});
} else {
	filtro.setTipoAnagrafica(tipo.getCodice());
}


l = dao.getImmagini(idFunzione);
//ordino la lista
GeneralComparator gc=new GeneralComparator("nome");
Collections.sort(l,gc);

// Paginazione della lista visualizzata
pag.setRequest(request);
pag.setDelta(webApp.getDeltaPagine());
l = pag.paginaOrdina(l,null,webApp.getRecordPagina());
listaHtml.setBeans(l);

listaHtml.addProperty("idimmagine","Identificativo Immagine");
listaHtml.addProperty("nome","Riferimento");
listaHtml.addProperty("descrizione","Descrizione Immagine");
listaHtml.addProperty("path","Indirizzo");
listaHtml.addProperty("altro","Altri dati");
listaHtml.addProperty("path","Path");
listaHtml.addProperty("nota","Nota");
listaHtml.addProperty("url","URL");
listaHtml.addProperty("dataInserimento","Data");

if(!bALTRORICHIEDENTE)
{	
	String sImage=context+"/img/pulsanti/dettaglio.png";
	
	if(opSicurezza.hasPermission(user,"modificaAnagrafica"))
	{
		sImage=context+"/img/pulsanti/dettaglio.png";
		listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","dettaglio",32,32,sImage,"Dettaglio",false,""));
		sImage=context+"/img/pulsanti/modifica.png";
		listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","dati",32,32,sImage,"Modifica",false,""));
		sImage=context+"/img/pulsanti/stampa.png";
		listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","stampa",32,32,sImage,"Stampa",false,""));
	}
	if(opSicurezza.hasPermission(user,"cancellaAnagrafica"))
	{	
	    sImage=context+"/img/pulsanti/elimina.png";
		listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","cancellazione",32,32,sImage,"Cancellazione",true,"Sei sicuro di voler cancellare questo contatto?"));
	}
	if(user.getTipoUtente().equalsIgnoreCase("AMM"))
	{
		/*sImage=context+"/img/pulsanti/report.png";
		listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","report",32,32,sImage,"Report",false,""));
		*/
		String preUrl=tempDir;
		listaHtml.addProperty("idimmagine","", new ImageURLFormat("url","anteprima",150,50,sImage,"Anteprima Immagine",false,"",preUrl));
	}	
	else
	{
	sImage=context+"/img/pulsanti/conferma.png";
	listaHtml.addProperty("idimmagine","", new ImageFormat("idimmagine","dati",32,32,sImage));
	}	
}
// Impostazione della grafica della tabella
AlternateTableStyler styler = new AlternateTableStyler("primo1","primo2");
listaHtml.setTableStyler(styler);

// Impostazione della validazione del form
formValidator.addValidationRule(new SimpleValidationRule("idimmagine","Selezionare una Anagrafica","notEmpty"));

String NomeFile="esportazione";

%>
<%out.clear();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="it.realt.webapps.utente.costanti.TipiUtente"%><html>
<head>
<title>Gestione Immagine</title>

<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css"
	rel="stylesheet" />
<tags:formValidation validator="${formValidator}" function="inviaDati"
	form="dati" />
<tags:formValidation validator="${formValidator}"
	function="inviaCancellazione" form="cancellazione" />

<script type="text/javascript" src="<%=context%>/js/validator.js">

function agisci() {
	document.dati.submit();
}

</script>

<script type="text/javascript">

function controlla_ricerca(){
	
	var f=document.ricerca;
	
	if (isNaN(f.telefono.value)|(f.telefono.value < 0)  ) {
	
	  alert("Nel campo TELEFONO, consentiti solo valori numerici e positivi!");
	  
	} else {

	 document.ricerca.submit();

	}
}

function resetta(){
	
	var f=document.ricerca;
	
	f.cognome.value="";
	f.citta.value="";
	f.url.value="";
	f.telefono.value="";
	f.nome.value="";
	f.email.value="";
}

function esportaExcel(){
	
	if (confirm("Vuoi procedere all'esportazione su File Excel?")){
		
		document.esporta.action='<%=context%>/servlet/ReportServletExcel?nomeFile=<%=NomeFile%>';
			document.esporta.codoper.value = 'esportaImmagine';
			document.esporta.mimetype.value = 'application/excel';
			document.esporta.tiporeport.value = 'xls';
			document.esporta.submit();

		}

	}
</script>
</head>
<body>
	<tags:menuFunzioni root="root" />
	<!-- qui inizia  la parte della RICERCA -->
	<br>
	<div align="center" class="titolo">Ricerca</div>
	<br>
	<div align="center">
		<form name="ricerca" action="<%=PORTIERE%>" method="post">
			<table class="listaRicerca">

				<tr ALIGN="LEFT">
					<td align="center" class="primo1">Id Immagine</td>
					<td align="center" class="primo1">Riferimento</td>
					<td align="center" class="primo1">Descrizione</td>
					<td><a href="javascript:controlla_ricerca();"><IMG
							SRC="<%=context%>/img/pulsanti/cerca.png" BORDER="0" alt="Cerca"
							title="Cerca"></a></td>
					<td><a
						href="immagineForm.jsp?nuovo=true&amp;tipo=<%= tipo.getCodice() %>"><img
							alt="Nuovo" title="Nuovo"
							src="<%=context%>/img/pulsanti/nuovo.png" border="0"></a></td>

				</tr>
				<tr ALIGN="LEFT">

					<td align="left"><span><input type="text"
							name="cognome"
							value='<%=(filtro.getCognome()==null)? "" :filtro.getCognome().replaceAll("%", "")%>'></span></td>
					<td align="left"><span><input type="text" name="nome"
							value='<%=(filtro.getNome()==null)? "" :filtro.getNome().replaceAll("%", "")%>'></span></td>
					<td align="left"><span><input type="text" name="url"
							value='<%=(filtro.getUrl()==null)? "" :filtro.getUrl().replaceAll("%", "")%>'></span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

				<tr ALIGN="LEFT">
					<td class="primo1">Indirizzo</td>
					<td class="primo1">Altri Dati</td>
					<td class="primo1">Altri Dati</td>
					<td><a href="javascript:resetta();"><img
							alt="Cancella Campi" title="Resetta Campi"
							src="<%=context%>/img/pulsanti/gomma.png" border="0"></a></td>
					<td><a href="javascript:esportaExcel();"><IMG
							SRC="<%=context%>/img/pulsanti/excel.png" BORDER="0"
							alt="Esporta in Excel" title="Esporta in Excel"></a></td>
				</tr>

				<tr ALIGN="LEFT">

					<td align="left"><span><input type="text" name="citta"
							value='<%=(filtro.getCitta()==null)? "" :filtro.getCitta().replaceAll("%", "")%>'></span></td>
					<td align="left"><span><input type="text"
							name="telefono"
							value='<%=(filtro.getTelefono()==null)? "" :filtro.getTelefono().replaceAll("%", "")%>'></span></td>
					<td align="left"><span><input type="text" name="email"
							value='<%=(filtro.getEmail()==null)? "" :filtro.getEmail().replaceAll("%", "")%>'></span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</TR>
			</table>

			<br> <input type="hidden" name="tipoImmagine"
				value="<%= tipo.getCodice() %>"> <input type="hidden"
				name="targetpage"
				value="backoffice/immagineList.jsp?tipo=<%= tipo.getCodice() %>">
			<input type="hidden" name="callpage" value="errore.jsp"> <input
				type="hidden" name="codoper" value="findAnagrafica">
		</form>

		<!-- qui inizia la VISUALIZZAZIONE DELLA TABELLA -->

		<table align="center">
			<%if(sFunzioneDA!=null){%><tr>
				<th>( da <%=sFunzioneDA%>)
				</th>
			</tr>
			<%} %>
		</TABLE>

		<br />
		<div class="listaHtml" scrolling="yes">
			<tags:lista listaHtml="${listaHtml}" paginazione="${pag}"
				thClass="primo" tableTags="align='center' class='lista'" />
			<tags:menuPagine paginazione="${pag}" />
		</div>

		<form name="dati" action="<%=PORTIERE%>" method="post">

			<input type="hidden" name="idimmagine"> <input type="hidden"
				name="codoper" value="dettaglioImmagine">
			<%if(!bALTRORICHIEDENTE){ %>
			<input type="hidden" name="targetpage"
				value="backoffice/immagineForm.jsp?tipo=<%= tipo.getCodice() %>">
			<%} else{ %>
			<input type="hidden" name="targetpage" value="<%=sritornaA%>">
			<%} %>
		</form>

		<form name="dettaglio" action="<%=PORTIERE%>" method="post">
			<input type="hidden" name="idimmagine"> <input type="hidden"
				name="codoper" value="dettaglioImmagine"> <input
				type="hidden" name="targetpage"
				value="backoffice/immagineDetail.jsp?tipo=<%= tipo.getCodice() %>">
		</form>

		<form name="stampa" action="<%=PORTIERE%>" method="post">
			<input type="hidden" name="idimmagine"> <input type="hidden"
				name="codoper" value="dettaglioImmagine"> <input
				type="hidden" name="targetpage"
				value="backoffice/immagineStampaDetail.jsp?tipo=<%= tipo.getCodice() %>">
		</form>


		<form name="cancellazione" action="<%=PORTIERE%>" method="post">
			<input type="hidden" name="idimmagine"> <input type="hidden"
				name="codoper" value="eliminaImmagine"> <input type="hidden"
				name="targetpage"
				value="backoffice/immagineList.jsp?tipo=<%= tipo.getCodice() %>">
		</form>

		<form name="esporta" action="<%=PORTIERE%>" method="post">
			<input type="hidden" name="nomeFile" value="<%=NomeFile%>" /> <input
				type="hidden" name="mimetype" value="application/excel" /> <input
				type="hidden" name="tiporeport" value="xls" /> <input type="hidden"
				name="idimmagine"> <input type="hidden" name="codoper"
				value="esportaAnagrafica"> <input type="hidden"
				name="targetpage"
				value="backoffice/immagineList.jsp?tipo=<%= tipo.getCodice() %>">
		</form>

		<form name="report" action="<%=PORTIERE%>" method="post">
			<input type="hidden" name="idimmagine"> <input type="hidden"
				name="codoper" value="dettaglioAudit"> <input type="hidden"
				name="targetpage" value="backoffice/immagineReport.jsp">
		</form>

	</div>

</body>
</html>
