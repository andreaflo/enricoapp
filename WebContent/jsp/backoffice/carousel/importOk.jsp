<?xml version="1.0" encoding="UTF-8"?>
<%@ page import="it.realt.webapps.i18n.*,it.realt.webapps.html.validationrule.*, it.realt.webapps.utente.dbbeans.*, it.realt.webapps.utente.mvc.control.*, java.util.*, it.realt.webapps.html.*,it.realt.webapps.html.tablestyler.*, it.realt.webapps.beans.*, java.text.*"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<tags:sicurezza idFunzione="report"/>

<%pageContext.setAttribute("languages", Languages.getInstance(), pageContext.PAGE_SCOPE);%>
<jsp:useBean id="listaHtml" class="it.realt.webapps.html.ListaHtml" scope="page"/>
<jsp:useBean id="pag" class="it.realt.webapps.html.Paginazione" scope="page"/>
<jsp:useBean id="formValidator" class="it.realt.webapps.html.FormValidator" scope="page"/>

<%
session.setAttribute("titolo","processi");

%>
<%out.clear();%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<link href="<%=context%>/jsp/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/jsp/stylepartedinamica.css" type="text/css" rel="stylesheet" />

<tags:formValidation validator="${formValidator}" function="inviaDati" form="dati"/>
<tags:formValidation validator="${formValidator}" function="inviaCancellazione" form="cancellazione"/>
<script type="text/javascript" src="<%=context%>/jsp/commons/js/validator.js"></script>

</head>
<body class="sfondo"><tags:menuFunzioni root="root" />
<br /><br />
<div align="center" class="sfondo_tabella" >

<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tr><th align="center" class="titolo" > <font color="#404040">Risultato Importazione</font></th></tr>
</table>

<br /><br />

<table>
      <tr align="left">
        <td><b>Importazione avvenuta con SUCCESSO!!!</b></td>
      </tr>
      <tr align="left">
        <td>&nbsp;</td>
      </tr>
      <tr align="left">
        <td>&nbsp;</td>
      </tr>
      <tr align="left">
        <td colspan="2" align="center">
	         <input type="button" name="Indietro" value="Menu"  onclick="location.href ='../../main.jsp';" /> &nbsp;&nbsp;&nbsp; 
	         <input type="button" name="Indietro" value="Configura carousel"  onclick="location.href ='carousel.jsp';" /> &nbsp;&nbsp;&nbsp; 
        </td>
      </tr>
</table>

</div>

</body>
</html>
