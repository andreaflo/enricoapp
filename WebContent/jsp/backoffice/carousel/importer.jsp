<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ page import="it.realt.beans.*"%>
<%@page import="actions.*"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/simpleTree.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/smoothness/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=context%>/javascript/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="<%=context%>/javascript/jquery.simple.tree.js"></script>
<script type="text/javascript" src="<%=context%>/javascript/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<%=context%>/javascript/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=context%>/javascript/messages_it.js"></script>

<SCRIPT language=JavaScript >
function valida() 
{
	var ext = document.dati.indirizzo.value;
	if(ext.length != 0) {
		document.dati.submit();
	} else alert('Scegliere un file xls, prima di continuare!!!');		
}
	
</SCRIPT>

</head>
<body><tags:menuFunzioni root="root" />
<br>
<center>
<TABLE class="tabella" width="50%">
	<tr>
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td align="center" style="font-weight: bold">Selezionare un file Excel</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
	</tr>
</TABLE>
<form action="<%=PORTIERE%>" method="post" name="dati" enctype="multipart/form-data">
<input type="hidden" name="codoper" value="importaUtenti">
<input type="hidden" name="targetpage" value="catalogo/importer/importOk.jsp" />

<table width="50%">
	<tr>
		<td align="center" style="font-weight: bold"> 
			FILE:&nbsp;&nbsp;
			<input type="file" name="indirizzo" size="30" >			
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
	
	<tr align="center">
		<td colspan="2">
			<input  class="bottone" type="button" value="carica" onclick="valida()">
		</td>
	</tr>
</table>

</form>

</center>


</body>
</html>