<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="actions.immagine.ImportaImmaginiAction"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ include file="/jsp/commons/noCache.jspf"%>
<%@ include file="/jsp/commons/costanti.jspf"%>
<%@ page import="actions.immagine.ImportaImmaginiAction"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="<%=context%>/css/style.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/stylepartedinamica.css" type="text/css" rel="stylesheet" />
<link href="<%=context%>/css/simpleTree.css" type="text/css" rel="stylesheet" />


<SCRIPT language=JavaScript >
function valida(idimmagine) 
{
	var ext = document.dati.indirizzo.value;
	document.dati.idimmagine.value =   idimmagine;
	if(ext.length != 0) {
		document.dati.submit();
	} else alert('Scegliere un file immagine, prima di continuare!!!');		
}

function nascondiDisabilitati() 
{
	var riga1 = document.getElementById('riga1');
	var riga2 = document.getElementById('riga2');
	var riga3 = document.getElementById('riga3');
	var riga4 = document.getElementById('riga4');
	var riga5 = document.getElementById('riga5');
	
	if (riga1.getElementsByTagName("input").item(0).valueOf("1"))
			riga1.setAttribute("display","inline");
		else
			riga1.setAttribute("display","none");
	
}
	
</SCRIPT>
<%ImportaImmaginiAction importa= new ImportaImmaginiAction(); 

String path = session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL();
System.out.println("PATH: "+ path);

%>

<%


%>
</head>
<body>
<br/>
<br/>
<tags:menuFunzioni root="user" />
<br/>
<tags:sicurezza idFunzione="postContatto"/>

<br>
<center>
<TABLE class="tabella" width="50%">
	<tr>
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td align="center" style="font-weight: bold">Selezionare le immagini da visualizzare in home page</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
	</tr>
</TABLE>
<form action="<%=PORTIERE%>" method="post" name="dati" enctype="multipart/form-data">
<input type="hidden" name="codoper" value="importaImmagini">
<input type="hidden" name="targetpage" value="catalogo/importer/importOk.jsp" />

<table width="50%">
	<tr>
		<td align="center" style="font-weight: bold"> 
			FILE:&nbsp;&nbsp;
			<input type="file" name="indirizzo" size="30" >			
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
</table>
<table width="50%">
	<tr id=riga1 align="center">
	<td colspan="2">
			<p> Immagine 1 </p>
			
		</td>
		<td colspan="2">
			<input  id="1" 1class="bottone" type="button" value="carica" title="carica" onclick="valida(1)" >
		</td>

	<td>
	<% if (ImportaImmaginiAction.fileExists(session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL()+"/1"+ImportaImmaginiAction.getEXTENSION())){%>
		<A HREF="<%=context%>/<%=ImportaImmaginiAction.getCAROUSEL()%>/1<%=ImportaImmaginiAction.getEXTENSION()%>" target="blank">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<% 
	} else {
			%>
		<A HREF="#">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<%} %>
		</TD>
	<td>
	<input type="checkbox" name="attivo" value="1" /> Attivo
	</td>
	
	<td>
	<input type="button" name="Salva" value="" onclick="nascondiDisabilitati()">
	</td>
	
	</tr>
	
</table>

<table width="50%">
	<tr id=riga2 align="center">
	<td colspan="2">
			<p> Immagine 2 </p>
			
		</td>
		<td colspan="2">
			<input id="2" class="bottone" type="button" value="carica" title="carica" onclick="valida(2)">
			
		</td>
	
	<td>
	<% if (ImportaImmaginiAction.fileExists(session.getServletContext().getRealPath("/")+"/"+ ImportaImmaginiAction.getCAROUSEL()+"/2"+ImportaImmaginiAction.getEXTENSION())){%>
		<A HREF="<%=context%>/<%=ImportaImmaginiAction.getCAROUSEL()%>/2<%=ImportaImmaginiAction.getEXTENSION()%>" target="blank">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<% 
	} else {
			%>
		<A HREF="#">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<%} %>
		</TD>
	<td>
	<input type="checkbox" name="attivo" value="1" /> Attivo
	</td>
	</tr>
</table>

<table width="50%">
	<tr id=riga3 align="center">
	<td colspan="2">
			<p> Immagine 3 </p>
			
		</td>
		<td colspan="2">
			<input id="3" class="bottone" type="button" value="carica" title="carica" onclick="valida(3)">
			
		</td>
	
	<td>
	<% if (ImportaImmaginiAction.fileExists(session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL()+"/3"+ImportaImmaginiAction.getEXTENSION())){%>
		<A HREF="<%=context%>/<%=ImportaImmaginiAction.getCAROUSEL()%>/3<%=ImportaImmaginiAction.getEXTENSION()%>" target="blank">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<% 
	} else {
			%>
		<A HREF="#">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<%} %>
		</TD>
	<td>
	<input type="checkbox" name="attivo" value="1" /> Attivo
	</td>
	</tr>
</table>


	<table width="50%">
	<tr id=riga4 align="center">
	<td colspan="2">
			<p> Immagine 4 </p>
			
		</td>
		<td colspan="2">
			<input id="4" class="bottone" type="button" value="carica" title="carica" onclick="valida(4)">
			
		</td>
	
	<td>
	<% System.out.println("PATH IMMAGINE :" +
			
			session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL()+"/4"+ImportaImmaginiAction.getEXTENSION());
	
	if (ImportaImmaginiAction.fileExists(session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL()+"/4"+ImportaImmaginiAction.getEXTENSION())){%>
		<A HREF="<%=context%>/<%=ImportaImmaginiAction.getCAROUSEL()%>/4<%=ImportaImmaginiAction.getEXTENSION()%>" target="blank">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<% 
	} else {
			%>
		<A HREF="#">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<%} %>
		</TD>
	<td>
	<input type="checkbox" name="attivo" value="1" /> Attivo
	</td>
	</tr>
	
	</table>
	<table width="50%">
	<tr id=riga5 align="center">
	<td colspan="2">
			<p> Immagine 5 </p>
			
		</td>
		<td colspan="2">
			<input id="5" class="bottone" type="button" value="carica" title="carica" onclick="valida(5)">
		</td>
	
	
	<td>
	<% if (ImportaImmaginiAction.fileExists(session.getServletContext().getRealPath("/")+"/"+ImportaImmaginiAction.getCAROUSEL()+"/5"+ImportaImmaginiAction.getEXTENSION())){%>
		<A HREF="<%=context%>/<%=ImportaImmaginiAction.getCAROUSEL()%>/5<%=ImportaImmaginiAction.getEXTENSION()%>" target="blank">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<% 
	} else {
			%>
		<A HREF="#">
		<IMG SRC="<%=context%>/img/pulsanti/dettaglio.png" width="20" height="21" BORDER="0" alt="Dettaglio"></A>
		<%} %>
		</TD>
	<td>
	<input type="checkbox" name="attivo" value="1" /> Attivo
	</td>
	</tr>
	<tr>
	<td>
	<input type="hidden" name="idimmagine" value="">
	</td>
	</tr>
</table>

</form>

</center>


</body>
</html>