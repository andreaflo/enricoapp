<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0042)https://www.stampagrafica.it/ita/news.aspx -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico">
<link rel="shortcut icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link href="./News_files/css" rel="stylesheet" type="text/css">
<link href="./News_files/stampagrafica.css" rel="stylesheet" type="text/css">
<link href="./News_files/animenu.css" rel="stylesheet" type="text/css">
<link href="./News_files/glide.css" rel="stylesheet" type="text/css">
<script async="" src="./News_files/analytics.js.download"></script><script type="text/javascript" src="./News_files/jquery.min.js.download"></script>
<title>News</title>

<!-- GOOGLE ANALITYCS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->

</head>

<body>
<div class="frame">
  <div class="bit-1">
    <div id="topbar">
      <div class="quickcontact">
        <p class="vcenter"><strong>Contatti veloci:</strong><br>
          tel. 045.7830311<br>
          info@coronaSrl.it</p>
      </div>
      ﻿ <div class="lingue"><p class="vcenter"><a href="./Home_files/ITA/index.html">ITA</a> | ENG</p></div>


      <div class="logo"><a href="Home.jsp"><img src="./News_files/Logo-StampaGrafica.jpg" width="250" height="100"></a></div>
    </div>
    <!-- fine topbar --> 
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame ------------------------------->
<div class="frame">
  <div class="bit-1 centertxt">
    ﻿<nav class="animenu" id="nav"> 
  <button class="animenu__toggle">
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
    <span class="animenu__toggle__bar"></span>
  </button>
  <ul class="animenu__nav">
   
   
    <li>
      <a href="Azienda.jsp">azienda</a>
      <ul class="animenu__nav__child">
        <li><a href="I nostri Reparti">i nostri reparti</a></li>
        <li><a href="Possibilita.jsp">i nostri clienti</a></li>
       </ul>
    </li> 
    <li>
      <a href="Prodotti.jsp">prodotti</a>
    </li>    
    <li>
      <a href="ProdottiEServizi.jsp">servizi</a>
     </li>
    <li>
      <a href="News.jsp">news</a>
    </li>
    <li>
      <a href="Contatti.jsp">contatti</a>
    </li>   
    
                     
  </ul>
</nav>


  </div>
</div>
<div class="frame">
  <div class="bit-1"> 
    
    <!-- Slideshow 4 -->
    
    <div class="slider">
      <ul class="slider__wrapper">
        <li class="slider__item"><img src="./News_files/slide01.jpg"></li>
      </ul>
    </div>
    
    <!-- fine Slideshow 4 --> 
    
  </div>
  <!--fine bit-1 --> 
</div>
<!-------------------------fine frame -------------------------------> 












<!----------------------------------- FOOTER ------------------------------------------>
<div class="frame">
  ﻿<!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


<div id="footer">

            <div class="bit-4">
                <div class="spot7">
                <p class="txt09-logo">s|g<br>stampa grafica</p>
                
                <p class="txt10">Stampa Grafica vive di sfide.
Nasce nel 1988 proprio da una sfida di Guido
Santi, cresciuto tra macchine tipografiche e
sistemi di stampa. Oggi, i progetti più complessi e
le sfide alla creatività trovano la soluzione più
consona, nei tempi stabiliti.</p>
              </div><!-- fine SPOT -->
             </div>
             
             
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">I FOCUS PRINCIPALI</p>
                <hr class="linea">
                <p class="txt10">Prestampa<br>
Stampa Offset<br>
Stampa Digitale<br>
Unico - stampa di prototipi, mock up e pezzi unici<br>
Collaborazione con agenzie creative<br>
Consulenza di stampa</p>
              </div><!-- fine SPOT -->
            </div>
            
            
              
            
            <div class="bit-4">
                <div class="spot7">
                <p class="txt09">CONTATTI</p>
                <hr class="linea">
                <p class="txt10">Stampa Grafica Srl<br>
Via Zambona, 12<br>
37031 Illasi VR<br>
P.I. 03053960237<br>
tel. +39 045.7830311<br>
mail. info@coronaSrl.it</p>
              </div><!-- fine SPOT -->
            </div>
            
             <div class="bit-4">
              <div class="spot7">
                <p class="txt09">CREDITS</p>
                <hr class="linea">
                <p class="txt10">Sito realizzato da <a href="http://www.linkedin.it/" target="_blank">Andrea Giraldi</a></p>
                <p class="txt10"><a href="InformativaCookies.html" target="_blank">Cookie policy</a></p>
              </div><!-- fine SPOT -->
            </div>
            
            
</div>

<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
  <div style="clear:both; height:60px; overflow:hidden"></div>
<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
</div>
<!-------------------------fine frame ------------------------------->    
<!-------------------------------- fine FOOTER ---------------------------->

<script src="./News_files/animenu.js.download"></script>

</body></html>