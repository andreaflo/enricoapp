<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0054)https://www.stampagrafica.it/ita/i-nostri-clienti.aspx -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- FOR RESPONSIVE MOBILE DISPLAY-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Stampa Grafica Srl - Via Zambona, 12 -37031 Illasi VR - Tel.+39 045.7830311">

<!-- ALTRI META
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2014 BCF">
<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2020 11:12:01 GMT">
<META HTTP-EQUIV="REFRESH" CONTENT="15;URL=http://www.miosito.com/index.html">
<META NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
 -->

<!-- FAVICON-->
<link rel="shortcut icon"
	href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="shortcut icon"
	href="https://www.stampagrafica.it/favicon.ico">
<link rel="shortcut icon"
	href="https://www.stampagrafica.it/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.stampagrafica.it/favicon.ico"
	type="image/x-icon">
<link href="./Possibilita di stampa_files/css" rel="stylesheet"
	type="text/css">
<link href="./Possibilita di stampa_files/stampagrafica.css"
	rel="stylesheet" type="text/css">
<link href="./Possibilita di stampa_files/animenu.css" rel="stylesheet"
	type="text/css">
<link href="./Possibilita di stampa_files/glide.css" rel="stylesheet"
	type="text/css">
	<link href="./Possibilita di stampa_files/carousel.css" rel="stylesheet" type="text/css">
<link href="./Possibilita di stampa_files/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="./Possibilita di stampa_files/connected-carousels.css" rel="stylesheet"
	type="text/css">
	
<script async=""
	src="./Possibilita di stampa_files/analytics.js.download"
	type="text/javascript"></script>
<script type="text/javascript" src="./Possibilita di stampa_files/jquery.js"></script>
<script type="text/javascript" src="./Possibilita di stampa_files/jquery-ui.js"></script>
<script type="text/javascript" src="./Possibilita di stampa_files/glide.js.download"></script>
<script type="text/javascript" src="./Possibilita di stampa_files/jquery.jcarousel.js"></script>

<!-- SLIDER -->
<script type="text/javascript">
	(function($) {
		// This is the connector function.
		// It connects one item from the navigation carousel to one item from the
		// stage carousel.
		// The default behaviour is, to connect items with the same index from both
		// carousels. This might _not_ work with circular carousels!
		var connector = function(itemNavigation, carouselStage) {
			return carouselStage.jcarousel('items').eq(itemNavigation.index());
		};

		$(function() {
			// Setup the carousels. Adjust the options for both carousels here.
			var carouselStage = $('.carousel-stage').jcarousel();
			var carouselNavigation = $('.carousel-navigation').jcarousel();

			// We loop through the items of the navigation carousel and set it up
			// as a control for an item from the stage carousel.
			carouselNavigation.jcarousel('items').each(function() {
				var item = $(this);

				// This is where we actually connect to items.
				var target = connector(item, carouselStage);

				item.on('jcarouselcontrol:active', function() {
					carouselNavigation.jcarousel('scrollIntoView', this);
					item.addClass('active');
				}).on('jcarouselcontrol:inactive', function() {
					item.removeClass('active');
				}).jcarouselControl({
					target : target,
					carousel : carouselStage
				});
			});

			// Setup controls for the stage carousel
			$('.prev-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-stage').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			// Setup controls for the navigation carousel
			$('.prev-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '-=1'
			});

			$('.next-navigation').on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			}).on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			}).jcarouselControl({
				target : '+=1'
			});

			$('.carousel').jcarousel({
				 wrap: 'circular',
				 scroll : 1
			}).jcarouselAutoscroll({
				interval : 5000,
				target : '+=1',
				autostart : true
			}).hover(function() {
			    $(this).jcarouselAutoscroll('stop');
			}, function() {
			    $(this).jcarouselAutoscroll('start');

			});
		});
	})(jQuery);

	$('.jcarousel').jcarousel({
		wrap : 'circular'
	}).jcarouselAutoscroll({
		interval : 3000,
		 target: '+=1'
	});
</script>


<title>Possibilita di stampa</title>

<!-- GOOGLE ANALITYCS-->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1442714-23', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<!-- FINE GOOGLE ANALYTICS -->


<style type="text/css" media="screen">

.connected-carousels .prev-navigation {
    left: -220px;
    top: -50px;
    text-indent: 6px;
</style>
<style type="text/css" media="screen">
.connected-carousels .next-navigation {
    right: -220px;
    top: -50px;
    text-indent: 20px;
}
</style>
</head>

<body>
	<div class="frame">
		<div class="bit-1">
			<div id="topbar">
				<div class="quickcontact">
					<p class="vcenter">
						<strong>Contatti veloci:</strong><br> tel. 045.7830311<br>
						info@coronasrl.it
					</p>
				</div>
				﻿
				<div class="lingue">
					<p class="vcenter">
						<a href="./Home_files/ITA/index.html">ITA</a> | ENG
					</p>
				</div>


				<div class="logo">
					<a href="./Home.jsp"><img
						src="./Possibilita di stampa_files/Logo-StampaGrafica.jpg"
						width="250" height="100" alt=""></a>
				</div>
			</div>
			<!-- fine topbar -->
		</div>
		<!--fine bit-1 -->
	</div>
	<!-------------------------fine frame ------------------------------->
	<div class="frame">
		<div class="bit-1 centertxt">
			﻿
			<nav class="animenu" id="nav">
				<button class="animenu__toggle">
					<span class="animenu__toggle__bar"></span> <span
						class="animenu__toggle__bar"></span> <span
						class="animenu__toggle__bar"></span>
				</button>
				<ul class="animenu__nav">


					<li><a href="Azienda.jsp">azienda</a>
						<ul class="animenu__nav__child">
							<li><a href="I nostri Reparti.jsp">i nostri reparti</a></li>
							<li><a href="Possibilita.jsp">i nostri clienti</a></li>
						</ul></li>
					<li><a href="Prodotti.jsp">prodotti</a></li>
					<li><a href="ProdottiEServizi.jsp">servizi</a></li>
					<li><a href="News.jsp">news</a></li>
					<li><a href="Contatti.jsp">contatti</a></li>


				</ul>
			</nav>


		</div>
	</div>
	<div class="frame">
		<div class="bit-1">

			<!-- Slideshow 4 -->

			<div align="left" id="centratore">
				<div class="frame">
					<div class="wrapper">
						<div class="connected-carousels">
							<div class="stage">
								<div class="carousel carousel-stage">
									<ul>
										<li><img src="./Home_files/slide01.jpg" width="1200"
											height="400" alt=""></li>
										<li><img src="./Home_files/slide02.jpg" width="1200"
											height="400" alt=""></li>
										<li><img src="./Home_files/slide03.jpg" width="1200"
											height="400" alt=""></li>
										<li><img src="./Home_files/slide04.jpg" width="1200"
											height="400" alt=""></li>
									</ul>
								</div>
							<div class="navigation">
								<a href="#" class="prev prev-navigation">&lsaquo;</a> <a
									href="#" class="next next-navigation">&rsaquo;</a>
							</div>
						</div>
						</div>
						</div>
						</div>
						</div>
						<!-- fine Slideshow 4 -->

					</div>
					<!--fine bit-1 -->
					<div></div>
					<!-------------------------fine frame ------------------------------->



					<div class="frame">
						<div class="bit-1 payoffbox">
							<p class="payoff noborder">Chi ha fatto la storia di Stampa
								Grafica</p>
						</div>
						<!--fine bit-1 -->
						<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
						<div style="clear: both; height: 50px; overflow: hidden"></div>
						<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

					</div>
					<!-------------------------fine frame ------------------------------->


					<div class="frame">
						<div class="bit-2 lista-clienti-sx">
							<ul>
								<li>Adiatek</li>
								<li>A.I.A.</li>
								<li>AGSM Verona</li>
								<li>Alluflon</li>
								<li>Alphabet Italia</li>
								<li>Antera</li>
								<li>Antico Cadore</li>
								<li>Asolo Gold</li>
								<li>AUDI</li>
								<li>Bang &amp; Olufsen</li>
								<li>Bauli</li>
								<li>BMW</li>
								<li>CALZEDONIA</li>
								<li>Calzificio San Pellegrino</li>
								<li>Campagnola Giuseppe</li>
								<li>Cantina Sociale di Soave</li>
								<li>Carpigiani Group</li>
								<li>Cartiere Fedrigoni</li>
								<li>Citroën Italia</li>
								<li>Condè Nast Pubblications</li>
								<li>Dani</li>
								<li>Demeral</li>
								<li>Diesel</li>
								<li>Distillerie Marzadro</li>
								<li>Ducati</li>
								<li>Estel Office</li>
								<li>F.M. Bottega D'arte</li>
								<li>Ferrari Automobili</li>
								<li>Futura</li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<!--fine bit-2 -->
						<div class="bit-2 lista-clienti-dx">
							<ul>
								<li>FIAT Group</li>
								<li>Glaxo Smith Kline</li>
								<li>Hasselblad</li>
								<li>Hotel Cavallino Bianco di Ortisei</li>
								<li>Hotel Rosa Alpina</li>
								<li>Hotel Alpen Royal</li>
								<li>Hotel Palace Merano</li>
								<li>Imaforni</li>
								<li>INX Digital</li>
								<li>Isap Packaging</li>
								<li>ISEO Serrature</li>
								<li>KIA Italia</li>
								<li>Kuehne + Nagel</li>
								<li>Marangoni</li>
								<li>Marzocchi</li>
								<li>Menotti Specchia</li>
								<li>N.G. Group</li>
								<li>Negroni</li>
								<li>Oral-B</li>
								<li>Pataviumart</li>
								<li>Pedrollo</li>
								<li>Polsit</li>
								<li>Pregno</li>
								<li>Procter &amp; Gamble</li>
								<li>Subaru</li>
								<li>Tadema</li>
								<li>Volkswagen Group Italia</li>
								<li>Wella Italia</li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<!--fine bit-2 -->
					</div>
					<!--fine FRAME -->






					<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
					<div style="clear: both; height: 50px; overflow: hidden"></div>
					<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->

					<!----------------------------------- FOOTER ------------------------------------------>
					<div class="frame">
						﻿
						<!--<link href="../css/stampagrafica.css" rel="stylesheet" type="text/css" />-->


						<div id="footer">

							<div class="bit-4">
								<div class="spot7">
									<p class="txt09-logo">
										s|g<br>stampa grafica
									</p>

									<p class="txt10">Stampa Grafica vive di sfide. Nasce nel
										1988 proprio da una sfida di Guido Santi, cresciuto tra
										macchine tipografiche e sistemi di stampa. Oggi, i progetti
										più complessi e le sfide alla creatività trovano la soluzione
										più consona, nei tempi stabiliti.</p>
								</div>
								<!-- fine SPOT -->
							</div>


							<div class="bit-4">
								<div class="spot7">
									<p class="txt09">I FOCUS PRINCIPALI</p>
									<hr class="linea">
									<p class="txt10">
										Prestampa<br> Stampa Offset<br> Stampa Digitale<br>
										Unico - stampa di prototipi, mock up e pezzi unici<br>
										Collaborazione con agenzie creative<br> Consulenza di
										stampa
									</p>
								</div>
								<!-- fine SPOT -->
							</div>




							<div class="bit-4">
								<div class="spot7">
									<p class="txt09">CONTATTI</p>
									<hr class="linea">
									<p class="txt10">
										Stampa Grafica Srl<br> Via Zambona, 12<br> 37031
										Illasi VR<br> P.I. 03053960237<br> tel. +39
										045.7830311<br> mail. info@stampagrafica.it
									</p>
								</div>
								<!-- fine SPOT -->
							</div>

							<div class="bit-4">
								<div class="spot7">
									<p class="txt09">CREDITS</p>
									<hr class="linea">
									<p class="txt10">
										Sito realizzato da <a href="http://www.linkedin.it/"
											target="_blank">Andrea Giraldi</a>
									</p>
									<p class="txt10">
										<a href="InformativaCookies.html" target="_blank">Cookie policy</a>
									</p>
								</div>
								<!-- fine SPOT -->
							</div>


						</div>

						<!-- INIZIO DIV che serve per far adattare al container   NON TOGLIERE -->
						<div style="clear: both; height: 60px; overflow: hidden"></div>
						<!-- FINE DIV che serve per far adattare al container   NON TOGLIERE -->
					</div>
					<!-------------------------fine frame ------------------------------->
					<!-------------------------------- fine FOOTER ---------------------------->


					<script src="./Possibilita di stampa_files/animenu.js.download"
						type="text/javascript"></script>
</body>
</html>