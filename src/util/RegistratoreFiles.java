package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import dao.ContatoreDAO;
import dbobjects.Immagine;
import it.realt.general.BundledException;
import it.realt.webapps.file.ModuloFile;
import it.realt.webapps.file.costanti.CostantiModuloFile;

public class RegistratoreFiles {
	
	private final static String[] estensione = {"jpg","jpeg","png","xcf"};
	
	public static int contatore;
	
	private static ContatoreDAO contatoreDao = new ContatoreDAO();
	
	public static Immagine numeraFiles(File file,Immagine immagine) {
		String map = null;
		int contatoreAggiornato  = contatoreDao.incrementa();
		if (contatoreAggiornato > 0) {
			String[] str = file.toString().split(File.separator);
			for (String s : str) {
				for (String e : estensione) {
				if (s.contains(e)) {
					map = s;
					break;
				}
				}
			}
			immagine.setPath(map);
			immagine.setUrl("" + contatoreAggiornato);
			return immagine;
		}
		else
			return null;
		// TODO Auto-generated method stub
		
	}
	
	public static Immagine renameFile(Immagine immagine) throws IOException, BundledException {
		
		String saveRootPath = CostantiModuloFile.getInstance().saveRootPath+ File.separator;
		 
		// File (or directory) with old name
		File file = new File(saveRootPath +immagine.getPath());

		// File (or directory) with new name
		File file2 = new File(saveRootPath + immagine.getUrl());

		if (file2.exists())
		   throw new java.io.IOException("file exists");

		// Rename file (or directory)
		Path temp = Files.move(Paths.get(file.getAbsolutePath()), Paths.get(file2.getAbsolutePath()));
		if(temp != null) 
	        { 
	            System.out.println("File renamed and moved successfully"); 
	        } 
	        else
	        { 
	            System.out.println("Failed to move the file"); 
	        } 
		//Files.delete(Paths.get(file.getPath())); 
		immagine.setUrl(file2.toString());
		return immagine;
	}
	
	public static Immagine deleteFile(Immagine immagine) throws IOException, BundledException {
		
		String saveRootPath = CostantiModuloFile.getInstance().saveRootPath+ File.separator;
		 
		// File (or directory) with old name
		File file = new File(immagine.getUrl());

		if (!file.exists())
		   throw new java.io.IOException("file not exists");
		
		Files.delete(Paths.get(file.getPath())); 
		return immagine;
	}
}
