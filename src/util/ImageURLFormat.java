 package util;
 
 import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import dao.ImmagineDAOImpl;
import dao.ImmagineDao;
import dbobjects.Immagine;
 
 public class ImageURLFormat
   extends Format
 {
   /**
	 * 
	 */
	private static final long serialVersionUID = 6309797613661409967L;
	
	private ImmagineDao dao = new ImmagineDAOImpl();
	
private static int defaultwidth = 22;
   private static int defaultheight = 22;
   
   public static final String defaultNomeForm = "dati";
   
   public static final String defaultImageName = "immagine.jpg";
   
   public static final String defaultImageTitle = "";
   private String nomeForm;
   private String propertyName;
   private String imageName;
   private String imageTitle;
   private int imageWidth;
   private int imageHeight;
   private boolean confirm = false;
   private String confirmMessage;
   private String imageURL;
   
   public ImageURLFormat(String propertyName) {
     this(propertyName, "dati", defaultwidth, defaultheight, "immagine.jpg");
   }
   
   public ImageURLFormat(String propertyName, String nomeForm) {
     this(propertyName, nomeForm, defaultwidth, defaultheight, "immagine.jpg");
   }
   
   public ImageURLFormat(String propertyName, String nomeForm, String imageName) {
     this(propertyName, nomeForm, defaultwidth, defaultheight, imageName);
   }
   
   public ImageURLFormat(String propertyName, String nomeForm, int width, int height, String imageName) {
     this(propertyName, nomeForm, width, height, imageName, "");
   }

   public ImageURLFormat(String propertyName, String nomeForm, int width, int height, String imageName, String url) {
     this(propertyName, nomeForm, width, height, imageName, "", false, "", url);
   }
   
   public ImageURLFormat(String propertyName, String nomeForm, int width, int height, String imageName, String imageTitle, boolean bConfirm, String confirmMessage,String url) {
     this.nomeForm = nomeForm;
     this.propertyName = propertyName;
     this.imageName = imageName;
     this.imageWidth = width;
     this.imageHeight = height;
     this.imageTitle = imageTitle;
     this.confirm = bConfirm;
     this.confirmMessage = confirmMessage;
     this.imageURL = url;
   }
   
   public Object parseObject(String source, ParsePosition pos) {
     throw new UnsupportedOperationException();
   }
   
   public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
     
	 try {
	 Immagine detail = dao.getImmagine(obj.toString());
	 int i = 0;
	 String str = detail.getUrl();
	 String splitted  = str.split("/home/andrea/WorkspaceTest")[1];
	 String splitted2  = splitted.replace("/WebContent","");
	 String pre = "http://localhost:8080"  ;
     String imgStr = "<IMG SRC=\"" + pre + splitted2 + "\"";
     if (!"".equalsIgnoreCase(this.imageTitle))
     {
       imgStr = imgStr + " alt=\"" + this.imageTitle + "\" title=\"" + this.imageTitle + "\" ";
     }
     imgStr = imgStr + "border=0 width=" + this.imageWidth + "  height=" + this.imageHeight;
     toAppendTo.append(imgStr);
     toAppendTo.append("\" /></A>");
     return toAppendTo;
	   }
     catch(Exception e) {
    	 e.printStackTrace();
     }
	return toAppendTo;
   }
 }
