package it;

import it.realt.webapps.html.PluginConstants;

public class TipiEnrico extends PluginConstants {
	public TipiEnrico() {

		this(null);
	}

	public TipiEnrico(final String selectName) {
		super(selectName);
		super.add(1, "FAQ", "Faq", true);
	}
}