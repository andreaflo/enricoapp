package it;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

public class HibernateUtils {
	private static  SessionFactory sessionFactory = buildSessionFactory();

	public static final DateFormat DTF = new SimpleDateFormat("dd-MM-yyyy");

	private static SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				
				Configuration cfg = new Configuration();
				sessionFactory = cfg.configure("/config/hibernate.cfg.xml").buildSessionFactory();
				return sessionFactory;        
				//new Configuration().configure().buildSessionFactory(); 
	        }
			
			return sessionFactory;
			
		} catch (final Throwable ex) {
			System.err.println("SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void closeConnection(final Connection conn) throws SQLException {

		getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class)
				.closeConnection(conn);
	}

	private static String decapitalize(final String string) {

		if ((string == null) || (string.length() == 0)) {
			return string;
		}
		final char c[] = string.toCharArray();
		c[0] = Character.toLowerCase(c[0]);
		return new String(c);
	}

	public static Connection getConnection() throws SQLException {

		return getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class)
				.getConnection();
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	/* metodo utilizzato al posto di session.beginTransaction() che permette di verificare se c'� gia n atransazione attiva restituisce
	 * 
	 * quella attiva altrimenti ne crea una nuova
	 *  
	 */
	public static Transaction openTransactionIfNoActive(final Session session) {
		
		if(session.getTransaction().isActive()) {
			
			return session.getTransaction();
			
		}
		
		return session.beginTransaction();
		
	}

	public static <T> List<T> select(final Session session, final T dbobject) {

		try {
			final StringBuilder where = new StringBuilder(" 1 = 1 ");
			for (final Method method : dbobject.getClass().getDeclaredMethods()) {
				if (method.getName().contains("get") && !method.getReturnType().isInterface()
						&& !method.getReturnType().isArray() && (method.invoke(dbobject, null) != null)) {
					final String field = decapitalize(method.getName().replace("get", ""));
					where.append(" AND ").append(field).append(" = :").append(field);
				}
			}
			final StringBuilder from = new StringBuilder();
			from.append(" FROM ").append(dbobject.getClass().getSimpleName()).append(" WHERE ");
			return session.createQuery(from.toString() + where.toString()).setProperties(dbobject).getResultList();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static <T> List<T> select(final T dbobject) {

		try (final Session hbSession = HibernateUtils.getSessionFactory().openSession()) {
			return select(hbSession, dbobject);
		}
	}

	public static <T> T selectFirst(final Session session, final T dbobject) {

		final List<T> result = select(session, dbobject);
		if ((result != null) && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	public static <T> T selectFirst(final T dbobject) {

		try (final Session hbSession = HibernateUtils.getSessionFactory().openSession()) {
			return selectFirst(hbSession, dbobject);
		}
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

	public static final String toDate(final Date date) {

		return "STR_TO_DATE('" + DTF.format(date) + "', '%d-%m-%Y')";
	}

	public static final String toDate(final String date) {

		return "STR_TO_DATE('" + date + "', '%d-%m-%Y')";
	}
}
