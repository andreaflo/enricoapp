package it;


import it.realt.webapps.utente.ModuloActionBase;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ModuloEnrico extends ModuloActionBase {

	public Collection getActionFiles() {
		
		List ret = new LinkedList();
		ret.add("enrico.xml");
		return ret;
	}

	public String getDescrizione() {
		
		return "moduloEnrico";
	}

	public String getVersion() {
		
		return "1.0";
	}

}
