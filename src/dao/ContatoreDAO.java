package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dbobjects.Contatore;
import it.HibernateUtils;

public class ContatoreDAO {

	private SessionFactory sessionFactory;

	private org.hibernate.Transaction transaction;

	private Session createTransaction(){

		if (sessionFactory == null)
			sessionFactory = HibernateUtils.getSessionFactory();

		Session session = null;

		if (sessionFactory.isOpen()){

			session = sessionFactory.getCurrentSession();

		}

		else {
			session = sessionFactory.openSession();
		}

		if (session.getTransaction().isActive()) {
			transaction = session.getTransaction();
		}
		else {
			transaction = session.beginTransaction();
		}
		return  session;
	}

	public Contatore getContatore() {
		Session session = createTransaction();
		return session.createQuery("FROM Contatore a", Contatore.class).getSingleResult();
	}

	public void saveContatore(Contatore contatore) {
		Session session = createTransaction();
		session.save(contatore);
		transaction.commit();
	}

	public int incrementa() {
		Contatore current = this.getContatore();
		current.setContatore(current.getContatore() + 1);
		saveContatore(current);
		return current.getContatore();
	}
}
