package dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import dbobjects.Contatto;
import dbobjects.Immagine;
import it.HibernateUtils;

public class ContattoDaoImpl implements ContattoDAO {
	
    private SessionFactory sessionFactory;
    
    private org.hibernate.Transaction transaction;
	
    public ContattoDaoImpl() {
    	try {
    		}
    		catch(Exception e) {
    			e.printStackTrace();
    		}
	}
    
    private Session createTransaction(){
    	
    	if (sessionFactory == null)
    		sessionFactory = HibernateUtils.getSessionFactory();
        
        Session session = null;
        
        if (sessionFactory.isOpen()){
        
		session = sessionFactory.getCurrentSession();
		
        }
        
        else {
        	session = sessionFactory.openSession();
        }
		
		if (session.getTransaction().isActive()) {
			transaction = session.getTransaction();
		}
		else {
			transaction = session.beginTransaction();
		}
		return  session;
        }
        
	@Override
	public Contatto addContatto(Contatto c) {
		
		Session session = createTransaction();
		session.save(c);
		transaction.commit();
		//c = session.get(Contatto.class, c.getIdcontatto());
	   return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contatto> getAllContattos() {
		
		try {
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
		Session session = createTransaction();
		return session.createQuery("FROM Contatto C").list();
	}

	@Override
	public boolean deleteContatto(String id) {
		Session session = createTransaction();
		Contatto c  = getContatto(id);
		session.delete(c);
		transaction.commit();
		return true;
		
	}

	@Override
	public Contatto updateContatto(Contatto c) {
		Session session = createTransaction();
		session.update(c);
		transaction.commit();
		return c;
	}

	@Override
	public Contatto getContatto(String id) {
		Session session = createTransaction();
		return session.get(Contatto.class, id);
	}

	@Override
	public List getContattos(String idFunzione) {
		return getAllContattos();
	}
    
}
   