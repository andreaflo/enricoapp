package dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import dbobjects.Immagine;
import it.HibernateUtils;

public class ImmagineDAOImpl implements ImmagineDao {

	private SessionFactory sessionFactory = it.HibernateUtils.getSessionFactory();

	org.hibernate.Transaction transaction;

	public ImmagineDAOImpl() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Immagine addImmagine(Immagine immagine) {
		try {
			Session session = createTransaction();
			session.save(immagine);
			transaction.commit();
			// c = session.get(Contatto.class, c.getIdcontatto());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return immagine;
	}

	private Session createTransaction() {

		if (sessionFactory == null)
			sessionFactory = HibernateUtils.getSessionFactory();

		Session session = null;

		if (sessionFactory.isOpen()) {

			session = sessionFactory.getCurrentSession();

		}

		else {
			session = sessionFactory.openSession();
		}

		if (session.getTransaction().isActive()) {
			transaction = session.getTransaction();
		} else {
			transaction = session.beginTransaction();
		}
		return session;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Immagine> getAllImmagines() {
		// TODO Auto-generated method stub

		Immagine i = new Immagine();
		List l = new ArrayList<Immagine>();
		l.add(i);
		try {
			Session session = createTransaction();
			List result = session.createQuery("FROM dbobjects.Immagine i").list();
			if (result.size() > 0) {
				return result;
			} else
				return Collections.EMPTY_LIST;
			
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
		finally {
			transaction.commit();
		}
	}

	@Override
	public boolean deleteImmagine(String id) {
		Session session = createTransaction();
		Immagine immagine = getImmagine(id);
		session.delete(immagine);
		transaction.commit();
		return true;
	}

	@Override
	public Immagine updateImmagine(Immagine immagine) {
		Session session = createTransaction();
		session.update(immagine);
		transaction.commit();
		return immagine;
	}

	@Override
	public Immagine getImmagine(String id) {
		// TODO Auto-generated method stub
		Session session = createTransaction();
		return session.get(Immagine.class, id);
	}

	@Override
	public List getImmagini(String idFunzione) {
		// TODO Auto-generated method stub
		return getAllImmagines();
	}

}
