package dao;

import java.util.List;

import dbobjects.Contatto;


public interface ContattoDAO {
	 
	    public Contatto addContatto(Contatto Contatto);
	 
	    public List<Contatto> getAllContattos();
	 
	    public boolean deleteContatto(String id);
	 
	    public Contatto updateContatto(Contatto Contatto);
	 
	    public Contatto getContatto(String id);

		public List getContattos(String idFunzione);
}
