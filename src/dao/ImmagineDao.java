package dao;

import java.util.List;

import dbobjects.Immagine;


public interface ImmagineDao {
	 
	    public Immagine addImmagine(Immagine Immagine);
	 
	    public List<Immagine> getAllImmagines();
	 
	    public boolean deleteImmagine(String id);
	 
	    public Immagine updateImmagine(Immagine Immagine);

		public List getImmagini(String idFunzione);

		public Immagine getImmagine(String id);
}
