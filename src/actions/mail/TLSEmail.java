
package actions.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

public class TLSEmail {

	/**
	   Outgoing Mail (SMTP) Server
	   requires TLS or SSL: smtp.gmail.com (use authentication)
	   Use Authentication: Yes
	   Port for TLS/STARTTLS: 587
	 * @return 
	 */
	public static boolean sendEmail(String nome,String cognome,String email, String ragione,String telefono,String testo)  {
		final String fromEmail = "andreaflo@tin.it"; //requires valid gmail id
		final String password = "hellasverona"; // correct password for gmail id
		final String toEmail = "andreaflo@tin.it"; // can be any email id 

		System.out.println("TLSEmail Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", "mail.tin.it"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

		//create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);

		// Set Subject: header field
		String subject = "Sito Corona SRL : Richiesta Contatto da " + email;

		String body = "Sito Corona SRL" + "\n" +
				"Hai ricevuto una richiesta di contatto da parte di " + cognome + " " + nome + "</p>"  
				+ telefono +
				" ragione sociale : "+ ragione+ " \n "+	
				" Il cliente ha scritto : " + testo + "\n ";

		return EmailUtil.send(session, toEmail,subject, body);

	}


}
