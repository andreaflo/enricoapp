package actions.immagine;

import java.io.File;
import java.util.*;
import org.apache.log4j.*;
import dao.ImmagineDAOImpl;
import dao.ImmagineDao;
import dbobjects.Immagine;
import dbobjects.Stato;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;
import util.RegistratoreFiles;


public class ModificaImmagine extends Operation {
	private Logger logger;
	private ImmagineDao immagineDao = new ImmagineDAOImpl();

	public ModificaImmagine() throws BundledException {
		logger = Logger.getLogger(ModificaImmagine.class);
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
			BundledException 
	{
		Immagine mapped = null;
		File pathF = null;
		//Immagine immagineSource = immagineDao.getImmagine((String)ht.get("idAnagrafica"));
		
		Immagine dettaglioImmagine = (Immagine)sessione.getAttribute("dettaglioImmagine");
		
		String idtoupdate = (String)ht.get("idimmagine");
		String riferimento = (String)ht.get("nome");
		String descrizione =(String)ht.get("descrizione");
		String altro =(String)ht.get("altro");
		Object path = ht.get("file");
		String url =(String)ht.get("url");
		String nota =(String)ht.get("nota");
		String tipo =(String)ht.get("tipo");
		String stato = (String)ht.get("stato");
		try {
		pathF = (File) path;
		mapped = RegistratoreFiles.numeraFiles(pathF,new Immagine());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		Immagine immagineToStore = new Immagine(idtoupdate,descrizione,riferimento,pathF.toString(),altro,url,nota,tipo,Stato.Y);
		
		Immagine immagineTarget = immagineDao.updateImmagine(immagineToStore);
		super.setOKAnswer(immagineTarget, targetpage);
		return super.risposta;
	}

	

	protected String getIdFunzione() {
		return "modificaImmagine";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="dettaglioImmagine";
		return chiaveSessione;
	}

	/*
	*protected boolean isAuthorized(Utente arg0) {
	*	return true;
	*}
	*/
}
