package actions.immagine;

import it.realt.general.BundledException;
import it.realt.webapps.Util;
import it.realt.webapps.file.dbbeans.File;
import it.realt.webapps.file.internet.ops.OpVolume;
import it.realt.webapps.utente.Operation;
import it.realt.webapps.utente.dbbeans.Utente;
import it.realt.webapps.utente.mvc.action.Answer;
import util.RegistratoreFiles;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import dao.ImmagineDAOImpl;
import dao.ImmagineDao;
import dbobjects.Immagine;
import dbobjects.Stato;

public class InsertImmagine extends Operation {
	private static Logger logger;
	private ImmagineDao immagineDao = new ImmagineDAOImpl();

	public InsertImmagine() throws BundledException {
		logger = Logger.getLogger(InsertImmagine.class);
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws BundledException {

		String mapped = null;

		String riferimento = (String) ht.get("nome");
		String descrizione = (String) ht.get("descrizione");
		java.io.File path = (java.io.File) ht.get("file");
		String altro = (String) ht.get("altro");
		String nota = (String) ht.get("nota");
		String url = (String) ht.get("url");
		Date dataIns = (Date) ht.get("dataInserimento");

		//SALVO VALORI NELLA TABELLA ANAGRAFICA
		Immagine immagine = new Immagine();
		String id = (Util.getId()).toString();
		immagine.setIdimmagine(id);
		immagine.setNome(riferimento);
		immagine.setDescrizione(descrizione);
		immagine.setNota(nota);
		immagine.setDataInserimento(dataIns);

		immagine.setTipo("C");
		immagine.setStato(Stato.Y.ordinal());

		try {
			immagine = RegistratoreFiles.numeraFiles(path,immagine);
			immagine = RegistratoreFiles.renameFile(immagine);
			//immagine.setUrl(path.toString());
		}
		catch(Exception e) {
			e.printStackTrace();
		}


		immagineDao.addImmagine(immagine);
		/*java.io.File upFile_comodato = (java.io.File)ht.get("file_comodato");

		if (upFile_comodato != null) {
			// Salvataggio file su disco
			OpVolume opVolume = new OpVolume(user);
			File savedFile = opVolume.save(upFile_comodato);	
		}*/

		logger.debug(""+immagine+"|-|"+ht.size()+"|");       

		super.setOKAnswer(immagine, targetpage);
		return super.risposta;
	}

	protected String getIdFunzione() {
		return "insertImmagine";
	}
}
