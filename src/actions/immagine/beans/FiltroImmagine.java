package actions.immagine.beans;
	/*****************************************************************************************************************************
	 * Classe che rappresenta un filtro per la ricerca nella tabella nota_cliente_x_anagrafica.
	 * @author Generata da it.realt.dbutil.dev.AbstractFileWriter 3.0
	 * @version 1.0 06/11/12
	 ****************************************************************************************************************************/
	public class FiltroImmagine implements java.io.Serializable {
	/** I valori da ricercare per la colonna id_nota_cliente @serial */
	    java.lang.String[] arrayIdImmagine;
	    /** Mi dice se devo ricercare per range (true) o per punti (false) descrizione @serial */
	    boolean rangeIdImmagine;
	    java.lang.String[] arrayTesto;
	    /** Mi dice se devo ricercare per range (true) o per punti (false) descrizione @serial */
	    boolean rangeTesto;
	/** I valori da ricercare per la colonna descrizione @serial */
	    java.lang.String[] arrayDescrizione;
	/** Mi dice se devo ricercare per range (true) o per punti (false) descrizione @serial */
	    boolean rangeDescrizione;
	/** I valori da ricercare per la colonna data_visita @serial */
	    java.lang.String[] arrayDataVisita;
	/** Mi dice se devo ricercare per range (true) o per punti (false) data_visita @serial */
	    boolean rangeDataVisita;
	/** I valori da ricercare per la colonna stato_record @serial */
	    java.lang.String[] arrayStatoRecord;
	/** Mi dice se devo ricercare per range (true) o per punti (false) stato_record @serial */
	    boolean rangeStatoRecord;
	/** I valori da ricercare per la colonna user_ins @serial */
	    java.lang.String[] arrayUserIns;
	/** Mi dice se devo ricercare per range (true) o per punti (false) user_ins @serial */
	    boolean rangeUserIns;
	/** I valori da ricercare per la colonna user_var @serial */
	    java.lang.String[] arrayUserVar;
	/** Mi dice se devo ricercare per range (true) o per punti (false) user_var @serial */
	    boolean rangeUserVar;
	/** I valori da ricercare per la colonna data_ins @serial */
	    java.lang.String[] arrayDataIns;
	/** Mi dice se devo ricercare per range (true) o per punti (false) data_ins @serial */
	    boolean rangeDataIns;
	/** I valori da ricercare per la colonna data_var @serial */
	    java.lang.String[] arrayDataVar;
	/** Mi dice se devo ricercare per range (true) o per punti (false) data_var @serial */
	    boolean rangeDataVar;
	/** Costruttore pubblico senza argomenti */
	    public FiltroImmagine() {
			// TODO Auto-generated constructor stub
	        super();
	    }
	/** Imposta il valore per la colonna id_anagrafica */
	    public void setIdImmagine(java.lang.String newIdImmagine) {
	        if (arrayIdImmagine == null || arrayIdImmagine.length != 1)
	            arrayIdImmagine = new java.lang.String[1];
	        arrayIdImmagine[0] = newIdImmagine;
	    }
	/** Imposta il valore di partenza per la colonna id_anagrafica */
	    public void setFromIdImmagine(java.lang.String newFromIdImmagine) {
	        if (arrayIdImmagine == null || (arrayIdImmagine.length != 1 && !(arrayIdImmagine.length == 2 && rangeIdImmagine)))
	            arrayIdImmagine = new java.lang.String[2];
	        else if (arrayIdImmagine.length == 1) {
	            java.lang.String item = arrayIdImmagine[0];
	            arrayIdImmagine = new java.lang.String[2];
	            arrayIdImmagine[1] = item;
	        }
	        arrayIdImmagine[0] = newFromIdImmagine;
	        rangeIdImmagine = true;
	    }
	/** Imposta il valore di arrivo per la colonna id_anagrafica */
	    public void setToIdImmagine(java.lang.String newToIdImmagine) {
	        if (arrayIdImmagine == null || (arrayIdImmagine.length != 1 && !(arrayIdImmagine.length == 2 && rangeIdImmagine)))
	            arrayIdImmagine = new java.lang.String[2];
	        else if (arrayIdImmagine.length == 1) {
	            java.lang.String item = arrayIdImmagine[0];
	            arrayIdImmagine = new java.lang.String[2];
	            arrayIdImmagine[0] = item;
	        }
	        arrayIdImmagine[1] = newToIdImmagine;
	        rangeIdImmagine = true;
	    }
	/**
	 * Restituisce i valori per la colonna testo
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getTestoValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayTesto != null && arrayTesto.length == 2 && rangeTesto)
	            throw new IllegalStateException("Range testo");
	        return arrayTesto;
	    }
	/**
	 * Restituisce il valore per la colonna testo
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getTesto() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayTesto == null || arrayTesto.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeTesto && arrayTesto.length == 2
	              && (arrayTesto[0] == null ? arrayTesto[1] == null : arrayTesto[0].equals(arrayTesto[1]))) {
	            return arrayTesto[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayTesto.length != 1)
	            throw new IllegalStateException("No single testo");
	        //restituisco l'unico elemento
	        return arrayTesto[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna testo
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromTesto() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayTesto == null || arrayTesto.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayTesto.length == 1)
	            return arrayTesto[0];
	        //se non ricerco per range, errore!
	        if (!rangeTesto)
	            throw new IllegalStateException("No range testo");
	        //restituisco il primo elemento dell'array
	        return arrayTesto[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna testo
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToTesto() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayTesto == null || arrayTesto.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayTesto.length == 1)
	            return arrayTesto[0];
	        //se non ricerco per range, errore!
	        if (!rangeTesto)
	            throw new IllegalStateException("No range testo");
	        //restituisco il secondo elemento dell'array
	        return arrayTesto[1];
	    }
	/** Imposta i valori per la colonna testo */
	    public void setTestoValues(java.lang.String[] newTestoValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newTestoValues != null && newTestoValues.length == 2
	              && (newTestoValues[0] == null ? newTestoValues[1] == null : newTestoValues[0].equals(newTestoValues[1]))) {
	            this.arrayTesto = new java.lang.String[1];
	            this.arrayTesto[0] = newTestoValues[0];
	        } else {
	            this.arrayTesto = newTestoValues;
	            this.rangeTesto = false;
	        }
	    }
	/** Imposta il valore per la colonna testo */
	    public void setTesto(java.lang.String newTesto) {
	        if (arrayTesto == null || arrayTesto.length != 1)
	            arrayTesto = new java.lang.String[1];
	        arrayTesto[0] = newTesto;
	    }
	/** Imposta il valore di partenza per la colonna testo */
	    public void setFromTesto(java.lang.String newFromTesto) {
	        if (arrayTesto == null || (arrayTesto.length != 1 && !(arrayTesto.length == 2 && rangeTesto)))
	            arrayTesto = new java.lang.String[2];
	        else if (arrayTesto.length == 1) {
	            java.lang.String item = arrayTesto[0];
	            arrayTesto = new java.lang.String[2];
	            arrayTesto[1] = item;
	        }
	        arrayTesto[0] = newFromTesto;
	        rangeTesto = true;
	    }
	/** Imposta il valore di arrivo per la colonna testo */
	    public void setToTesto(java.lang.String newToTesto) {
	        if (arrayTesto == null || (arrayTesto.length != 1 && !(arrayTesto.length == 2 && rangeTesto)))
	            arrayTesto = new java.lang.String[2];
	        else if (arrayTesto.length == 1) {
	            java.lang.String item = arrayTesto[0];
	            arrayTesto = new java.lang.String[2];
	            arrayTesto[0] = item;
	        }
	        arrayTesto[1] = newToTesto;
	        rangeTesto = true;
	    }
	/**
	 * Restituisce i valori per la colonna descrizione
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getDescrizioneValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayDescrizione != null && arrayDescrizione.length == 2 && rangeDescrizione)
	            throw new IllegalStateException("Range descrizione");
	        return arrayDescrizione;
	    }
	/**
	 * Restituisce il valore per la colonna descrizione
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getDescrizione() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDescrizione == null || arrayDescrizione.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeDescrizione && arrayDescrizione.length == 2
	              && (arrayDescrizione[0] == null ? arrayDescrizione[1] == null : arrayDescrizione[0].equals(arrayDescrizione[1]))) {
	            return arrayDescrizione[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayDescrizione.length != 1)
	            throw new IllegalStateException("No single descrizione");
	        //restituisco l'unico elemento
	        return arrayDescrizione[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna descrizione
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromDescrizione() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDescrizione == null || arrayDescrizione.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDescrizione.length == 1)
	            return arrayDescrizione[0];
	        //se non ricerco per range, errore!
	        if (!rangeDescrizione)
	            throw new IllegalStateException("No range descrizione");
	        //restituisco il primo elemento dell'array
	        return arrayDescrizione[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna descrizione
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToDescrizione() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDescrizione == null || arrayDescrizione.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDescrizione.length == 1)
	            return arrayDescrizione[0];
	        //se non ricerco per range, errore!
	        if (!rangeDescrizione)
	            throw new IllegalStateException("No range descrizione");
	        //restituisco il secondo elemento dell'array
	        return arrayDescrizione[1];
	    }
	/** Imposta i valori per la colonna descrizione */
	    public void setDescrizioneValues(java.lang.String[] newDescrizioneValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newDescrizioneValues != null && newDescrizioneValues.length == 2
	              && (newDescrizioneValues[0] == null ? newDescrizioneValues[1] == null : newDescrizioneValues[0].equals(newDescrizioneValues[1]))) {
	            this.arrayDescrizione = new java.lang.String[1];
	            this.arrayDescrizione[0] = newDescrizioneValues[0];
	        } else {
	            this.arrayDescrizione = newDescrizioneValues;
	            this.rangeDescrizione = false;
	        }
	    }
	/** Imposta il valore per la colonna descrizione */
	    public void setDescrizione(java.lang.String newDescrizione) {
	        if (arrayDescrizione == null || arrayDescrizione.length != 1)
	            arrayDescrizione = new java.lang.String[1];
	        arrayDescrizione[0] = newDescrizione;
	    }
	/** Imposta il valore di partenza per la colonna descrizione */
	    public void setFromDescrizione(java.lang.String newFromDescrizione) {
	        if (arrayDescrizione == null || (arrayDescrizione.length != 1 && !(arrayDescrizione.length == 2 && rangeDescrizione)))
	            arrayDescrizione = new java.lang.String[2];
	        else if (arrayDescrizione.length == 1) {
	            java.lang.String item = arrayDescrizione[0];
	            arrayDescrizione = new java.lang.String[2];
	            arrayDescrizione[1] = item;
	        }
	        arrayDescrizione[0] = newFromDescrizione;
	        rangeDescrizione = true;
	    }
	/** Imposta il valore di arrivo per la colonna descrizione */
	    public void setToDescrizione(java.lang.String newToDescrizione) {
	        if (arrayDescrizione == null || (arrayDescrizione.length != 1 && !(arrayDescrizione.length == 2 && rangeDescrizione)))
	            arrayDescrizione = new java.lang.String[2];
	        else if (arrayDescrizione.length == 1) {
	            java.lang.String item = arrayDescrizione[0];
	            arrayDescrizione = new java.lang.String[2];
	            arrayDescrizione[0] = item;
	        }
	        arrayDescrizione[1] = newToDescrizione;
	        rangeDescrizione = true;
	    }
	/**
	 * Restituisce i valori per la colonna data_visita
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getDataVisitaValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayDataVisita != null && arrayDataVisita.length == 2 && rangeDataVisita)
	            throw new IllegalStateException("Range dataVisita");
	        return arrayDataVisita;
	    }
	/**
	 * Restituisce il valore per la colonna data_visita
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getDataVisita() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVisita == null || arrayDataVisita.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeDataVisita && arrayDataVisita.length == 2
	              && (arrayDataVisita[0] == null ? arrayDataVisita[1] == null : arrayDataVisita[0].equals(arrayDataVisita[1]))) {
	            return arrayDataVisita[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayDataVisita.length != 1)
	            throw new IllegalStateException("No single dataVisita");
	        //restituisco l'unico elemento
	        return arrayDataVisita[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna data_visita
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromDataVisita() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVisita == null || arrayDataVisita.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataVisita.length == 1)
	            return arrayDataVisita[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataVisita)
	            throw new IllegalStateException("No range dataVisita");
	        //restituisco il primo elemento dell'array
	        return arrayDataVisita[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna data_visita
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToDataVisita() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVisita == null || arrayDataVisita.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataVisita.length == 1)
	            return arrayDataVisita[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataVisita)
	            throw new IllegalStateException("No range dataVisita");
	        //restituisco il secondo elemento dell'array
	        return arrayDataVisita[1];
	    }
	/** Imposta i valori per la colonna data_visita */
	    public void setDataVisitaValues(java.lang.String[] newDataVisitaValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newDataVisitaValues != null && newDataVisitaValues.length == 2
	              && (newDataVisitaValues[0] == null ? newDataVisitaValues[1] == null : newDataVisitaValues[0].equals(newDataVisitaValues[1]))) {
	            this.arrayDataVisita = new java.lang.String[1];
	            this.arrayDataVisita[0] = newDataVisitaValues[0];
	        } else {
	            this.arrayDataVisita = newDataVisitaValues;
	            this.rangeDataVisita = false;
	        }
	    }
	/** Imposta il valore per la colonna data_visita */
	    public void setDataVisita(java.lang.String newDataVisita) {
	        if (arrayDataVisita == null || arrayDataVisita.length != 1)
	            arrayDataVisita = new java.lang.String[1];
	        arrayDataVisita[0] = newDataVisita;
	    }
	/** Imposta il valore di partenza per la colonna data_visita */
	    public void setFromDataVisita(java.lang.String newFromDataVisita) {
	        if (arrayDataVisita == null || (arrayDataVisita.length != 1 && !(arrayDataVisita.length == 2 && rangeDataVisita)))
	            arrayDataVisita = new java.lang.String[2];
	        else if (arrayDataVisita.length == 1) {
	            java.lang.String item = arrayDataVisita[0];
	            arrayDataVisita = new java.lang.String[2];
	            arrayDataVisita[1] = item;
	        }
	        arrayDataVisita[0] = newFromDataVisita;
	        rangeDataVisita = true;
	    }
	/** Imposta il valore di arrivo per la colonna data_visita */
	    public void setToDataVisita(java.lang.String newToDataVisita) {
	        if (arrayDataVisita == null || (arrayDataVisita.length != 1 && !(arrayDataVisita.length == 2 && rangeDataVisita)))
	            arrayDataVisita = new java.lang.String[2];
	        else if (arrayDataVisita.length == 1) {
	            java.lang.String item = arrayDataVisita[0];
	            arrayDataVisita = new java.lang.String[2];
	            arrayDataVisita[0] = item;
	        }
	        arrayDataVisita[1] = newToDataVisita;
	        rangeDataVisita = true;
	    }
	/**
	 * Restituisce i valori per la colonna stato_record
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getStatoRecordValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayStatoRecord != null && arrayStatoRecord.length == 2 && rangeStatoRecord)
	            throw new IllegalStateException("Range statoRecord");
	        return arrayStatoRecord;
	    }
	/**
	 * Restituisce il valore per la colonna stato_record
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getStatoRecord() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayStatoRecord == null || arrayStatoRecord.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeStatoRecord && arrayStatoRecord.length == 2
	              && (arrayStatoRecord[0] == null ? arrayStatoRecord[1] == null : arrayStatoRecord[0].equals(arrayStatoRecord[1]))) {
	            return arrayStatoRecord[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayStatoRecord.length != 1)
	            throw new IllegalStateException("No single statoRecord");
	        //restituisco l'unico elemento
	        return arrayStatoRecord[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna stato_record
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromStatoRecord() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayStatoRecord == null || arrayStatoRecord.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayStatoRecord.length == 1)
	            return arrayStatoRecord[0];
	        //se non ricerco per range, errore!
	        if (!rangeStatoRecord)
	            throw new IllegalStateException("No range statoRecord");
	        //restituisco il primo elemento dell'array
	        return arrayStatoRecord[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna stato_record
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToStatoRecord() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayStatoRecord == null || arrayStatoRecord.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayStatoRecord.length == 1)
	            return arrayStatoRecord[0];
	        //se non ricerco per range, errore!
	        if (!rangeStatoRecord)
	            throw new IllegalStateException("No range statoRecord");
	        //restituisco il secondo elemento dell'array
	        return arrayStatoRecord[1];
	    }
	/** Imposta i valori per la colonna stato_record */
	    public void setStatoRecordValues(java.lang.String[] newStatoRecordValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newStatoRecordValues != null && newStatoRecordValues.length == 2
	              && (newStatoRecordValues[0] == null ? newStatoRecordValues[1] == null : newStatoRecordValues[0].equals(newStatoRecordValues[1]))) {
	            this.arrayStatoRecord = new java.lang.String[1];
	            this.arrayStatoRecord[0] = newStatoRecordValues[0];
	        } else {
	            this.arrayStatoRecord = newStatoRecordValues;
	            this.rangeStatoRecord = false;
	        }
	    }
	/** Imposta il valore per la colonna stato_record */
	    public void setStatoRecord(java.lang.String newStatoRecord) {
	        if (arrayStatoRecord == null || arrayStatoRecord.length != 1)
	            arrayStatoRecord = new java.lang.String[1];
	        arrayStatoRecord[0] = newStatoRecord;
	    }
	/** Imposta il valore di partenza per la colonna stato_record */
	    public void setFromStatoRecord(java.lang.String newFromStatoRecord) {
	        if (arrayStatoRecord == null || (arrayStatoRecord.length != 1 && !(arrayStatoRecord.length == 2 && rangeStatoRecord)))
	            arrayStatoRecord = new java.lang.String[2];
	        else if (arrayStatoRecord.length == 1) {
	            java.lang.String item = arrayStatoRecord[0];
	            arrayStatoRecord = new java.lang.String[2];
	            arrayStatoRecord[1] = item;
	        }
	        arrayStatoRecord[0] = newFromStatoRecord;
	        rangeStatoRecord = true;
	    }
	/** Imposta il valore di arrivo per la colonna stato_record */
	    public void setToStatoRecord(java.lang.String newToStatoRecord) {
	        if (arrayStatoRecord == null || (arrayStatoRecord.length != 1 && !(arrayStatoRecord.length == 2 && rangeStatoRecord)))
	            arrayStatoRecord = new java.lang.String[2];
	        else if (arrayStatoRecord.length == 1) {
	            java.lang.String item = arrayStatoRecord[0];
	            arrayStatoRecord = new java.lang.String[2];
	            arrayStatoRecord[0] = item;
	        }
	        arrayStatoRecord[1] = newToStatoRecord;
	        rangeStatoRecord = true;
	    }
	/**
	 * Restituisce i valori per la colonna user_ins
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getUserInsValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayUserIns != null && arrayUserIns.length == 2 && rangeUserIns)
	            throw new IllegalStateException("Range userIns");
	        return arrayUserIns;
	    }
	/**
	 * Restituisce il valore per la colonna user_ins
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getUserIns() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserIns == null || arrayUserIns.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeUserIns && arrayUserIns.length == 2
	              && (arrayUserIns[0] == null ? arrayUserIns[1] == null : arrayUserIns[0].equals(arrayUserIns[1]))) {
	            return arrayUserIns[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayUserIns.length != 1)
	            throw new IllegalStateException("No single userIns");
	        //restituisco l'unico elemento
	        return arrayUserIns[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna user_ins
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromUserIns() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserIns == null || arrayUserIns.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayUserIns.length == 1)
	            return arrayUserIns[0];
	        //se non ricerco per range, errore!
	        if (!rangeUserIns)
	            throw new IllegalStateException("No range userIns");
	        //restituisco il primo elemento dell'array
	        return arrayUserIns[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna user_ins
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToUserIns() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserIns == null || arrayUserIns.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayUserIns.length == 1)
	            return arrayUserIns[0];
	        //se non ricerco per range, errore!
	        if (!rangeUserIns)
	            throw new IllegalStateException("No range userIns");
	        //restituisco il secondo elemento dell'array
	        return arrayUserIns[1];
	    }
	/** Imposta i valori per la colonna user_ins */
	    public void setUserInsValues(java.lang.String[] newUserInsValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newUserInsValues != null && newUserInsValues.length == 2
	              && (newUserInsValues[0] == null ? newUserInsValues[1] == null : newUserInsValues[0].equals(newUserInsValues[1]))) {
	            this.arrayUserIns = new java.lang.String[1];
	            this.arrayUserIns[0] = newUserInsValues[0];
	        } else {
	            this.arrayUserIns = newUserInsValues;
	            this.rangeUserIns = false;
	        }
	    }
	/** Imposta il valore per la colonna user_ins */
	    public void setUserIns(java.lang.String newUserIns) {
	        if (arrayUserIns == null || arrayUserIns.length != 1)
	            arrayUserIns = new java.lang.String[1];
	        arrayUserIns[0] = newUserIns;
	    }
	/** Imposta il valore di partenza per la colonna user_ins */
	    public void setFromUserIns(java.lang.String newFromUserIns) {
	        if (arrayUserIns == null || (arrayUserIns.length != 1 && !(arrayUserIns.length == 2 && rangeUserIns)))
	            arrayUserIns = new java.lang.String[2];
	        else if (arrayUserIns.length == 1) {
	            java.lang.String item = arrayUserIns[0];
	            arrayUserIns = new java.lang.String[2];
	            arrayUserIns[1] = item;
	        }
	        arrayUserIns[0] = newFromUserIns;
	        rangeUserIns = true;
	    }
	/** Imposta il valore di arrivo per la colonna user_ins */
	    public void setToUserIns(java.lang.String newToUserIns) {
	        if (arrayUserIns == null || (arrayUserIns.length != 1 && !(arrayUserIns.length == 2 && rangeUserIns)))
	            arrayUserIns = new java.lang.String[2];
	        else if (arrayUserIns.length == 1) {
	            java.lang.String item = arrayUserIns[0];
	            arrayUserIns = new java.lang.String[2];
	            arrayUserIns[0] = item;
	        }
	        arrayUserIns[1] = newToUserIns;
	        rangeUserIns = true;
	    }
	/**
	 * Restituisce i valori per la colonna user_var
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getUserVarValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayUserVar != null && arrayUserVar.length == 2 && rangeUserVar)
	            throw new IllegalStateException("Range userVar");
	        return arrayUserVar;
	    }
	/**
	 * Restituisce il valore per la colonna user_var
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getUserVar() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserVar == null || arrayUserVar.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeUserVar && arrayUserVar.length == 2
	              && (arrayUserVar[0] == null ? arrayUserVar[1] == null : arrayUserVar[0].equals(arrayUserVar[1]))) {
	            return arrayUserVar[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayUserVar.length != 1)
	            throw new IllegalStateException("No single userVar");
	        //restituisco l'unico elemento
	        return arrayUserVar[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna user_var
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromUserVar() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserVar == null || arrayUserVar.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayUserVar.length == 1)
	            return arrayUserVar[0];
	        //se non ricerco per range, errore!
	        if (!rangeUserVar)
	            throw new IllegalStateException("No range userVar");
	        //restituisco il primo elemento dell'array
	        return arrayUserVar[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna user_var
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToUserVar() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayUserVar == null || arrayUserVar.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayUserVar.length == 1)
	            return arrayUserVar[0];
	        //se non ricerco per range, errore!
	        if (!rangeUserVar)
	            throw new IllegalStateException("No range userVar");
	        //restituisco il secondo elemento dell'array
	        return arrayUserVar[1];
	    }
	/** Imposta i valori per la colonna user_var */
	    public void setUserVarValues(java.lang.String[] newUserVarValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newUserVarValues != null && newUserVarValues.length == 2
	              && (newUserVarValues[0] == null ? newUserVarValues[1] == null : newUserVarValues[0].equals(newUserVarValues[1]))) {
	            this.arrayUserVar = new java.lang.String[1];
	            this.arrayUserVar[0] = newUserVarValues[0];
	        } else {
	            this.arrayUserVar = newUserVarValues;
	            this.rangeUserVar = false;
	        }
	    }
	/** Imposta il valore per la colonna user_var */
	    public void setUserVar(java.lang.String newUserVar) {
	        if (arrayUserVar == null || arrayUserVar.length != 1)
	            arrayUserVar = new java.lang.String[1];
	        arrayUserVar[0] = newUserVar;
	    }
	/** Imposta il valore di partenza per la colonna user_var */
	    public void setFromUserVar(java.lang.String newFromUserVar) {
	        if (arrayUserVar == null || (arrayUserVar.length != 1 && !(arrayUserVar.length == 2 && rangeUserVar)))
	            arrayUserVar = new java.lang.String[2];
	        else if (arrayUserVar.length == 1) {
	            java.lang.String item = arrayUserVar[0];
	            arrayUserVar = new java.lang.String[2];
	            arrayUserVar[1] = item;
	        }
	        arrayUserVar[0] = newFromUserVar;
	        rangeUserVar = true;
	    }
	/** Imposta il valore di arrivo per la colonna user_var */
	    public void setToUserVar(java.lang.String newToUserVar) {
	        if (arrayUserVar == null || (arrayUserVar.length != 1 && !(arrayUserVar.length == 2 && rangeUserVar)))
	            arrayUserVar = new java.lang.String[2];
	        else if (arrayUserVar.length == 1) {
	            java.lang.String item = arrayUserVar[0];
	            arrayUserVar = new java.lang.String[2];
	            arrayUserVar[0] = item;
	        }
	        arrayUserVar[1] = newToUserVar;
	        rangeUserVar = true;
	    }
	/**
	 * Restituisce i valori per la colonna data_ins
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getDataInsValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayDataIns != null && arrayDataIns.length == 2 && rangeDataIns)
	            throw new IllegalStateException("Range dataIns");
	        return arrayDataIns;
	    }
	/**
	 * Restituisce il valore per la colonna data_ins
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getDataIns() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataIns == null || arrayDataIns.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeDataIns && arrayDataIns.length == 2
	              && (arrayDataIns[0] == null ? arrayDataIns[1] == null : arrayDataIns[0].equals(arrayDataIns[1]))) {
	            return arrayDataIns[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayDataIns.length != 1)
	            throw new IllegalStateException("No single dataIns");
	        //restituisco l'unico elemento
	        return arrayDataIns[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna data_ins
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromDataIns() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataIns == null || arrayDataIns.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataIns.length == 1)
	            return arrayDataIns[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataIns)
	            throw new IllegalStateException("No range dataIns");
	        //restituisco il primo elemento dell'array
	        return arrayDataIns[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna data_ins
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToDataIns() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataIns == null || arrayDataIns.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataIns.length == 1)
	            return arrayDataIns[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataIns)
	            throw new IllegalStateException("No range dataIns");
	        //restituisco il secondo elemento dell'array
	        return arrayDataIns[1];
	    }
	/** Imposta i valori per la colonna data_ins */
	    public void setDataInsValues(java.lang.String[] newDataInsValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newDataInsValues != null && newDataInsValues.length == 2
	              && (newDataInsValues[0] == null ? newDataInsValues[1] == null : newDataInsValues[0].equals(newDataInsValues[1]))) {
	            this.arrayDataIns = new java.lang.String[1];
	            this.arrayDataIns[0] = newDataInsValues[0];
	        } else {
	            this.arrayDataIns = newDataInsValues;
	            this.rangeDataIns = false;
	        }
	    }
	/** Imposta il valore per la colonna data_ins */
	    public void setDataIns(java.lang.String newDataIns) {
	        if (arrayDataIns == null || arrayDataIns.length != 1)
	            arrayDataIns = new java.lang.String[1];
	        arrayDataIns[0] = newDataIns;
	    }
	/** Imposta il valore di partenza per la colonna data_ins */
	    public void setFromDataIns(java.lang.String newFromDataIns) {
	        if (arrayDataIns == null || (arrayDataIns.length != 1 && !(arrayDataIns.length == 2 && rangeDataIns)))
	            arrayDataIns = new java.lang.String[2];
	        else if (arrayDataIns.length == 1) {
	            java.lang.String item = arrayDataIns[0];
	            arrayDataIns = new java.lang.String[2];
	            arrayDataIns[1] = item;
	        }
	        arrayDataIns[0] = newFromDataIns;
	        rangeDataIns = true;
	    }
	/** Imposta il valore di arrivo per la colonna data_ins */
	    public void setToDataIns(java.lang.String newToDataIns) {
	        if (arrayDataIns == null || (arrayDataIns.length != 1 && !(arrayDataIns.length == 2 && rangeDataIns)))
	            arrayDataIns = new java.lang.String[2];
	        else if (arrayDataIns.length == 1) {
	            java.lang.String item = arrayDataIns[0];
	            arrayDataIns = new java.lang.String[2];
	            arrayDataIns[0] = item;
	        }
	        arrayDataIns[1] = newToDataIns;
	        rangeDataIns = true;
	    }
	/**
	 * Restituisce i valori per la colonna data_var
	 * @throws IllegalStateException se questo filtro ricerca per range su questa colonna.
	 */
	    public java.lang.String[] getDataVarValues() {
	        //l'unico caso di eccezione è quando l'array ha 2 elementi e ricerco per range
	        if (arrayDataVar != null && arrayDataVar.length == 2 && rangeDataVar)
	            throw new IllegalStateException("Range dataVar");
	        return arrayDataVar;
	    }
	/**
	 * Restituisce il valore per la colonna data_var
	 * @throws IllegalStateException se questo filtro non ricerca un solo valore su questa colonna.
	 */
	    public java.lang.String getDataVar() {
	        //restituisci il valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVar == null || arrayDataVar.length == 0)
	            return null;
	        //se ricerco per range e ho 2 elementi uguali, restituisco
	        if (rangeDataVar && arrayDataVar.length == 2
	              && (arrayDataVar[0] == null ? arrayDataVar[1] == null : arrayDataVar[0].equals(arrayDataVar[1]))) {
	            return arrayDataVar[0];
	        }
	        //se non ho un solo elemento, eccezione!
	        if (arrayDataVar.length != 1)
	            throw new IllegalStateException("No single dataVar");
	        //restituisco l'unico elemento
	        return arrayDataVar[0];
	    }
	/**
	 * Restituisce il valore di partenza per la colonna data_var
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getFromDataVar() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVar == null || arrayDataVar.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataVar.length == 1)
	            return arrayDataVar[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataVar)
	            throw new IllegalStateException("No range dataVar");
	        //restituisco il primo elemento dell'array
	        return arrayDataVar[0];
	    }
	/**
	 * Restituisce il valore di arrivo per la colonna data_var
	 * @throws IllegalStateException se questo filtro non ricerca per range su questa colonna.
	 */
	    public java.lang.String getToDataVar() {
	        //valore di default se non ho l'array ovvero se è vuoto
	        if (arrayDataVar == null || arrayDataVar.length == 0)
	            return null;
	        //unico valore se l'array ha un solo elemento
	        if (arrayDataVar.length == 1)
	            return arrayDataVar[0];
	        //se non ricerco per range, errore!
	        if (!rangeDataVar)
	            throw new IllegalStateException("No range dataVar");
	        //restituisco il secondo elemento dell'array
	        return arrayDataVar[1];
	    }
	/** Imposta i valori per la colonna data_var */
	    public void setDataVarValues(java.lang.String[] newDataVarValues) {
	        //caso particolare: il parametro ha 2 elementi uguali imposto un solo valore
	        if (newDataVarValues != null && newDataVarValues.length == 2
	              && (newDataVarValues[0] == null ? newDataVarValues[1] == null : newDataVarValues[0].equals(newDataVarValues[1]))) {
	            this.arrayDataVar = new java.lang.String[1];
	            this.arrayDataVar[0] = newDataVarValues[0];
	        } else {
	            this.arrayDataVar = newDataVarValues;
	            this.rangeDataVar = false;
	        }
	    }
	/** Imposta il valore per la colonna data_var */
	    public void setDataVar(java.lang.String newDataVar) {
	        if (arrayDataVar == null || arrayDataVar.length != 1)
	            arrayDataVar = new java.lang.String[1];
	        arrayDataVar[0] = newDataVar;
	    }
	/** Imposta il valore di partenza per la colonna data_var */
	    public void setFromDataVar(java.lang.String newFromDataVar) {
	        if (arrayDataVar == null || (arrayDataVar.length != 1 && !(arrayDataVar.length == 2 && rangeDataVar)))
	            arrayDataVar = new java.lang.String[2];
	        else if (arrayDataVar.length == 1) {
	            java.lang.String item = arrayDataVar[0];
	            arrayDataVar = new java.lang.String[2];
	            arrayDataVar[1] = item;
	        }
	        arrayDataVar[0] = newFromDataVar;
	        rangeDataVar = true;
	    }
	/** Imposta il valore di arrivo per la colonna data_var */
	    public void setToDataVar(java.lang.String newToDataVar) {
	        if (arrayDataVar == null || (arrayDataVar.length != 1 && !(arrayDataVar.length == 2 && rangeDataVar)))
	            arrayDataVar = new java.lang.String[2];
	        else if (arrayDataVar.length == 1) {
	            java.lang.String item = arrayDataVar[0];
	            arrayDataVar = new java.lang.String[2];
	            arrayDataVar[0] = item;
	        }
	        arrayDataVar[1] = newToDataVar;
	        rangeDataVar = true;
	    }

}
