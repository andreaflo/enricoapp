package actions.immagine;

import java.util.*;
import org.apache.log4j.*;
import dao.ImmagineDAOImpl;
import dao.ImmagineDao;
import dbobjects.Immagine;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;

public class DettaglioImmagine extends Operation {
	private static Logger logger;
	private ImmagineDao dao; 

	public DettaglioImmagine() throws BundledException {
		logger = Logger.getLogger(DettaglioImmagine.class);
           dao = new ImmagineDAOImpl();
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
	BundledException 
	{
		if(valido((String)ht.get("idimmagine")))
		{
			String id = (String)ht.get("idimmagine");
			Immagine immagine = dao.getImmagine(id);
			sessione.setAttribute("dettaglioImmagine", immagine);
			super.setOKAnswer(immagine, targetpage);
		}
		return super.risposta;
	}

	private boolean valido(String in)
	{
		boolean result=false;
		if(in!=null)
		{
			if(in.length()>0){result=true;}
		}
		return(result);
	}

	protected String getIdFunzione() {
		return "leggiImmagine";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="dettaglioImmagine";
		return chiaveSessione;
	}
}
