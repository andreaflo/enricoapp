package actions.immagine;

import it.realt.application.Configuration;
import it.realt.general.BundledException;
import it.realt.webapps.utente.Operation;
import it.realt.webapps.utente.dbbeans.Utente;
import it.realt.webapps.utente.mvc.action.Answer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Map;
import org.apache.log4j.Logger;

public class ImportaImmaginiAction extends Operation {

	private static final Logger logger = Logger.getLogger(ImportaImmaginiAction.class);
	private static final String CONFIG = "moduloFile.properties";
	private static String CAROUSEL;
	private static String PATH;
	private static String EXTENSION;

	public ImportaImmaginiAction() throws BundledException {
		super();
		try{
			if (CAROUSEL == null)
				CAROUSEL = loadConfiguration("carousel.path");
			if (PATH == null);
				// nessuno loggato -> nessuna sessione
				//PATH = sessione.getServletContext().getRealPath("/")+"/"+getCAROUSEL();
			if (EXTENSION == null)
				EXTENSION = ".jpg";
			//DEBUG
			System.out.println("CAROUSEL : " + CAROUSEL);
			System.out.println("PATH : " + PATH);
		}
		catch (BundledException e) {
			logger.error("Impossibile trovare la chiave nel file di properties " + CONFIG);
		}
	}

	private static String loadConfiguration(String key) throws BundledException {
		Configuration global = null;
		String profile = null;
		try {
			global = Configuration.getDefault(CONFIG);
			profile = global.getParam(key);
			if (profile == null) throw new BundledException("Impossibile trovare la chiave nel file di properties" + CONFIG,profile);
		} catch (BundledException e) {
			logger.error("Impossibile trovare la chiave nel file di properties " + CONFIG);
			throw new BundledException("Impossibile trovare la chiave nel file di properties" + CONFIG,e);
		}
		return profile;
	}


	@SuppressWarnings("rawtypes")
	@Override
	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws BundledException {
		String idImm="1";
		int tipoDoc = 0;
	
			Utente admin = new Utente();
			admin.setIdUtente("0");
			admin.setStatoRecord("Y");
			File input = null;
			String serverPath = sessione.getServletContext().getRealPath("/");
			try {
				if (ht.get("indirizzo")!= null) {
						input = (File) ht.get("indirizzo");
					if ((ht.get("idimmagine")!= null) && (!"".equals(ht.get("idimmagine")))) {
						idImm = (String) (ht.get("idimmagine")); 
							/*if (!input.getName().substring(0, input.length()-3).equalsIgnoreCase(idDoc)){
								logger.debug("Il nome del file non corrisponde alla nomenclatura stabilita");
							}*/
								
					}
					if (input.isFile()){
						File dir  = new File(serverPath + "" + CAROUSEL);			
						
						if (!dir.exists())
							dir.mkdir();
						File source = new File(input.getPath());
						
						int h = source.getName().lastIndexOf('.');
						String extension = "";
						if (h > 0) {
						    extension = source.getName().substring(h);
						}
						System.out.println("ESTENSIONE " + extension);
						setEXTENSION(extension);
						
						File dest = new File(serverPath + "" + CAROUSEL + "/" + idImm + extension);
						
						//DEBUG
						System.out.println("DESTINAZIONE " + dest.toString());
						
						
						

						
						
						
						if (dest.exists())
							logger.info("File gia presente sul server, sovrascrivo");
						long lastModified = dest.lastModified();
						copyFile(source, dest);
						//System.out.println(lastModified);
						//System.out.println(dest.lastModified());
						if (dest.exists() && (dest.lastModified() > lastModified)) {
							logger.info("Upload del file completato");
							
						}
							
					}
					else {
						logger.error("Errore : tipo file non corretto");
						super.setKOAnswer("", targetpage);
					}
				}
				else {
					logger.error("Errore : nessun file caricato");
					super.setKOAnswer("", targetpage);
				}
			}
			catch (IOException e) {
				logger.error("Errore nella copia del file "+ input + " sul server",e);
				super.setKOAnswer("", targetpage);
			}
			catch(Exception e) {
				logger.error("Errore ",e);
				super.setKOAnswer("", targetpage);
			}
		super.setOKAnswer("", targetpage);
		return super.risposta;
	}



	public static String getCAROUSEL() {
		return CAROUSEL;
	}
	public static String getPATH() {
		return PATH;
	}

	private static File copyFile(File sourceFile, File destFile) throws IOException {

		destFile.createNewFile();

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
		return destFile;
	}

	protected String getIdFunzione() {
		return "uploadImmagine";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="uploadImmagine";
		return chiaveSessione;
	}
	
	public static boolean fileExists(String path) {
		
			File test  = new File(path);
			if (test.exists())
			return true;
			else
				return false;
	}

	public static String getEXTENSION() {
		return EXTENSION;
	}

	public static void setEXTENSION(String eXTENSION) {
		EXTENSION = eXTENSION;
	}

	/*
	 *protected boolean isAuthorized(Utente arg0) {
	 *	return true;
	 *}
	 */

}
