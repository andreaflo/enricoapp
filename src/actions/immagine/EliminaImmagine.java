package actions.immagine;

import java.io.IOException;
import java.util.*;
import org.apache.log4j.*;
import dao.ImmagineDAOImpl;
import dao.ImmagineDao;
import dbobjects.Immagine;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;
import util.RegistratoreFiles;


public class EliminaImmagine extends Operation {
	private static Logger logger;
	private ImmagineDao dao;

	public EliminaImmagine() throws BundledException {
		logger = Logger.getLogger(EliminaImmagine.class);
		dao = new ImmagineDAOImpl();
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
			BundledException 
	{
		String id = (String)ht.get("idimmagine");
		Immagine immagine = dao.getImmagine(id);
		boolean bool = dao.deleteImmagine(id);
		
		try {
			RegistratoreFiles.deleteFile(immagine);
		} catch (IOException | BundledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.setOKAnswer(bool, targetpage);
		return super.risposta;
	}

	protected String getIdFunzione() {
		return "modificaRiferimentiXAnagrafica";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="eliminaRiferimentiXAnagrafica";
		return chiaveSessione;
	}

	/*
	*protected boolean isAuthorized(Utente arg0) {
	*	return true;
	*}
	*/
}
