package actions.contatto.beans;

import java.util.*;
import org.apache.log4j.*;

import actions.immagine.beans.FiltroImmagine;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;


public class FindContatto extends Operation {
	private static Logger logger = Logger.getLogger(FindContatto.class);

	public FindContatto() throws BundledException {
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
			BundledException 
	{
		FiltroImmagine myFiltro = new FiltroImmagine();
				if(valido((String)ht.get("idImmagine")))
				{myFiltro.setIdImmagine((String)ht.get("idImmagine"));}
						if(valido((String)ht.get("dataInstallazione")))
				{myFiltro.setStatoRecord((String)ht.get("stato"));}
						if(valido((String)ht.get("dataIns")))
				{myFiltro.setDataIns((String)ht.get("dataIns"));}
						if(valido((String)ht.get("dataVar")))
				{myFiltro.setDataVar((String)ht.get("dataVar"));}
						if(valido((String)ht.get("userIns")))
				{myFiltro.setUserIns((String)ht.get("userIns"));}
						if(valido((String)ht.get("userVar")))
				{myFiltro.setUserVar((String)ht.get("userVar"));}
						//List lista=opRiferimentiXAnagrafica.findByFilter(myFiltro);
		//super.setOKAnswer(lista, targetpage);
		super.setOKAnswer(myFiltro, targetpage);
		return super.risposta;
	}
	
	private boolean valido(String in)
	{
		boolean result=false;
		if(in!=null)
		{
			if(in.length()>0){result=true;}
		}
		return(result);
	}
	
	protected String getIdFunzione() {
		return "leggiRiferimentiXAnagrafica";
	}
	protected String getChiaveSessione() {
		//String chiaveSessione="listaRiferimentiXAnagrafica";
		String chiaveSessione="filtroRiferimentiXAnagrafica";
		return chiaveSessione;
	}

	/*
	*protected boolean isAuthorized(Utente arg0) {
	*	return true;
	*}
	*/
}
