package actions.contatto;

import java.util.Map;

import org.apache.log4j.Logger;

import it.realt.general.BundledException;
import it.realt.webapps.mail.MailSender;
import it.realt.webapps.utente.Operation;
import it.realt.webapps.utente.dbbeans.Utente;
import it.realt.webapps.utente.mvc.action.Answer;

public class MailContattoAction extends Operation{

	public MailContattoAction() throws BundledException {
		super();
		// TODO Auto-generated constructor stub
	}

	private Logger logger = Logger.getLogger(MailContattoAction.class);

	@Override
	protected Answer processImpl(Map arg0, String arg1, String arg2, String arg3, Utente arg4) throws BundledException {
		
		String nome = (String) arg0.get("nome");
		String cognome = (String) arg0.get("cognome");
		String email = (String) arg0.get("email");
		
		//sendMail(nome, cognome, email);
		
		String targetPageOK  = "/contatto/contattoList.jsp";

		String targetPageKO  = "/contatto/contattoErrore.jsp";
		
		super.setOKAnswer(null, targetPageOK);
		return super.risposta;
	}
	
}
