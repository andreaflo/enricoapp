package actions.contatto;

import java.util.*;
import org.apache.log4j.*;

import dao.ContattoDAO;
import dao.ContattoDaoImpl;
import dbobjects.Contatto;
import dbobjects.Immagine;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;


public class ModificaContatto extends Operation {
	private static Logger logger;
	private ContattoDAO dao = new ContattoDaoImpl();

	public ModificaContatto() throws BundledException {
		logger = Logger.getLogger(ModificaContatto.class);
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
			BundledException 
	{
		//Immagine immagineSource = immagineDao.getImmagine((String)ht.get("idAnagrafica"));
		Contatto toStore  = null;
		Date dt = null;
		try {
		Immagine dettaglioImmagine = (Immagine)ht.get("dettaglioImmagine");
		
		String idtoupdate = (String)ht.get("idcontatto");
		String ragione = (String)ht.get("ragionesociale");
		String nominativo =(String)ht.get("nominativo");
		String email =(String)ht.get("email");
		String telefono =(String)ht.get("telefono");
		String testo =(String)ht.get("testo");
		String stato =(String)ht.get("stato");
		String data = (String)ht.get("dataInserimento");
		String nota = (String)ht.get("notaAmministratore");
		String provenienza = (String)ht.get("provenienza");
		
		Long tele = new Long(telefono);
		
		Integer st = new Integer(stato);
		
		if (data.isEmpty())
			dt = new Date();
		else
			dt = new Date(data.toString());
		
		toStore = new Contatto(idtoupdate,ragione,nominativo,email,tele,testo,st,dt,nota,provenienza);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		Contatto target = dao.updateContatto(toStore);
		super.setOKAnswer(target, targetpage);
		return super.risposta;
	}

	protected String getIdFunzione() {
		return "modificaContatto";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="dettaglioContatto";
		return chiaveSessione;
	}

	/*
	*protected boolean isAuthorized(Utente arg0) {
	*	return true;
	*}
	*/
}
