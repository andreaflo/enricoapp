package actions.contatto;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.text.MaskFormatter;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import actions.mail.TLSEmail;
import dao.ContattoDAO;
import dao.ContattoDaoImpl;
import dbobjects.Contatto;
import dbobjects.Stato;
import file.UtilContatto;
import it.realt.general.BundledException;
import it.realt.webapps.mail.MailSender;
import it.realt.webapps.newsletter.costanti.CostantiModuloNewsletter;
import it.realt.webapps.utente.Operation;
import it.realt.webapps.utente.dbbeans.Utente;
import it.realt.webapps.utente.mvc.action.Answer;
import it.realt.webapps.utente.mvc.xml.binding.Targetpage;

public class ContattoAction extends Operation{


	private Logger logger;

	private static ContattoDAO dao = new ContattoDaoImpl();

	public ContattoAction() throws BundledException {
		logger = Logger.getLogger(ContattoAction.class);
		// inizializzato one liner
	}



	@Override
	protected Answer processImpl(Map arg0, String arg1, String arg2, String arg3, Utente arg4) throws BundledException {

		Contatto contatto = null;

		String targetPage = "";

		String targetPageOK  = "/contatto/contattoList.jsp";

		String targetPageKO  = "/contatto/contattoErrore.jsp";


		String testo ="";
		String nominativo ="";
		String telefono = "";
		String email ="";
		String ragione ="";
		Date dataIns = new Date();
		String nota = "";
		Long fone = 0L;

		try {
			Map request = arg0;

			if (request.get("testo")!= null) {
				testo = request.get("testo").toString();
			}
			if (request.get("nominativo")!= null) {
				nominativo = request.get("nominativo").toString();
			}
			if (request.get("telefono")!= null) {
				telefono = request.get("telefono").toString();
			}
			if (request.get("email")!= null) {
				email = request.get("email").toString();
			}
			if (request.get("ragione")!= null) {
				ragione = request.get("ragione").toString();
			}	
			if (request.get("dataInserimento")!= null) {
				dataIns = (Date) request.get("dataInserimento");
			}

			if (request.get("notaAmministratore")!= null) {
				nota = request.get("notaAmministratore").toString();
			}
			fone = 0L;
			try
			{
				fone = UtilContatto.isNumeric(telefono);
			}
			catch(Exception r ) {
				logger.info("End insertContatto, ko");
				targetPage = targetPageKO;
				super.setOKAnswer(contatto, targetPageKO);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			targetPage = targetPageKO;
			super.setOKAnswer(contatto, targetPageKO);
		}
		logger.info("End contattoAction, "+ targetPage);

		boolean esito1 = UtilContatto.inserisciESpedisciMail(dao, logger,ragione, nominativo, email, fone, testo, contatto, targetPage, targetPageOK, targetPageKO, dataIns, nota);
		if (esito1) {
			String nome = nominativo.split(" ")[0];
			String cognome = nominativo.replace(nome,"");
			boolean esito2 = TLSEmail.sendEmail(nome,cognome,email,ragione,fone.toString(),testo);
			if (esito2)
				super.setOKAnswer(contatto, targetPageOK);
			else
				super.setOKAnswer(contatto, targetPageKO);
		}
		else 
			super.setOKAnswer(contatto, targetPageKO);
		return super.risposta;
	}		





	private void setMailContatto(String nominativo,String email) {

		String cognome = nominativo.split(" ")[0];
		sessione.setAttribute("cognome",cognome);
		sessione.setAttribute("nome", nominativo.replace(cognome,""));
		sessione.setAttribute("email", email);

	}

	
}
