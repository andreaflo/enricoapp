package actions.contatto;

import java.util.Map;
import org.apache.log4j.Logger;
import dao.ContattoDAO;
import dao.ContattoDaoImpl;
import dbobjects.Contatto;
import it.realt.general.BundledException;
import it.realt.webapps.utente.Operation;
import it.realt.webapps.utente.dbbeans.Utente;
import it.realt.webapps.utente.mvc.action.Answer;

public class DettaglioContatto extends Operation {
	private static Logger logger;
	private ContattoDAO dao; 

	public DettaglioContatto() throws BundledException {
		logger = Logger.getLogger(DettaglioContatto.class);
           dao = new ContattoDaoImpl();
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
	BundledException 
	{
		if(valido((String)ht.get("idcontatto")))
		{
			String id = (String)ht.get("idcontatto");
			Contatto c = dao.getContatto(id);
			super.setOKAnswer(c, targetpage);
		}
		return super.risposta;
	}

	private boolean valido(String in)
	{
		boolean result=false;
		if(in!=null)
		{
			if(in.length()>0){result=true;}
		}
		return(result);
	}

	protected String getIdFunzione() {
		return "leggiContatto";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="dettaglioContatto";
		return chiaveSessione;
	}
}
