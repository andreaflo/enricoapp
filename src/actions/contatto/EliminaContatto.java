package actions.contatto;

import java.util.*;
import org.apache.log4j.*;

import dao.ContattoDAO;
import dao.ContattoDaoImpl;
import it.realt.general.*;
import it.realt.webapps.utente.*;
import it.realt.webapps.utente.dbbeans.*;
import it.realt.webapps.utente.mvc.action.*;


public class EliminaContatto extends Operation {
	private static Logger logger;
	private ContattoDAO dao;

	public EliminaContatto() throws BundledException {
		logger = Logger.getLogger(EliminaContatto.class);
		dao = new ContattoDaoImpl();
	}

	protected Answer processImpl(Map ht, String codoper, String callpage, String targetpage, Utente user) throws
			BundledException 
	{
		String id = (String)ht.get("idcontatto");
		boolean bool = dao.deleteContatto(id);
	
		super.setOKAnswer(bool, targetpage);
		return super.risposta;
	}

	protected String getIdFunzione() {
		return "modificaRiferimentiXAnagrafica";
	}
	protected String getChiaveSessione() {
		String chiaveSessione="eliminaRiferimentiXAnagrafica";
		return chiaveSessione;
	}

	/*
	*protected boolean isAuthorized(Utente arg0) {
	*	return true;
	*}
	*/
}
