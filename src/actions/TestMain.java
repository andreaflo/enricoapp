package actions;

import java.text.ParseException;
import javax.swing.text.MaskFormatter;
import org.hibernate.Session;
import dbobjects.Contatto;

public class TestMain {
		     
		    public static void main(String[] args) 
		    {
		        Session session = it.HibernateUtils.getSessionFactory().openSession();
		        session.beginTransaction();
		        
		        //Add new Employee object
		        Contatto emp = new Contatto();
		        emp.setEmail("lokesh@mail.com");
		        emp.setNominativo(" E");
		        emp.setRagionesociale("E R");
		        try {
					emp.setTelefono(new Long(checkTelefone("045986379")));
				} catch (NumberFormatException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        emp.setTesto(" PROVA" );
		         
		        //Save the employee in database
		        session.save(emp);
		 
		        //Commit the transaction
		        session.getTransaction().commit();
		        it.HibernateUtils.shutdown();
		}
		    
		    private static String checkTelefone(String phoneNumber) throws ParseException {

		    	String phoneMask= "###-######";

		    	MaskFormatter maskFormatter= new MaskFormatter(phoneMask);
		    	maskFormatter.setValueContainsLiteralCharacters(false);

		    	return maskFormatter.valueToString(phoneNumber);
		    	
		    	}
}
