package file;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.text.MaskFormatter;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import actions.mail.EmailUtil;
import actions.mail.TLSEmail;
import dao.ContattoDAO;
import dbobjects.Contatto;
import dbobjects.Stato;

public class UtilContatto {

	public static boolean inserisciESpedisciMail(ContattoDAO dao,Logger logger ,String ragione,String nominativo,String email,Long fone,String testo,Contatto contatto,String targetPage,String targetPageOK,String targetPageKO,Date dataIns,String nota) {

		if (validazioneCampi(ragione,nominativo,email,fone,testo)) {
			targetPage = targetPageOK;
			Date d = new Date();
			SimpleDateFormat f = new SimpleDateFormat("YYYYMMDDHHMMSS");
			String idContatto = f.format(d);

			//contatto = new Contatto("",ragione,nominativo,email,fone,testo,Stato.Y.ordinal(),dataIns,nota,"C");
			try {
				insertContatto(logger,dao, contatto);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("Errore durante l'inserimento del contatto in database");
			}
			logger.info("End insertContatto, ok");
			if (TLSEmail.sendEmail(nominativo, nominativo, email, ragione, fone.toString(), testo)) {
			logger.info("End insertContatto, ok");
			return true;
			}
		}

		else {
			targetPage = targetPageKO;
			return false;
		}
		return true;
	}

	private static void insertContatto(Logger logger,ContattoDAO dao,Contatto contatto) throws SQLException {
		Contatto inserted = null;
		try {
			inserted = dao.addContatto(contatto);
			if (inserted != null)
				logger.info("Contatto inserito , "+inserted.toString());
			else 
				logger.info("Contatto errore inserimento");
		}
		catch(HibernateException e ) {
			throw new SQLException("Inserimento fallito per contatto");
		}
	}
	
	private static String sendHTMLEmail(String nome,String cognome,String email,String ragione,String testo) {

		String testoEmail = "";
		// Recipient's email ID needs to be mentioned.
		String to = email;

		// Sender's email ID needs to be mentioned
		String from = "andreaflo@tin.it";

		// Assuming you are sending email from localhost
		String host = "localhost";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Set Subject: header field
			message.setSubject("Sito Corona SRL : Richiesta Contatto da " + email);

			testoEmail = "<h1>Sito Corona SRL</h1>" + "\n" +
					"<div> align = \"center\"> Hai ricevuto una richiesta di contatto "
					+ "\n da parte di " + ragione + " \t , " +cognome + " \t " + nome  
					+ "\n"
					+ "</div>"
					+ "\n" +
					"<div> Il cliente ha scritto : " + testo + " </div> ";
			// Send the actual HTML message, as big as you like
			message.setContent(testoEmail, "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Inviato OK....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
			return "KO";
		}
		return testoEmail;
	}

	private static boolean validazioneCampi(String ragione,String nominativo,String email,Long telefono,String testo) {

		try {

			Long f = isNumeric(telefono.toString());
			// TODO Auto-generated method stub
			if ((ragione != null)
					&& 
					(nominativo != null)
					&& 
					(email != null && isEmail(email))
					&&
					(telefono != null && f>=0 )
					&&
					(testo != null))
				return true;

		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private static boolean isEmail(String email) {
		// TODO Auto-generated method stub
		return true;
	}

	public static Long isNumeric(String floa) {
		Long  ok = -1L;
		if (floa == null) {
			return -1L;
		}
		try {
			ok = Long.parseLong(floa);
		} catch (NumberFormatException nfe) {
			return -1L;
		}
		return ok;
	}

	private static String checkTelefone(String phoneNumber) throws ParseException {

		String phoneMask= "###-######";

		MaskFormatter maskFormatter= new MaskFormatter(phoneMask);
		maskFormatter.setValueContainsLiteralCharacters(false);

		return maskFormatter.valueToString(phoneNumber);

	}
}