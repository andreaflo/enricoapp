package file;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.sun.xml.internal.ws.server.sei.EndpointResponseMessageBuilder;

import actions.contatto.ContattoAction;
import dao.ContattoDAO;
import dao.ContattoDaoImpl;
import dbobjects.Contatto;
 
@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = -913349767145301381L;
	
	private ContattoDAO contattoDAO = new ContattoDaoImpl();
	private Logger logger;
	
	private static final String filePath = "/home/andrea/WorkspaceTest/EnricoApp/files/example/example.pdf";

	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		if (postContatto(request,response))
		       downloadFile(request, response);
    }
	
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		logger = Logger.getLogger(ContattoAction.class);
        // reads input file from an absolute path
       
       if (postContatto(request,response))
       downloadFile(request, response);
       response.sendRedirect("/EnricoApp/jsp/Home.jsp");
    }
	
	
	
	private boolean postContatto(HttpServletRequest request,HttpServletResponse response) {
		String email = "";
		Long longEmail = 0L;
		if (request.getParameter("email")!= null) {
			email = request.getParameter("email");
		}
		try {
			new Email(email);
		
		// creo un nuovo contatto di provenienza Download
		Contatto contatto = new Contatto("","","Sconosciuto",email,0L,"",1,new Date(),"","D");
		
		String targetPageOK  = "/contatto/contattoList.jsp";

		String targetPageKO  = "/contatto/contattoErrore.jsp";
		UtilContatto.inserisciESpedisciMail(contattoDAO, logger, "", "", email, 0L, "", contatto, "", targetPageOK, targetPageKO, new Date(), "");
		return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Email inserita non corretta");
			return false;
		}
		
	}

	@SuppressWarnings("unused")
	private void downloadFile(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		
	        File downloadFile = new File(filePath);
	        FileInputStream inStream = new FileInputStream(downloadFile);
	         
	        // if you want to use a relative path to context root:
	        String relativePath = getServletContext().getRealPath("");
	        System.out.println("relativePath = " + relativePath);
	         
	        // obtains ServletContext
	        ServletContext context = getServletContext();
	         
	        // gets MIME type of the file
	        String mimeType = context.getMimeType(filePath);
	        if (mimeType == null) {        
	            // set to binary type if MIME mapping not found
	            mimeType = "application/octet-stream";
	        }
	        System.out.println("MIME type: " + mimeType);
	         
	        // modifies response
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	         
	        // forces download
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	         
	        // obtains response's output stream
	        OutputStream outStream = response.getOutputStream();
	         
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	         
	        while ((bytesRead = inStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	         
	        inStream.close();
	        outStream.close();  
	        
	}
}