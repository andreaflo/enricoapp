package file;

public class Email {
	
	private String first;
	
	private Character commercialA = '@';
	
	private String domain;
	
	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public Character getCommercialA() {
		return commercialA;
	}

	public void setCommercialA(Character commercialA) {
		this.commercialA = commercialA;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	

	@Override
	public String toString() {
		return "Email [first=" + first + ", commercialA=" + commercialA + ", domain=" + domain + "]";
	}
	
	public void init (String first, Character commercialA, String domain) {
		this.first = first;
		this.commercialA = commercialA;
		this.domain = domain;
	}

	public Email(String first, Character commercialA, String domain) {
		super();
		init(first, commercialA, domain);
	}
	
	public Email(String email) throws Exception {
		super();
		try {
			String[] array = email.split("@");
			first = array[0];
			domain = array[1];
			commercialA = '@';
		}
		catch (IndexOutOfBoundsException i) {
			throw new Exception("The mail is not valid");
		}
		try {
		if ((first.length()> 0) && (commercialA != null) && (domain.length() > 0)){
				init(first,commercialA,domain);
		}
		else 
			throw new Exception("The mail is not valid");
		if (!domain.contains("."))
			throw new Exception("The mail is not valid");
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
	}
		
}
