package dbobjects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name ="contatore", schema ="public" )
@IdClass(Contatore.class)
public class Contatore {

	@Id
	private Integer id;
	
	private Integer contatore;
	
	public Contatore() {
		
	}

	public Integer getContatore() {
		return contatore;
	}

	public void setContatore(Integer contatore) {
		this.contatore = contatore;
	} 
	
	public Contatore(Integer contatore) {
		super();
		this.contatore = contatore;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer pk) {
		this.id = pk;
	}

}
