package dbobjects;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="immagine", schema ="public" )
public class Immagine implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3278399284397652270L;

	@Id
	@GeneratedValue
	private String idimmagine;

	private String descrizione;
	
	private String nome;
	
	private String path;
	
	private String  altro;
	
	private String url;
	
	private String nota;
	
	private String tipo;
	
	private int stato;
	
	private Date dataInserimento;
	
	public Immagine() {
		
	}
	
	public Immagine(String idimmagine, String descrizione, String nome, String path, String altro, String url, String nota, String tipo, Stato stato) {
		super();
		this.idimmagine = idimmagine;
		this.descrizione = descrizione;
		this.nome = nome;
		this.path = path;
		this.altro = altro;
		this.url = url;
		this.nota = nota;
		this.tipo = tipo;
		this.stato = Stato.Y.ordinal();
	}

	@Override
	public String toString() {
		return "Immagine [idimmagine=" + idimmagine + ", descrizione=" + descrizione + ", nome=" + nome + ", path="
				+ path + ", altro=" + altro + ", url=" + url + ", nota=" + nota + ", tipo=" + tipo + ", stato=" + stato
				+ " data " + dataInserimento + "]";
	}

	public String getIdimmagine() {
		return idimmagine;
	}

	public void setIdimmagine(String idimmagine) {
		this.idimmagine = idimmagine;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAltro() {
		return altro;
	}

	public void setAltro(String altro) {
		this.altro = altro;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getStato() {
		return stato;
	}

	public void setStato(int stato) {
		this.stato = stato;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}
}
