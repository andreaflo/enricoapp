package dbobjects;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name ="contatto", schema ="public" )
public class Contatto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1349772910898074612L;

	@Id
	@GeneratedValue
	private String idcontatto;

	private String ragionesociale;
	
	private String nominativo;
	
	private String email;
	
	private Long telefono;
	
	private String testo;
	
	private int stato;
	
	private Date dataInserimento;
	
	private String notaAmministratore;
	
	private String provenienza;
	
	public Contatto() {
		super();
		this.ragionesociale = "00012013289382";
		this.nominativo = "Prova";
		this.telefono = 986379L;
		this.email ="andreaflo@tin.it";
		this.testo =" LORE IPSUM DIXIT ";
		this.stato = Stato.Y.ordinal();
		this.dataInserimento = new Date();
		this.notaAmministratore = "Tutto ok";
		this.provenienza = "C";
	}

	/**
	 * @param idContatto
	 * @param ragioneSociale
	 * @param nominativo
	 * @param telefono
	 * @param email
	 * @param testo
	 * @param provenienza 
	 */
	public Contatto(String idcontatto, String ragionesociale, String nominativo, String email, Long telefono,
			String testo, int stato, Date dataInserimento, String notaAmministratore, String provenienza) {
		super();
		this.idcontatto = idcontatto;
		this.ragionesociale = ragionesociale;
		this.nominativo = nominativo;
		this.email = email;
		this.telefono = telefono;
		this.testo = testo;
		this.stato = stato;
		this.dataInserimento = dataInserimento;
		this.notaAmministratore = notaAmministratore;
		this.provenienza = provenienza;
	}

	public String getIdcontatto() {
		return idcontatto;
	}

	public void setIdcontatto(String idContatto) {
		this.idcontatto = idContatto;
	}

	public String getRagionesociale() {
		return ragionesociale;
	}

	public void setRagionesociale(String ragioneSociale) {
		this.ragionesociale = ragioneSociale;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public int getStato() {
		return stato;
	}

	public void setStato(int stato) {
		this.stato = stato;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getNotaAmministratore() {
		return notaAmministratore;
	}

	public void setNotaAmministratore(String notaAmministratore) {
		this.notaAmministratore = notaAmministratore;
	}

	public String getProvenienza() {
		return provenienza;
	}

	public void setProvenienza(String provenienza) {
		this.provenienza = provenienza;
	}

	@Override
	public String toString() {
		return "Contatto [idcontatto=" + idcontatto + ", ragionesociale=" + ragionesociale + ", nominativo="
				+ nominativo + ", email=" + email + ", telefono=" + telefono + ", testo=" + testo + ", stato=" + stato
				+ ", dataInserimento=" + dataInserimento + ", notaAmministratore=" + notaAmministratore + "provenienza="+ provenienza + "]";
	}
	
	
	
}
